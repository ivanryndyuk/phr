//
//  CheckListStatusCell.swift
//  phr.kz
//
//  Created by ivan on 14/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class CheckListStatusCell: UITableViewCell {

    //MARK: - VARS
    
    @IBOutlet weak var circleView: View!
    @IBOutlet weak var titleLabel: UILabel!
    
    var mode: CheckListStatusCellMode = .inactive {
        didSet {
            update()
        }
    }
    
    var title: String? {
        get {
            return titleLabel.text
        }
        
        set {
            titleLabel.text = newValue
        }
    }
    
    
    
    //MARK: - FUNCS
    
    func update() {
        var borderColor: UIColor
        var fillColor: UIColor
        var shadowColor: UIColor
        var shadowRadius: CGFloat
        var shadowOpacity: Float
        
        switch mode {
        case .inactive:
            borderColor = UIColor(hex: 0x6A7494)
            fillColor = UIColor.clear
            shadowColor = UIColor.clear
            shadowRadius = 0.0
            shadowOpacity = 0.0
        case .current:
            borderColor = UIColor(hex: 0xFDAE55)
            fillColor = UIColor.clear
            shadowColor = UIColor.clear
            shadowRadius = 0.0
            shadowOpacity = 0.0
        case .done:
            borderColor = UIColor(hex: 0x79EDBA)
            fillColor = UIColor(hex: 0x79EDBA)
            shadowColor = UIColor(hex: 0x79EDBA)
            shadowRadius = 5.0
            shadowOpacity = 0.5
        }
        
        titleLabel.textColor = borderColor
        circleView.borderColor = borderColor
        circleView.backgroundColor = fillColor
        circleView.shadowColor = shadowColor
        circleView.shadowRadius = shadowRadius
        circleView.shadowOpacity = shadowOpacity
        circleView.shadowOffset = .zero
    }
    
    
    enum CheckListStatusCellMode {
        case inactive, current, done
    }
}
