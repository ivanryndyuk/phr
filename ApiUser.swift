//
//  ApiUser.swift
//  phr.kz
//
//  Created by ivan on 06/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ApiUser: ApiSection {
    
    //MARK: - FUNCS
    
    /* Register new user or doctor */
    
    func register(type: String, login: String, password: String, callback: @escaping Block<User>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .post, path: "security/register/\(type)", params: ["login": login, "password": password])
        parent.pass(request: request, callback: { (json) in
            if let userId = (json["data"] as? JsonObject)?["user"] as? String {
                if let user = app.stack.user() {
                    user.reset(newId: userId)
                    callback(user)
                } else {
                    fallback(NSError.with(description: "Невозможно создать нового пользователя. Попробуйте позднее или обратитесь в службу поддержки.".localized))
                }
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
    }
    
    
    /* Authorize user with login/password */
    
    func authorize(login: String, password: String, callback: @escaping (() -> Void), fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .post, path: "security/authorize", params: ["login": login, "password": password])
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                if let token = data["token"] as? String {
                    app.api.token = token
                    app.ud.set(true, forKey: app.keys.authorization)
                    callback()
                } else {
                    fallback(Api.Errors.wrongJsonFormat)
                }
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
    }
    
    
    /* Reset password */
    
    func reset(login: String, callback: @escaping Block<String>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .post, path: "security/reset-password", params: ["login": login])
        parent.pass(request: request, callback: { (json) in
            if let message = (json["data"] as? JsonObject)?["message"] as? String {
                callback(message)
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
    }
    
    
    /* Confirm operations with code */
    
    func confirm(code: String, id: String, action: ActionType, callback: @escaping Block<Bool>, fallback: @escaping Block<Error>) {
        var params = ["code": code, "user": id]
        
        switch action {
        case .authorize:
            params["type"] = "auth"
        case .change:
            params["type"] = "change"
        case  .registration:
            params["type"] = "registration"
        }
        
        let request = parent.request(withMethod: .post, path: "security/confirm", params: params)
        parent.pass(request: request, callback: { (json) in
            callback(true)
        }, fallback: fallback)
    }
    
    
    
    //MARK: - UPDATE
    
    func updatePatient(params: [String: Any], needsConfirm: Bool = false, callback: @escaping Block<String?>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .patch, path: "selfuser/patient", params: params, token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                if let user = data["user"] as? JsonObject {
                    app.user?.apply(params: user)
                    
                    if needsConfirm {
                        if let changed = data["changed_providers"] as? JsonObject {
                            if changed.count == 0 {
                                fallback(NSError.with(description: "Ошибка изменения профиля.".localized))
                            } else {
                                var message: String?
                                
                                if let email = changed["email"] as? JsonObject {
                                    message = email["message"] as? String
                                }
                                
                                if let email = changed["phone"] as? JsonObject {
                                    message = email["message"] as? String
                                }
                                
                                callback(message)
                            }
                        } else {
                            fallback(NSError.with(description: "Ошибка изменения профиля.".localized))
                        }
                    } else {
                        callback(nil)
                    }
                    
                    return
                }
            }
            
            fallback(NSError.with(description: "Ошибка изменения профиля.".localized))
        }, fallback: fallback)
    }
    
    func updateDoctor(params: [String: Any], needsConfirm: Bool = false, callback: @escaping Block<String?>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .patch, path: "selfuser/doctor", params: params, token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                if let user = data["user"] as? JsonObject {
                    app.user?.apply(params: user)
                    
                    if needsConfirm {
                        if let changed = data["changed_providers"] as? JsonObject {
                            if changed.count == 0 {
                                fallback(NSError.with(description: "Ошибка изменения профиля.".localized))
                            } else {
                                var message: String?
                                
                                if let email = changed["email"] as? JsonObject {
                                    message = email["message"] as? String
                                }
                                
                                if let email = changed["phone"] as? JsonObject {
                                    message = email["message"] as? String
                                }
                                
                                callback(message)
                            }
                        } else {
                            fallback(NSError.with(description: "Ошибка изменения профиля.".localized))
                        }
                    } else {
                        callback(nil)
                    }
                    
                    return
                }
            }
            
            fallback(NSError.with(description: "Ошибка изменения профиля.".localized))
        }, fallback: fallback)
    }
    
    
    
    //MARK: - ActionType
    
    enum ActionType {
        case authorize, change, registration
    }

}
