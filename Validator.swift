//
//  Validator.swift
//  phr.kz
//
//  Created by ivan on 07/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class Validator: NSObject {

    //MARK: - FUNCS
    
    /* Validate string with regex. */
    
    func validate(string: String, regex: String) -> Bool {
        let tester = NSPredicate(format: "SELF MATCHES %@", regex)
        return tester.evaluate(with: string)
    }
    
    
    /* Checks input string for matching to the phone template. */
    
    func validate(phone: String) -> Bool {
        return validate(string: phone, regex: "^(\\s*)?(\\+)?([- _():=+]?\\d[- _():=+]?){10,14}(\\s*)?$")
    }
    
    
    /* Checks input string for matching to the email template. */
    
    func validate(email: String) -> Bool {
        return validate(string: email, regex: "^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z]{2,10})$")
    }
    
}
