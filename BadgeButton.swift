//
//  BadgeButton.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class BadgeButton: UIButton {
    
    //MARK: - VARS
    
    let badgeView = UIView()
    let badgeLabel = UILabel()
    private var attrs = [String: Any]()
    
    var isActive = false {
        willSet {
            if newValue {
                setTitleColor(UIColor(r: 255, g: 255, b: 255, a: 1.0), for: .normal)
            } else {
                setTitleColor(UIColor(r: 255, g: 255, b: 255, a: 0.5), for: .normal)
            }
        }
    }
    
    var badge: String? {
        get {
            
            return badgeLabel.attributedText?.string
        }
        
        set {
            if let newValue = newValue {
                badgeLabel.attributedText = NSAttributedString(string: newValue, attributes: attrs)
                badgeView.isHidden = false
            } else {
                badgeLabel.attributedText = nil
                badgeView.isHidden = true
            }
            
        }
    }
    
    
    
    //MARK: - LIFE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setupBadgeView()
    }
    
    
    
    //MARK: - FUNCS
    
    func setupBadgeView() {
        badgeView.backgroundColor = UIColor(hex: 0xFD5577)
        badgeView.radius = 10.0
        addSubview(badgeView)
        badgeView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraint(NSLayoutConstraint(item: badgeView, attribute: .centerY, relatedBy: .equal, toItem: titleLabel, attribute: .centerY, multiplier: 1.0, constant: 0.0))
        
        addConstraint(NSLayoutConstraint(item: badgeView, attribute: .leading, relatedBy: .equal, toItem: titleLabel, attribute: .trailing, multiplier: 1.0, constant: 6.0))
        
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:[badge(==20)]",
            options: .zero,
            metrics: nil,
            views: ["badge": badgeView]))
        
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:[badge(==20)]",
            options: .zero,
            metrics: nil,
            views: ["badge": badgeView]))
        
        let style = NSMutableParagraphStyle()
        style.alignment = .center
        style.minimumLineHeight = 12.0
        style.maximumLineHeight = 12.0
        
        attrs = [NSParagraphStyleAttributeName: style, NSFontAttributeName: UIFont.with(name: app.theme.fontTahoma, size: 12), NSForegroundColorAttributeName: UIColor.white]
        
        badgeLabel.adjustsFontSizeToFitWidth = true
        badgeView.addSubview(badgeLabel)
        badgeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        badgeView.addConstraint(NSLayoutConstraint(item: badgeLabel, attribute: .centerY, relatedBy: .equal, toItem: badgeView, attribute: .centerY, multiplier: 1.0, constant: 0.0))
        
        badgeView.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-2-[badgeLabel]-2-|",
            options: .zero,
            metrics: nil,
            views: ["badgeLabel": badgeLabel]))
        
        badgeView.isHidden = true
    }
    
    
    
    //MARK: - TOUCH
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2) {
            self.alpha = 0.35
        }
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
        }) { (finished) in
            if finished {
                if let touch = touches.first {
                    let point = touch.location(in: self)
                    if self.bounds.contains(point) {
                        self.sendActions(for: .touchUpInside)
                    }
                }
            }
        }
    }
    
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2) {
            self.alpha = 1.0
        }
    }
}
