//
//  MessageCell.swift
//  phr.kz
//
//  Created by ivan on 24/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

protocol MessageCellDelegate {
    func translate(for cell: MessageCell)
    func showTranslation(for cell: MessageCell)
}

class MessageCell: UITableViewCell {
    
    //MARK: - VARS
    
    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var containerView: RandomCornerView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var messageView: RandomCornerView!
    @IBOutlet weak var messageViewWidth: NSLayoutConstraint!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var attachmentView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var statusImage: ImageView!
    @IBOutlet weak var statusImageWidth: NSLayoutConstraint!
    @IBOutlet weak var loaderBlock: UIView!
    @IBOutlet weak var loader: Loader!
    @IBOutlet weak var statusView: UIView!
    @IBOutlet weak var translationButton: ImageButton!
    
    static let textViewPattern = UITextView(frame: CGRect(x: 0.0, y: 0.0, width: 250.0, height: 0))
    static let preferredImageSide: CGFloat = 250.0
    
    var delegate: MessageCellDelegate?
    var longPressRecognizer: UILongPressGestureRecognizer!
    
    var fillColor: UIColor? {
        didSet {
            messageView.backgroundColor = fillColor
        }
    }
    
    var type: MessageCellType = .outgoing {
        didSet {
            updateType()
        }
    }
    
    var message: String? {
        get {
            return messageTextView.text
        }
        
        set {
            messageTextView.text = newValue
        }
    }
    
    var header: String? {
        get {
            return headerLabel.text
        }
        
        set {
            headerLabel.text = newValue
            if newValue == nil || newValue == .some("") {
                headerLabel.isHidden = true
            } else {
                headerLabel.isHidden = false
            }
        }
    }
    
    var timestamp: String? {
        get {
            return timeLabel.text
        }
        
        set {
            timeLabel.text = newValue
        }
    }
    
    var attachmentUrl: URL? {
        didSet {
            loadImage()
        }
    }
    
    var isRead = false {
        didSet {
            if isRead {
                statusImage.tintImage = #imageLiteral(resourceName: "icon-read")
            } else {
                statusImage.tintImage = #imageLiteral(resourceName: "icon-sent")
            }
        }
    }
    
    var isLoading = false {
        didSet {
            if isLoading {
                loader.startAnimating()
                loaderBlock.isHidden = false
            } else {
                loaderBlock.isHidden = true
                loader.stopAnimating()
            }
        }
    }
    
    
    
    //MARK: - LIFE

    override func awakeFromNib() {
        super.awakeFromNib()
        messageTextView.contentInset = .zero
        messageTextView.textContainerInset = .zero
        longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(handlerOf(longPress:)))
        translationButton.addTarget(self, action: #selector(handlerOf(translationButton:)), for: .touchUpInside)
        
        MessageCell.textViewPattern.font = messageTextView.font
        MessageCell.textViewPattern.contentInset = .zero
        MessageCell.textViewPattern.textContainerInset = .zero
    }
    
    
    
    //MARK: - FUNCS
    
    func handlerOf(translationButton: ImageButton) {
        delegate?.showTranslation(for: self)
    }
    
    func updateType() {
        switch type {
        case .incoming:
            containerView.roundBottomRight = true
            containerView.roundBottomLeft = false
            containerView.roundCorners()
            messageView.roundBottomRight = true
            messageView.roundBottomLeft = false
            messageView.roundCorners()
            //fillColor = UIColor(hex: 0xF3F4F1)
            stack.alignment = .leading
        case .outgoing:
            containerView.roundBottomRight = false
            containerView.roundBottomLeft = true
            containerView.roundCorners()
            messageView.roundBottomRight = false
            messageView.roundBottomLeft = true
            messageView.roundCorners()
            //fillColor = UIColor(hex: 0xFAEF9E)
            showReadStatus()
            stack.alignment = .trailing
        }
    }
    
    
    func loadImage() {
        DispatchQueue.main.async {
            if let url = self.attachmentUrl {
                do {
                    let data = try Data(contentsOf: url)
                    if let image = UIImage(data: data) {
                        self.attachmentView.image = image
                    }
                } catch let error {
                    print(error)
                }
            }
        }
    }
    
    
    func reset() {
        message = nil
        timestamp = nil
        attachmentView.image = nil
        isRead = false
        statusImage.isHidden = true
        statusImageWidth.constant = 4.0
        isLoading = false
        delegate = nil
        setupStatus()
        remoceLongPress()
        translationButton.isEnabled = false
        translationButton.isHidden = true
        messageViewWidth.constant = 250.0
        headerLabel.isHidden = true
        header = nil
    }
    
    
    func setupStatus(forImageMessage: Bool = false) {
        if forImageMessage {
            statusView.backgroundColor = UIColor(r: 0, g: 0, b: 0, a: 0.65)
            timeLabel.textColor = UIColor(r: 255, g: 255, b: 255, a: 0.65)
            statusImage.imageColor = UIColor(r: 255, g: 255, b: 255, a: 0.65)
        } else {
            statusView.backgroundColor = UIColor.clear
            timeLabel.textColor = UIColor(r: 0, g: 0, b: 0, a: 0.5)
            statusImage.imageColor = UIColor(r: 0, g: 0, b: 0, a: 0.5)
        }
        
    }
    
    
    func showReadStatus() {
        statusImage.isHidden = false
        statusImageWidth.constant = 25.0
    }
    
    
    func remoceLongPress() {
        removeGestureRecognizer(longPressRecognizer)
    }
    
    
    func translate() {
        delegate?.translate(for: self)
    }
    
    
    func set(translationStatus: String) {
        translationButton.title = translationStatus
        translationButton.isEnabled = true
        translationButton.isHidden = false
    }
    
    
    func showActionsMenu() {
        becomeFirstResponder()
        let menu = UIMenuController.shared
        let translateItem = UIMenuItem(title: "Перевести".localized, action: #selector(translate))
        menu.menuItems = [translateItem]
        //menu.arrowDirection = .up
        menu.setTargetRect(CGRect(x: containerView.bounds.width / 2, y: containerView.bounds.height, width: 0, height: 0), in: containerView)
        menu.setMenuVisible(true, animated: true)
        messageTextView.text = ""
        messageTextView.sizeToFit()
    }
    
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(translate) {
            return true
        }
        
        return false
    }
    
    
    func handlerOf(longPress: UILongPressGestureRecognizer) {
        showActionsMenu()
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        shadowView.isHidden = false
        super.touchesBegan(touches, with: event)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        shadowView.isHidden = true
        super.touchesEnded(touches, with: event)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        shadowView.isHidden = true
        super.touchesCancelled(touches, with: event)
    }
    
}

enum MessageCellType {
    case incoming, outgoing
    
    static func with(hash: Int) -> MessageCellType {
        if hash == 0 {
            return .incoming
        } else {
            return .outgoing
        }
    }
}
