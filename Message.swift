//
//  Message.swift
//  phr.kz
//
//  Created by ivan on 12/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import TwilioChatClient

class Message: NSObject, NSCoding {
    
    //MARK: - VARS
    
    var object: TCHMessage?
    var json: JsonObject?
    var bodyTranslation: String?
    var isRead: Bool = false
    var type: MessageCellType = .incoming
    var preferredHeight: CGFloat = 0
    
    var sid: String {
        get {
            if let sid = object?.sid {
                return sid
            } else if let sid = json?["sid"] as? String {
                return sid
            } else {
                return "--> nil? <--"
            }
        }
    }
    
    var index: NSNumber {
        get {
            if let index = object?.index {
                return index
            } else if let index = json?["index"] as? NSNumber {
                return index
            } else {
                return NSNumber(integerLiteral: 0)
            }
        }
    }
    
    var author: String {
        get {
            if let author = object?.author {
                return author
            } else if let author = json?["author"] as? String {
                return author
            } else {
                return "--> nil? <--"
            }
        }
    }
    
    var body: String {
        get {
            if let body = object?.body {
                return body
            } else if let body = json?["body"] as? String {
                return body
            } else {
                return "--> nil? <--"
            }
        }
    }
    
    var timestamp: String {
        get {
            if let timestamp = object?.timestamp {
                return timestamp
            } else if let timestamp = json?["timestamp"] as? String {
                return timestamp
            } else {
                return "--> nil? <--"
            }
        }
    }
    
    var timestampAsDate: Date {
        get {
            if let timestampAsDate = object?.timestampAsDate {
                return timestampAsDate
            } else if let timestampAsDate = json?["timestampAsDate"] as? Date {
                return timestampAsDate
            } else {
                return Date()
            }
        }
    }
    
    var dateUpdated: String {
        get {
            if let dateUpdated = object?.dateUpdated {
                return dateUpdated
            } else if let dateUpdated = json?["dateUpdated"] as? String {
                return dateUpdated
            } else {
                return "--> nil? <--"
            }
        }
    }
    
    var dateUpdatedAsDate: Date {
        get {
            if let dateUpdatedAsDate = object?.dateUpdatedAsDate {
                return dateUpdatedAsDate
            } else if let dateUpdatedAsDate = json?["dateUpdatedAsDate"] as? Date {
                return dateUpdatedAsDate
            } else {
                return Date()
            }
        }
    }
    
    var attributes: [String: Any] {
        get {
            if let attributes = object?.attributes() {
                return attributes
            } else if let attributes = json?["attributes"] as? [String: Any] {
                return attributes
            } else {
                return [String: Any]()
            }
        }
    }
    
    var file: String? {
        get {
            if let attachments = attributes["attachments"] as? JsonArray {
                if attachments.count > 0 {
                    if let attachment = attachments[0] as? JsonObject {
                        if let original = attachment["original"] as? JsonObject {
                            if let url = original["url"] as? String {
                                return url
                            }
                        }
                    }
                }
            }
            
            return nil
        }
    }
    
    var fileId: String? {
        get {
            if let attachments = attributes["attachments"] as? JsonArray {
                if attachments.count > 0 {
                    if let attachment = attachments[0] as? JsonObject {
                        if let original = attachment["original"] as? JsonObject {
                            if let url = original["id"] as? String {
                                return url
                            }
                        }
                    }
                }
            }
            
            return nil
        }
    }
    
    var fileTranslation: String? {
        get {
            if let attachments = attributes["attachments"] as? JsonArray {
                if attachments.count > 0 {
                    if let attachment = attachments[0] as? JsonObject {
                        if let translation = attachment["translation"] as? JsonObject {
                            if let url = translation["url"] as? String {
                                return url
                            }
                        }
                    }
                }
            }
            
            return nil
        }
    }
    
    
    var translationStatus: String? {
        get {
            if let attachments = attributes["attachments"] as? JsonArray {
                if attachments.count > 0 {
                    if let attachment = attachments[0] as? JsonObject {
                        if let translationStatus = attachment["translationStatus"] as? JsonArray {
                            for case let item as JsonObject in translationStatus {
                                if let lang = item["locale"] as? String {
                                    if lang == app.localeCode {
                                        return item["name"] as? String
                                    }
                                }
                            }
                        }
                    }
                }
            }
            
            return nil
        }
    }
    
    
    
    //MARK: - LIFE
    
    init(message: TCHMessage) {
        self.object = message
        super.init()
        print(message.attributes())
        setupProperties()
    }
    
    init(json: JsonObject) {
        self.json = json
        super.init()
        setupProperties()
    }
    
    required init?(coder aDecoder: NSCoder) {
        guard let json = aDecoder.decodeObject(forKey: "json") as? JsonObject else {
            return nil
        }
        
        guard let bodyTranslation = aDecoder.decodeObject(forKey: "bodyTranslation") as? String? else {
            return nil
        }
        
        let typeHash = aDecoder.decodeInteger(forKey: "type")
        
        self.isRead = aDecoder.decodeBool(forKey: "isRead")
        self.bodyTranslation = bodyTranslation
        self.json = json
        self.type = MessageCellType.with(hash: typeHash)
        super.init()
        setupProperties()
    }
    
    func setupProperties() {
        if let body = attributes["body"] as? JsonObject {
            if let translation = body["translation"] as? String {
                if translation != "<null>" {
                    bodyTranslation = translation
                }
            }
        }
        
        
    }
    
    
    //MARK: - FUNCS
    
    func encode(with aCoder: NSCoder) {
        guard let message = object else {
            return
        }
        var json = JsonObject()
        json["sid"] = message.sid
        json["index"] = message.index
        json["author"] = message.author
        json["body"] = message.body
        json["timestamp"] = message.timestamp
        json["timestampAsDate"] = message.timestampAsDate
        json["dateUpdated"] = message.dateUpdated
        json["dateUpdatedAsDate"] = message.dateUpdatedAsDate
        json["lastUpdatedBy"] = message.lastUpdatedBy
        json["attributes"] = message.attributes()
        aCoder.encode(json, forKey: "json")
        aCoder.encode(bodyTranslation, forKey: "bodyTranslation")
        aCoder.encode(isRead, forKey: "isRead")
        aCoder.encode(type.hashValue, forKey: "type")
    }

}
