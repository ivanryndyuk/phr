//
//  NavigationController.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

@IBDesignable class NavigationController: UINavigationController {
    
    @IBInspectable var useLightStatusBar: Bool = false {
        didSet {
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if useLightStatusBar {
            return .lightContent
        } else {
            return .default
        }
    }
    
    
    
    //MARK: - LIFE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNeedsStatusBarAppearanceUpdate()
    }

}
