//
//  ApiReference.swift
//  phr.kz
//
//  Created by ivan on 07/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ApiReference: ApiSection {
    
    //MARK: - FUNCS
    
    /* Get list of sicknesses */
    
    func sicknesses(limit: String = "100", page: String = "1", sort: String = "entity.createdAt", direction: String = "desc", callback: @escaping Block<(Meta, [Sickness])>, fallback: @escaping Block<Error>) {
        let params = ["limit": limit, "page": page, "sort": sort, "direction": direction]
        let request = parent.request(withMethod: .get, path: "references/diseases", params: params, token: parent.token)
        parent.pass(request: request, callback: { (json) in
            guard let data = json["data"] as? JsonObject else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let metaJson = data["meta"] as? JsonObject else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let meta = Meta(json: metaJson) else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let itemsJson = data["items"] as? JsonArray else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            let items = app.stack.sicknesses(fromJson: itemsJson)
            callback(meta, items)
        }, fallback: fallback)
    }
    
    
    /* Get list of regions */
    
    func regions(limit: String = "100", page: String = "1", sort: String = "entity.createdAt", direction: String = "desc", callback: @escaping Block<(Meta, [Region])>, fallback: @escaping Block<Error>) {
        let params = ["limit": limit, "page": page, "sort": sort, "direction": direction]
        let request = parent.request(withMethod: .get, path: "references/regions", params: params, token: parent.token)
        parent.pass(request: request, callback: { (json) in
            guard let data = json["data"] as? JsonObject else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let metaJson = data["meta"] as? JsonObject else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let meta = Meta(json: metaJson) else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let itemsJson = data["items"] as? JsonArray else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            let items = Region.list(fromJson: itemsJson)
            callback(meta, items)
        }, fallback: fallback)
    }
    
    
    /* Get list of specialties */
    
    func specialities(limit: String = "100", page: String = "1", sort: String = "entity.createdAt", direction: String = "desc", callback: @escaping Block<(Meta, [Speciality])>, fallback: @escaping Block<Error>) {
        let params = ["limit": limit, "page": page, "sort": sort, "direction": direction]
        let request = parent.request(withMethod: .get, path: "references/specialties", params: params, token: parent.token)
        parent.pass(request: request, callback: { (json) in
            guard let data = json["data"] as? JsonObject else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let metaJson = data["meta"] as? JsonObject else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let meta = Meta(json: metaJson) else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let itemsJson = data["items"] as? JsonArray else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            let oldItems = app.stack.items(forEntity: Speciality.self)
            let items = app.stack.specialities(fromJson: itemsJson)
            
            for item in oldItems {
                if !items.contains(item) {
                    app.stack.context?.delete(item)
                }
            }
            
            callback(meta, items)
        }, fallback: fallback)
    }
    
    
    
    //MARK: - ReferenceType
    
    enum ReferenceType {
        case region, sickness, speciality
    }
    
}
