//
//  SplashCell.swift
//  phr.kz
//
//  Created by ivan on 15/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class SplashCell: UICollectionViewCell {

    //MARK: - VARS
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    
    var image: UIImage? {
        get {
            return imageView.image
        }
        
        set {
            imageView.image = newValue
        }
    }
    
    
    var title: String? {
        get {
            return titleLabel.attributedText?.string
        }
        
        set {
            if let newValue = newValue {
                titleLabel.attributedText = NSAttributedString(string: newValue)
            }
        }
    }
    
    
    var message: String? {
        get {
            return messageLabel.attributedText?.string
        }
        
        set {
            if let newValue = newValue {
                messageLabel.text = newValue
            }
        }
    }

}
