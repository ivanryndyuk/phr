//
//  Language.swift
//  phr.kz
//
//  Created by ivan on 16/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class Language: NSObject, NSCoding {
    
    //MARK: - VARS
    
    var id: String
    var name: String
    
    
    
    //MARK: - LIFE
    
    init?(json: JsonObject) {
        guard let id = json["id"] as? String else {
            return nil
        }
        
        guard let name = json["name"] as? String else {
            return nil
        }
        
        self.id = id
        self.name = name
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        guard let id = aDecoder.decodeObject(forKey: "id") as? String else {
            return nil
        }
        
        guard let name = aDecoder.decodeObject(forKey: "name") as? String else {
            return nil
        }
        
        self.id = id
        self.name = name
    }
    
    
    class func list(fromJson json: JsonArray) -> [Language] {
        var result = [Language]()
        
        for case let item as JsonObject in json {
            if let language = Language(json: item) {
                result.append(language)
            }
        }
        
        return result
    }
    
    
    
    //MARK: - FUNCS
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
    }
    
}
