//
//  VCResetPassword.swift
//  phr.kz
//
//  Created by ivan on 18/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCResetPassword: UIViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var bottomOffset: NSLayoutConstraint!
    @IBOutlet weak var formBlock: UIView!
    @IBOutlet weak var messageBlock: UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var phoneBorder: UIView!
    
    
    
    //MARK: - LIFE

    override func viewDidLoad() {
        super.viewDidLoad()
        phoneField.delegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillShow:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillHide:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        app.nc.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(backItem: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func handlerOf(tapGesture: UITapGestureRecognizer) {
        hideKeyboard()
    }
    
    
    @IBAction func handlerOf(sendPassButton: Button) {
        sendPassword()
    }
    
    
    @IBAction func handlerOf(doneButton: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func hideKeyboard() {
        view.endEditing(false)
    }
    
    
    func handlerOf(keyboardWillShow note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = keyboardBounds.height - self.bottomLayoutGuide.length
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func handlerOf(keyboardWillHide note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = 0.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func sendPassword() {
        hideKeyboard()
        
        guard let phone = phoneField.text else {
            app.alert(message: "Укажите номер телефона.".localized)
            return
        }
        
        if phone.isEmpty {
            app.alert(message: "Укажите номер телефона.".localized)
            return
        }
        
        if !app.validator.validate(phone: phone) {
            app.alert(message: "Проверьте правильность номера телефона.".localized)
            return
        }
        
        app.api.user.reset(login: phone, callback: { (message) in
            self.messageLabel.text = message
            UIView.animate(withDuration: 0.2) {
                self.formBlock.alpha = 0.0
                self.messageBlock.alpha = 1.0
            }
        }) { (error) in
            app.alert(error: error)
        }
    }

}



//MARK: - UITextFieldDelegate

extension VCResetPassword: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        phoneBorder.backgroundColor = UIColor(hex: 0x79EDBA)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        phoneBorder.backgroundColor = UIColor(r: 209, g: 204, b: 209, a: 0.3)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        sendPassword()
        return true
    }
    
}
