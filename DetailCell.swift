//
//  DetailCell.swift
//  phr.kz
//
//  Created by ivan on 22/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class DetailCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var rightTitleLabel: UILabel!
    @IBOutlet weak var detailView: UIImageView!
    @IBOutlet weak var detailWidth: NSLayoutConstraint!
    @IBOutlet weak var detailLeading: NSLayoutConstraint!
    
    var title: String? {
        get {
            return titleLabel.text
        }
        
        set {
            titleLabel.text = newValue
        }
    }
    
    var rightTitle: String? {
        get {
            return rightTitleLabel.text
        }
        
        set {
            rightTitleLabel.text = newValue
        }
    }
    
    var detail: UIImage? {
        get {
            return detailView.image
        }
        
        set {
            detailView.image = newValue
            
            if newValue == nil {
                detailView.isHidden = true
                detailWidth.constant = 0.0
                detailLeading.constant = 0.0
            } else {
                detailView.isHidden = false
                detailWidth.constant = 10.0
                detailLeading.constant = 10.0
            }
        }
    }
    
}
