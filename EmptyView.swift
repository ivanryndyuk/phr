//
//  EmptyView.swift
//  phr.kz
//
//  Created by ivan on 16/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

protocol EmptyViewDelegate {
    func handlerOf(actionButton: Button, emptyView: EmptyView)
}

class EmptyView: UIView {
    
    //MARK: - VARS
    
    @IBOutlet weak var patternView: UIView!
    @IBOutlet weak var imageView: ImageView!
    @IBOutlet weak var textLabel: UILabel!
    @IBOutlet weak var actionButton: Button!
    
    var patternColor: UIColor = .clear {
        didSet {
            updatePattern()
        }
    }
    
    var text: String? {
        get {
            return textLabel.text
        }
        
        set {
            textLabel.text = newValue
        }
    }
    
    var image: UIImage? {
        get {
            return imageView.tintImage
        }
        
        set {
            imageView.tintImage = newValue
            imageView.isHidden = newValue == nil
        }
    }
    
    var color: UIColor? {
        willSet {
            imageView.tintColor = newValue
            textLabel.textColor = newValue
        }
    }
    
    var delegate: EmptyViewDelegate?
    
    
    //MARK: - LIFE
    
    class func instance(text: String?, image: UIImage? = nil, patternColor: UIColor = .clear) -> EmptyView {
        if let instance = Bundle.main.loadNibNamed("EmptyView", owner: nil, options: nil)?.first as? EmptyView {
            instance.color = UIColor(hex: 0xBBBBBB)
            instance.patternColor = patternColor
            instance.text = text
            instance.image = image
            return instance
        } else {
            return EmptyView()
        }
    }
    
    

    //MARK: - FUNCS
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    func updatePattern() {
        if patternColor == .clear {
            patternView.isHidden = true
        } else if let pattern = #imageLiteral(resourceName: "pattern").copy(with: patternColor) {
            patternView.backgroundColor = UIColor(patternImage: pattern)
            patternView.isHidden = false
        }
    }
    
    
    func set(delegate: EmptyViewDelegate, actionTitle: String) {
        self.delegate = delegate
        self.actionButton.setTitle(actionTitle, for: .normal)
        self.actionButton.isHidden = false
    }
    
    
    @IBAction func handlerOf(actionButton: Button) {
        delegate?.handlerOf(actionButton: actionButton, emptyView: self)
    }

}
