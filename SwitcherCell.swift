//
//  SwitcherCell.swift
//  phr.kz
//
//  Created by ivan on 22/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

protocol SwitcherCellDelegate {
    func valueChangedOf(switcherCell: SwitcherCell)
}

class SwitcherCell: UITableViewCell {
    
    //MARK: - VARS
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switcher: UISwitch!
    
    var delegate: SwitcherCellDelegate?
    
    var title: String? {
        get {
            return titleLabel.text
        }
        
        set {
            titleLabel.text = newValue
        }
    }
    
    var isOn: Bool {
        get {
            return switcher.isOn
        }
        
        set {
            switcher.setOn(newValue, animated: true)
        }
    }
    
    
    
    //MARK: - LIFE

    override func awakeFromNib() {
        super.awakeFromNib()
        switcher.addTarget(self, action: #selector(handlerOf(switcher:)), for: .valueChanged)
    }
    
    
    
    //MARK: - FUNCS
    
    func handlerOf(switcher: UISwitch) {
        delegate?.valueChangedOf(switcherCell: self)
    }

}
