//
//  TableRowAction.swift
//  phr.kz
//
//  Created by ivan on 15/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class TableRowAction: UITableViewRowAction {
    
    //MARK: - VARS
    
    //var customTitle: String?
    //var image: UIImage?
    private var frame: CGRect = .zero
    
    
    
    //MARK: - LIFE
    
    class func instance(style: UITableViewRowActionStyle, backgoundColor: UIColor?, image: UIImage?, height: CGFloat = 44.0, handler: @escaping (UITableViewRowAction, IndexPath) -> Void) -> TableRowAction {
        let mutableTitle = NSMutableString(string: "")
        let attrs = [NSFontAttributeName: UIFont.systemFont(ofSize: 15.0)]
        
        while mutableTitle.size(attributes: attrs).width < 10.0 {
            mutableTitle.append(" ")
        }
        
        let newTitle = mutableTitle as String
        
        let action = TableRowAction(style: style, title: newTitle, handler: handler)
        action.frame = CGRect(origin: .zero, size: CGSize(width: 50.0, height: 40.0))
        
        if let image = image, let color = backgoundColor {
            action.draw(image: image, onBackground: color)
        }
        
        return action
    }
    
    
    func draw(image: UIImage, onBackground color: UIColor) {
        UIGraphicsBeginImageContextWithOptions(frame.size, true, UIScreen.main.scale)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(color.cgColor)
        context?.fill(CGRect(origin: .zero, size: frame.size))
        
        if let cgImage = image.cgImage {
            context?.draw(cgImage, in: frame)
        }
        
        if let image = UIGraphicsGetImageFromCurrentImageContext() {
            backgroundColor = UIColor(patternImage: image)
        }
        
        UIGraphicsEndImageContext()
    }
    
}
