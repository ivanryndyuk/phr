//
//  ImageView.swift
//  phr.kz
//
//  Created by ivan on 24/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ImageView: UIImageView {

    //MARK: - VARS
    
    private var originalImage: UIImage?
    
    override var image: UIImage? {
        didSet {
            originalImage = image
        }
    }
    
    
    @IBInspectable var tintImage: UIImage? {
        willSet {
            originalImage = newValue
            
            if let color = imageColor {
                if let image = newValue {
                    self.image = image.copy(with: color)
                }
            }
        }
    }
    
    
    @IBInspectable var imageColor: UIColor? {
        didSet {
            if let color = imageColor {
                if let image = image {
                    self.image = image.copy(with: color)
                }
            } else {
                image = originalImage
            }
        }
    }

}
