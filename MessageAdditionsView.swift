//
//  MessageAdditionsView.swift
//  phr.kz
//
//  Created by ivan on 30/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

@objc protocol MessageAdditionsViewDelegate {
    @objc optional func didTap(cameraButton: ImageButton)
    @objc optional func didTap(photoButton: ImageButton)
    @objc optional func didTap(docButton: ImageButton)
    @objc optional func didTap(medrecButton: ImageButton)
}

class MessageAdditionsView: UIView {

    //MARK: - VARS
    
    var container = MessageAdditionsViewContainer.instance()
    var delegate: MessageAdditionsViewDelegate?
    
    
    
    //MARK: - LIFE
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupContainer()
        designSubviews()
    }
    
    
    
    //MARK: - FUNCS
    
    func setupContainer() {
        addSubview(container)
        container.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[container]-0-|",
            options: .zero,
            metrics: nil,
            views: ["container": container]))
        
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[container]-0-|",
            options: .zero,
            metrics: nil,
            views: ["container": container]))
    }
    
    func designSubviews() {
        container.cameraButton.addTarget(self, action: #selector(handlerOf(cameraButton:)), for: .touchUpInside)
        container.photoButton.addTarget(self, action: #selector(handlerOf(photoButton:)), for: .touchUpInside)
        container.docButton.addTarget(self, action: #selector(handlerOf(docButton:)), for: .touchUpInside)
        container.medrecButton.addTarget(self, action: #selector(handlerOf(medrecButton:)), for: .touchUpInside)
    }
    
    func handlerOf(cameraButton: ImageButton) {
        delegate?.didTap?(cameraButton: cameraButton)
    }
    
    func handlerOf(photoButton: ImageButton) {
        delegate?.didTap?(photoButton: photoButton)
    }
    
    func handlerOf(docButton: ImageButton) {
        delegate?.didTap?(docButton: docButton)
    }
    
    func handlerOf(medrecButton: ImageButton) {
        delegate?.didTap?(medrecButton: medrecButton)
    }

}



//MARK: - MessageAdditionsViewContainer

class MessageAdditionsViewContainer: UIView {
    
    //MARK: - VARS
    
    @IBOutlet weak var cameraButton: ImageButton!
    @IBOutlet weak var photoButton: ImageButton!
    @IBOutlet weak var docButton: ImageButton!
    @IBOutlet weak var medrecButton: ImageButton!
    
    
    
    //MARK: - LIFE
    
    class func instance() -> MessageAdditionsViewContainer {
        if let instance = Bundle.main.loadNibNamed("MessageAdditionsView", owner: nil, options: nil)?.first as? MessageAdditionsViewContainer {
            return instance
        } else {
            return MessageAdditionsViewContainer()
        }
    }
}
