//
//  VCFilters.swift
//  phr.kz
//
//  Created by ivan on 24/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCFilters: ViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var filtersTable: UITableView!
    var mode: FiltersMode = .language
    
    var specialities = [Speciality]()
    var sicknesses = [Sickness]()
    var languages = [Language]()
    
    var specialitiesSelected = [Speciality]()
    var sicknessesSelected = [Sickness]()
    var languagesSelected = [Language]()
    
    let reuseId = "filterItemCell"
    let filterStrings = [Any]()
    var checkedList = [Int]()
    let loadingView = LoadingView.instance(message: "Загрузка...".localized)
    
    var sender: VCSearch?
    
    
    
    //MARK: - LIFE

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitleView?.titleLabel.textColor = UIColor.white
        navigationTitleView?.title = "Фильтры".localized
        
        filtersTable.dataSource = self
        filtersTable.delegate = self
        filtersTable.tableFooterView = UIView()
        
        if let ru = Language(json: ["id": "4fa1da97-135b-4d48-868e-9ccdd31c0633", "name": "Русский".localized, "code": "ru"]) {
            languages.append(ru)
        }
        
        if let ru = Language(json: ["id": "", "name": "Английский".localized, "code": "en"]) {
            languages.append(ru)
        }
        
        if let ru = Language(json: ["id": "b15f4211-943e-495d-b4bc-fcfba3a6278f", "name": "Казахский".localized, "code": "kk"]) {
            languages.append(ru)
        }
        
        loadFilters()
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(cancelItem: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func handlerOf(applyItem: UIBarButtonItem) {
        switch mode {
        case .speciality:
            sender?.specialityFilters = specialitiesSelected
        case .sickness:
            sender?.sicknessFilters = sicknessesSelected
        case .language:
            sender?.languageFilters = languagesSelected
        }
        dismiss(animated: true, completion: nil)
    }
    
    
    func showLoading() {
        loadingView.frame = filtersTable.frame
        loadingView.loader.startAnimating()
        view.addSubview(loadingView)
    }
    
    
    func hideLoading() {
        loadingView.removeFromSuperview()
        loadingView.loader.stopAnimating()
    }
    
    
    func loadFilters() {
        switch mode {
        case .speciality:
            loadSpecialities()
        case .sickness:
            loadSicknesses()
        default:
            updateTable()
        }
    }
    
    
    func loadSpecialities() {
        app.api.reference.specialities(callback: { (meta, specialities) in
            self.specialities = specialities
            self.updateTable()
        }) { (error) in
            app.alert(error: error)
        }
    }
    
    
    func loadSicknesses() {
        app.api.reference.sicknesses(callback: { (meta, sicknesses) in
            self.sicknesses = sicknesses
            self.updateTable()
        }) { (error) in
            app.alert(error: error)
        }
    }
    
    
    func updateTable() {
        filtersTable.reloadData()
    }
    
    
    func index(of sickness: Sickness, in sicknesses: [Sickness]) -> Int? {
        for (i, item) in sicknesses.enumerated() {
            if sickness.id == item.id {
                return i
            }
        }
        
        return nil
    }
    
    func index(of speciality: Speciality, in specialities: [Speciality]) -> Int? {
        for (i, item) in specialities.enumerated() {
            if speciality.id == item.id {
                return i
            }
        }
        
        return nil
    }
    
    func index(of language: Language, in languages: [Language]) -> Int? {
        for (i, item) in languages.enumerated() {
            if language.id == item.id {
                return i
            }
        }
        
        return nil
    }
    
}



//MARK: - UITableViewDataSource

extension VCFilters: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch mode {
        case .speciality:
            return specialities.count
        case .sickness:
            return sicknesses.count
        case .language:
            return languages.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath)
    }
    
}



//MARK: - UITableViewDelegate

extension VCFilters: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
       
        switch mode {
        case .speciality:
            let item = specialities[indexPath.row]
            cell.textLabel?.text = item.name
            
            if index(of: item, in: specialitiesSelected) != nil {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        case .sickness:
            let item = sicknesses[indexPath.row]
            cell.textLabel?.text = item.name
            
            if index(of: item, in: sicknessesSelected) != nil {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        case .language:
            let item = languages[indexPath.row]
            cell.textLabel?.text = item.name
            
            if index(of: item, in: languagesSelected) != nil {
                cell.accessoryType = .checkmark
            } else {
                cell.accessoryType = .none
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch mode {
        case .speciality:
            let item = specialities[indexPath.row]
            
            if let index = index(of: item, in: specialitiesSelected) {
                specialitiesSelected.remove(at: index)
            } else {
                specialitiesSelected.append(item)
            }
        case .sickness:
            let item = sicknesses[indexPath.row]
            
            if let index = index(of: item, in: sicknessesSelected) {
                sicknessesSelected.remove(at: index)
            } else {
                sicknessesSelected.append(item)
            }
        case .language:
            let item = languages[indexPath.row]
            
            if let index = index(of: item, in: languagesSelected) {
                languagesSelected.remove(at: index)
            } else {
                languagesSelected.append(item)
            }
        }
        
        tableView.reloadRows(at: [indexPath], with: .fade)
    }
    
}



//MARK: - FiltersMode

enum FiltersMode {
    case speciality, sickness, language
    
    static func with(hash: Int) -> FiltersMode {
        switch hash {
        case 0:
            return .speciality
        case 1:
            return .sickness
        default:
            return .language
        }
    }
}
