//
//  ProgressBar.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ProgressBar: UIView {

    //MARK: - VARS
    
    @IBOutlet weak var progressViewWidth: NSLayoutConstraint!
    @IBOutlet weak var procentView: UIView!
    
    private var progress: CGFloat = 0
    
    
    
    //MARK: - LIFE
    
    class func instance() -> ProgressBar {
        if let instance = Bundle.main.loadNibNamed("ProgressBar", owner: nil, options: nil)?.first as? ProgressBar {
            return instance
        } else {
            return ProgressBar()
        }
    }
    
    
    func set(progress: CGFloat, for frame: CGRect) {
        progressViewWidth.constant = progress * frame.width
    }

}
