//
//  CheckListItem.swift
//  phr.kz
//
//  Created by ivan on 16/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class CheckListItem: NSObject, NSCoding {

    //MARK: - VARS
    
    var id: String
    var name: String
    var complete: Bool
    
    
    
    //MARK: - LIFE
    
    init(name: String, complete: Bool) {
        self.id = "nil"
        self.name = name
        self.complete = complete
    }
    
    
    init?(json: JsonObject) {
        guard let id = json["id"] as? String else {
            return nil
        }
        
        guard let name = json["name"] as? String else {
            return nil
        }
        
        guard let complete = json["complete"] as? Bool else {
            return nil
        }
        
        self.id = id
        self.name = name
        self.complete = complete
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        guard let id = aDecoder.decodeObject(forKey: "id") as? String else {
            return nil
        }
        
        guard let name = aDecoder.decodeObject(forKey: "name") as? String else {
            return nil
        }
        
        guard let complete = aDecoder.decodeObject(forKey: "complete") as? Bool else {
            return nil
        }
        
        self.id = id
        self.name = name
        self.complete = complete
    }
    
    
    class func list(fromJson json: JsonArray) -> [CheckListItem] {
        var result = [CheckListItem]()
        
        for case let item as JsonObject in json {
            if let checkListItem = CheckListItem(json: item) {
                result.append(checkListItem)
            }
        }
        
        return result
    }
    
    
    
    //MARK: - FUNCS
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(complete, forKey: "complete")
    }
    
}
