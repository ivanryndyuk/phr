//
//  Doctor.swift
//  phr.kz
//
//  Created by ivan on 08/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import CoreData

class Doctor: NSObject, NSCoding {

    //MARK: - VARS
    
    var id: String
    var firstName = ""
    var lastName = ""
    var middleName = ""
    var withSecondOpinion = false
    var price = 0
    var region: Region?
    var organizations = [Organization]()
    var specialities = [Speciality]()
    var sicknesses = [Sickness]()
    var languages = [Language]()
    
    
    
    //MARK: - LIFE
    
    init?(json: JsonObject) {
        guard let id = json["id"] as? String else {
            return nil
        }
        
        if let firstName = json["firstName"] as? String {
            self.firstName = firstName
        }
        
        if let lastName = json["lastName"] as? String {
            self.lastName = lastName
        }
        
        if let middleName = json["middleName"] as? String {
            self.middleName = middleName
        }
        
        if let regionJson = json["region"] as? JsonObject {
            self.region = app.stack.region(forJson: regionJson)
        }
        
        if let organizationsJson = json["organizations"] as? JsonArray {
            self.organizations = app.stack.organizations(fromJson: organizationsJson)
        }
        
        if let specialitiesJson = json["specialties"] as? JsonArray {
            self.specialities = app.stack.specialities(fromJson: specialitiesJson)
        }
        
        if let sicknessesJson = json["diseases"] as? JsonArray {
            self.sicknesses = app.stack.sicknesses(fromJson: sicknessesJson)
        }
        
        if let withSecondOpinion = json["secondOpinion"] as? Bool {
            self.withSecondOpinion = withSecondOpinion
        }
        
        if let price = json["price"] as? Int {
            self.price = price
        }
        
        if let languagesJson = json["languages"] as? JsonArray {
            self.languages = Language.list(fromJson: languagesJson)
        }
        
        self.id = id
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        guard let id = aDecoder.decodeObject(forKey: "id") as? String else {
            return nil
        }
        
        guard let firstName = aDecoder.decodeObject(forKey: "firstName") as? String else {
            return nil
        }
        
        guard let lastName = aDecoder.decodeObject(forKey: "lastName") as? String else {
            return nil
        }
        
        guard let middleName = aDecoder.decodeObject(forKey: "middleName") as? String else {
            return nil
        }
        
        guard let languages = aDecoder.decodeObject(forKey: "languages") as? [Language] else {
            return nil
        }
        
        if let id = aDecoder.decodeObject(forKey: "region") as? String {
            self.region = app.stack.item(forEntity: Region.self, predicateFormat: "id == '\(id)'")
        }
        
        guard let organizations = aDecoder.decodeObject(forKey: "organizations") as? [String] else {
            return nil
        }
        
        guard let specialities = aDecoder.decodeObject(forKey: "specialities") as? [String] else {
            return nil
        }
        
        guard let sicknesses = aDecoder.decodeObject(forKey: "sicknesses") as? [String] else {
            return nil
        }
        
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.middleName = middleName
        self.withSecondOpinion = aDecoder.decodeBool(forKey: "withSecondOpinion")
        self.price = aDecoder.decodeInteger(forKey: "price")
        self.languages = languages
        
        self.organizations = [Organization]()
        self.specialities = [Speciality]()
        self.sicknesses = [Sickness]()
        
        for id in organizations {
            if let item = app.stack.item(forEntity: Organization.self, predicateFormat: "id == '\(id)'") {
                self.organizations.append(item)
            }
        }
        
        for id in specialities {
            if let item = app.stack.item(forEntity: Speciality.self, predicateFormat: "id == '\(id)'") {
                self.specialities.append(item)
            }
        }
        
        for id in sicknesses {
            if let item = app.stack.item(forEntity: Sickness.self, predicateFormat: "id == '\(id)'") {
                self.sicknesses.append(item)
            }
        }
    }
    
    
    class func list(fromJson json: JsonArray) -> [Doctor] {
        var result = [Doctor]()
        
        for case let item as JsonObject in json {
            if let doctor = Doctor(json: item) {
                result.append(doctor)
            }
        }
        
        return result
    }
    
    
    
    //MARK - FUNCS
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(id, forKey: "id")
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(lastName, forKey: "lastName")
        aCoder.encode(middleName, forKey: "middleName")
        aCoder.encode(withSecondOpinion, forKey: "withSecondOpinion")
        aCoder.encode(price, forKey: "price")
        aCoder.encode(languages, forKey: "languages")
        aCoder.encode(getIds(fromArray: organizations), forKey: "organizations")
        aCoder.encode(getIds(fromArray: specialities), forKey: "specialities")
        aCoder.encode(getIds(fromArray: sicknesses), forKey: "sicknesses")
        
        if let id = region?.id {
            aCoder.encode(id, forKey: "region")
        }
    }
    
    
    func getIds(fromArray items: [NSManagedObject]) -> [String] {
        var result = [String]()
        
        for item in items {
            if let id = item.value(forKey: "id") as? String {
                result.append(id)
            }
        }
        
        return result
        /*
        var organizationsIds = [String]()
        
        for organization in organizations {
            organizationsIds.append(organization.id)
        }*/
    }
    
    
    func fullName() -> String {
        var result = ""
        var isFirst = true
        
        if !lastName.isEmpty {
            result += lastName
            isFirst = false
        }
        
        if !firstName.isEmpty {
            if isFirst {
                result += firstName
                isFirst = false
            } else {
                result += " \(firstName)"
            }
        }
        
        if !middleName.isEmpty {
            if isFirst {
                result += middleName
                isFirst = false
            } else {
                result += " \(middleName)"
            }
        }
        
        return result
    }
    
    
    func getName() -> String {
        var result = lastName
       
        if result.isEmpty {
            result += firstName
        } else {
            result += " \(firstName)"
        }
        
        return result
    }
    
    
    func getSpecialities() -> String {
        var specialities = ""
        
        for (i, speciality) in self.specialities.enumerated() {
            if let name = speciality.name {
                if i == 0 {
                    specialities += name
                } else {
                    specialities += ", \(name)"
                }
            }
        }
        
        return specialities
    }
    
    
    func getLanguageImage() -> UIImage {
        if languages.count > 0 {
            let language = languages[0]
            
            switch language.id {
            case "b15f4211-943e-495d-b4bc-fcfba3a6278f":
                return #imageLiteral(resourceName: "kz")
            case "":
                return #imageLiteral(resourceName: "en")
            default:
                return #imageLiteral(resourceName: "ru")
            }
        } else {
            return #imageLiteral(resourceName: "ru")
        }
    }
    
}
