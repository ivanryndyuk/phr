//
//  TableDividerView.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

enum TableDividerStyle {
    case header, footer
}

class TableDividerView: UIView {

    //MARK: - VARS
    
    var style: TableDividerStyle = .header
    var textLabel = UILabel()
    
    var text: String? {
        get {
            return textLabel.text
        }
        
        set {
            textLabel.text = newValue
        }
    }
    
    
    
    //MARK: - LIFE
    
    init(style: TableDividerStyle) {
        self.style = style
        super.init(frame: CGRect.zero)
        initialSetup(style: style)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup(style: style)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup(style: style)
    }
    
    private func initialSetup(style: TableDividerStyle) {
        textLabel.textColor = UIColor(hex: 0x6D6D72)
        addSubview(textLabel)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        
        var verticalFormat = ""
        
        switch style {
        case .header:
            verticalFormat = "V:[textLabel]-8-|"
        case .footer:
            verticalFormat = "V:|-0-[textLabel]-0-|"
        }
        
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: verticalFormat,
            options: .zero,
            metrics: nil,
            views: ["textLabel": textLabel]))
        
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-16-[textLabel]-16-|",
            options: .zero,
            metrics: nil,
            views: ["textLabel": textLabel]))
        
        apply(style: style)
    }
    
    
    
    //MARK: - FUNCS
    
    func apply(style: TableDividerStyle) {
        switch style {
        case .header:
            textLabel.font = UIFont.with(name: "Lato-Semibold", size: 12.0)
        case .footer:
            textLabel.font = UIFont.with(name: "Lato-Regular", size: 12.0)
        }
    }

}
