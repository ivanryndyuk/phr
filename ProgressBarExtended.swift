//
//  ProgressBarExtended.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ProgressBarExtended: UIView {
    
    //MARK: - VARS
    
    @IBInspectable override var radius: CGFloat {
        get {
            return layer.cornerRadius
        }
        
        set {
            layer.cornerRadius = newValue
            bar.procentView.radius = newValue
            bar.radius = newValue
        }
    }
    
    var bar = ProgressBar.instance()
    
    
    
    //MARK: - LIFE

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupBar()
    }
    
    
    
    //MARK: - FUNCS
    
    func setupBar() {
        addSubview(bar)
        bar.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[bar]-0-|",
            options: .zero,
            metrics: nil,
            views: ["bar": bar]))
        
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[bar]-0-|",
            options: .zero,
            metrics: nil,
            views: ["bar": bar]))
    }

}
