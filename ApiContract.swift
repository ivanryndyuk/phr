//
//  ApiContract.swift
//  phr.kz
//
//  Created by ivan on 08/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ApiContract: ApiSection {
    
    //MARK: - FUNCS
    
    /* Get patient contracts */
    
    func contracts(limit: String = "100", page: String = "1", sort: String = "entity.createdAt", direction: String = "desc", callback: @escaping Block<(Meta, [Contract])>, fallback: @escaping Block<Error>) {
        let params = ["limit": limit, "page": page, "sort": sort, "direction": direction]
        let request = parent.request(withMethod: .get, path: "contracts", params: params, token: parent.token)
        parent.pass(request: request, callback: { (json) in
            guard let data = json["data"] as? JsonObject else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let metaJson = data["meta"] as? JsonObject else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let meta = Meta(json: metaJson) else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            guard let itemsJson = data["items"] as? JsonArray else {
                fallback(Api.Errors.wrongJsonFormat)
                return
            }
            
            let items = app.stack.contracts(fromJson: itemsJson)
            callback(meta, items)
        }, fallback: fallback)
    }

}
