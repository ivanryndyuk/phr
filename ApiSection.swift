//
//  ApiSection.swift
//  phr.kz
//
//  Created by ivan on 07/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ApiSection: NSObject {
    
    //MARK: - VARS
    
    let parent: Api
    
    
    
    //MARK: - LIFE
    
    init(parent: Api) {
        self.parent = parent
    }
    
}
