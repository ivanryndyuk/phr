//
//  Translation.swift
//  phr.kz
//
//  Created by ivan on 13/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class Translation: NSObject {

    //MARK: - VARS
    
    var id: String
    var status: String
    var sourceFile: File?
    var translatedFile: File?
    
    
    
    //MARK: - LIFE
    
    init?(json: JsonObject) {
        guard let id = json["id"] as? String else {
            return nil
        }
        
        guard let statusJson = json["status"] as? JsonObject else {
            return nil
        }
        
        guard let status = statusJson["name"] as? String else {
            return nil
        }
        
        guard let sourceJson = json["sourceFile"] as? JsonObject else {
            return nil
        }
        
        self.id = id
        self.status = status
        self.sourceFile = app.stack.file(forJson: sourceJson)
        
        if let translatedJson = json["translatedFile"] as? JsonObject {
            self.translatedFile = app.stack.file(forJson: translatedJson)
        }
    }
}
