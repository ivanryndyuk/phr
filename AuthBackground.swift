//
//  AuthBackground.swift
//  phr.kz
//
//  Created by ivan on 16/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class AuthBackground: UIView {

    //MARK: - VARS
    
    let gradientLayer: CAGradientLayer = CAGradientLayer()
    var patternLayer = CALayer()
    var patternImage: UIImageView?
    
    
    
    //MARK: - LIVE
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup()
    }
    
    private func initialSetup() {
        patternImage = UIImageView()
        
        if let pattern = patternImage {
            addSubview(pattern)
            pattern.translatesAutoresizingMaskIntoConstraints = false
            addConstraints(NSLayoutConstraint.constraints(
                withVisualFormat: "V:|-(-30)-[pattern]-(-30)-|",
                options: .zero,
                metrics: nil,
                views: ["pattern": pattern]))
            
            addConstraints(NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-(-30)-[pattern]-(-30)-|",
                options: .zero,
                metrics: nil,
                views: ["pattern": pattern]))
        }
        
        gradientLayer.colors = [UIColor(hex: 0x4E4376).cgColor, UIColor(hex: 0x2B5876).cgColor]
        gradientLayer.locations = [0, 1]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 1.0)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.0)
        layer.insertSublayer(gradientLayer, at: 0)
        patternLayer.frame = frame
        patternLayer.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "auth-bg-pattern")).cgColor
        
        let vertical = UIInterpolatingMotionEffect(keyPath: "center.y", type: .tiltAlongVerticalAxis)
        vertical.minimumRelativeValue = -30
        vertical.maximumRelativeValue = 30
        
        let horizontal = UIInterpolatingMotionEffect(keyPath: "center.x", type: .tiltAlongHorizontalAxis)
        horizontal.minimumRelativeValue = -30
        horizontal.maximumRelativeValue = 30
        
        let group = UIMotionEffectGroup()
        group.motionEffects = [horizontal, vertical]
        
        patternImage?.addMotionEffect(group)
    }
    
    
    
    //MARK: - FUNCS
    
    override func layoutSubviews() {
        super.layoutSubviews()
        gradientLayer.frame = frame
        patternLayer.frame = frame
        getGradientWithPattern()
        showPattern()
    }
    
    
    func showPattern(animated: Bool = true) {
        if animated {
            patternImage?.isHidden = false
            UIView.animate(withDuration: 0.2, animations: { 
                self.patternImage?.alpha = 1.0
            })
        } else {
            patternImage?.alpha = 1.0
            patternImage?.isHidden = false
        }
    }
    
    
    func hidePattern(animated: Bool = true) {
        if animated {
            UIView.animate(withDuration: 0.2, animations: { 
                self.patternImage?.alpha = 0.0
            }, completion: { (finished) in
                self.patternImage?.isHidden = true
            })
        } else {
            patternImage?.alpha = 0.0
            patternImage?.isHidden = true
        }
    }
    
    
    private func getGradientWithPattern() {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, UIScreen.main.scale)
        
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        gradientLayer.render(in: context)
        context.translateBy(x: 0, y: bounds.size.height)
        context.scaleBy(x: 1.0, y: -1.0)
        context.setBlendMode(.colorBurn)
        patternLayer.render(in: context)
        patternImage?.image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    }

}
