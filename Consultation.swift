//
//  Consultation.swift
//  phr.kz
//
//  Created by ivan on 17/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import CoreData
import TwilioChatClient

class Consultation: NSManagedObject {

    //MARK: - VARS
    
    @NSManaged var id: String?
    @NSManaged var channel: Channel?
    @NSManaged var doctor: Doctor?
    @NSManaged var number: String?
    @NSManaged var price: NSNumber?
    @NSManaged var specialities: NSSet?
    @NSManaged var sicknesses: NSSet?
    @NSManaged var organization: Organization?
    @NSManaged var patient: Patient?
    @NSManaged var type: String?
    @NSManaged var isFinished: Bool
    
    var chat: TCHChannel?
    var lastMessage: TCHMessage?
    var unconsumedMessagesCount: UInt = 0
    
    
    
    //MARK: - FUNCS
    
    func getTitle() -> String {
        var result = ""
        
        if app.user?.type == .some(.doctor) {
            if let name = patient?.getName() {
                result = name
            }
        } else {
            if let name = doctor?.getName() {
                result = name
            }
        }
        
        return result
    }
    
    
    func getSubtitle() -> String {
        var result = ""
        
        if app.user?.type == .some(.doctor) {
            if let sicknesses = sicknesses {
                for item in sicknesses {
                    if let sickness = item as? Sickness {
                        if let name = sickness.name {
                            if result.isEmpty {
                                result = name
                            } else {
                                result += ", \(name)"
                            }
                        }
                    }
                }
            }
        } else {
            if let specialities = specialities {
                for item in specialities {
                    if let speciality = (item as? Speciality)?.name {
                        if result.isEmpty {
                            result = speciality
                        } else {
                            result += ", \(speciality)"
                        }
                    }
                }
            }
        }
        
        return result
    }
    
}
