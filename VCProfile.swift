//
//  VCProfile.swift
//  phr.kz
//
//  Created by ivan on 22/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCProfile: ViewController {

    //MARK: - VARS
    
    @IBOutlet weak var profileTable: UITableView!
    @IBOutlet weak var avatarButton: ImageButton!
    @IBOutlet weak var avatarLoaderView: UIView!
    @IBOutlet weak var avatarLoader: UIActivityIndicatorView!
    
    let showAddMID = "showAddMID"
    
    var isAvatarDownloading = false {
        didSet {
            if isAvatarDownloading {
                avatarLoaderView.isHidden = false
                avatarLoader.startAnimating()
            } else {
                avatarLoaderView.isHidden = true
                avatarLoader.stopAnimating()
            }
            
        }
    }
    
    var pointers = [Int: [String]]()
    let medIDCell = "medID"
    let lastNameCell = "lastName"
    let nameCell = "name"
    let patronymicNameCell = "patronymicName"
    let passwordCell = "password"
    let phoneCell = "phone"
    let emailCell = "email"
    let logoutCell = "logout"
    
    
    
    //MARK: - LIFE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitleView?.titleLabel.textColor = UIColor.white
        navigationTitleView?.title = "Профиль".localized
        profileTable.dataSource = self
        profileTable.delegate = self
        profileTable.contentInset = UIEdgeInsetsMake(0.0, 0.0, 15.0, 0.0)
        
        if let avatarFile = app.user?.avatar?.url {
            if let image = app.getCachedImage(path: avatarFile) {
                avatarButton.image = image
            }
        }
        
        var flag: UIImage
        
        switch app.locale.identifier {
        case "en":
            flag = #imageLiteral(resourceName: "en")
        case "kk_KZ":
            flag = #imageLiteral(resourceName: "kz")
        default:
            flag = #imageLiteral(resourceName: "ru")
        }
        
        let imageView = UIImageView(image: flag.withRenderingMode(.alwaysOriginal))
        imageView.frame = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 20.0)
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        
        let item = ImageButton(frame: imageView.frame)
        item.addSubview(imageView)
        item.imageView = imageView
        item.addTarget(self, action: #selector(switchLanguage), for: .touchUpInside)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: item)
        
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillShow:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillHide:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(reloadNote:)), name: app.notification(name: "reloadUserAfterChange"), object: nil)
        
        
        loadUserInfo()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updatePointers()
        self.profileTable.reloadData()
        self.downloadAvatarIfNeeded(file: app.user?.avatar)
    }
    
    
    
    //MARK: - FUNCS
    
    func loadUserInfo() {
        app.api.cabinet.selfuser(callback: { (json) in
            app.user?.apply(params: json)
            app.stack.save()
            self.updatePointers()
            self.profileTable.reloadData()
            self.downloadAvatarIfNeeded(file: app.user?.avatar)
        }) { (error) in
            log(error: error)
            self.downloadAvatarIfNeeded(file: app.user?.avatar)
        }
    }
    
    
    func updatePointers() {
        pointers = [Int: [String]]()
        var section = 0
        
        if app.user?.type == .some(.patient) {
            pointers[section] = [medIDCell]
            section += 1
        }
        
        pointers[section] = [lastNameCell, nameCell, patronymicNameCell]
        section += 1
        pointers[section] = [passwordCell, phoneCell, emailCell]
        section += 1
        pointers[section] = [logoutCell]
    }
    
    
    func handlerOf(reloadNote: Notification) {
        loadUserInfo()
    }
    
    
    func hideKeyboard() {
        view.endEditing(false)
    }
    
    
    func handlerOf(keyboardWillShow note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.profileTable.contentInset = UIEdgeInsetsMake(0.0, 0.0, keyboardBounds.height - self.bottomLayoutGuide.length - 64.0, 0.0)
            self.profileTable.scrollIndicatorInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardBounds.height - self.bottomLayoutGuide.length - 64.0, 0.0)
        }, completion: nil)
    }
    
    
    func handlerOf(keyboardWillHide note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.profileTable.contentInset = .zero
            self.profileTable.scrollIndicatorInsets = .zero
        }, completion: nil)
    }
    
    
    /* Log out */
    
    func logout() {
        app.ud.set(nil, forKey: app.keys.authorizationToken)
        app.ud.set(false, forKey: app.keys.authorization)
        app.stack.resetPersistentStore()
        app.delegate.transitLogout()
    }
    
    
    /* Change application language button handler */
    
    func switchLanguage() {
        let actions = UIAlertController(title: nil, message: "Выберите язык приложения".localized, preferredStyle: .actionSheet)
        
        for locale in app.locales {
            if let code = locale.languageCode {
                actions.addAction(UIAlertAction(title: locale.localizedString(forLanguageCode: code)?.capitalized, style: .default, handler: { (action) in
                    actions.dismiss(animated: true, completion: nil)
                    self.change(locale: locale)
                }))
            }
        }
        
        actions.addAction(UIAlertAction(title: "Отмена".localized, style: .cancel, handler: { (action) in
            actions.dismiss(animated: true, completion: nil)
        }))
        
        present(actions, animated: true, completion: nil)
    }
    
    
    /* Change locale */
    
    func change(locale: Locale) {
        if app.locale.identifier == locale.identifier {
            return
        }
        app.locale = locale
        
        if let tabs = app.delegate.getTabs() /*UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabs") as? UITabBarController*/ {
            tabs.selectedIndex = 2
            app.delegate.window?.rootViewController = tabs
            tabs.view.alpha = 0.0
            
            let loading = LoadingView.instance(message: app.settingLanguageMessage(forLocale: locale))
            loading.frame = UIScreen.main.bounds
            loading.alpha = 0.0
            loading.loader.startAnimating()
            app.delegate.window?.addSubview(loading)
            
            UIView.animate(withDuration: 0.4, animations: {
                loading.alpha = 1.0
                app.delegate.window?.rootViewController?.view.alpha = 0.0
            }, completion: { (finished) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                    app.delegate.window?.insertSubview(tabs.view, belowSubview: loading)
                    UIView.animate(withDuration: 0.4, animations: {
                        loading.alpha = 0.0
                        tabs.view.alpha = 1.0
                    }, completion: { (finished) in
                        loading.removeFromSuperview()
                        app.delegate.window?.rootViewController = tabs
                    })
                })
            })
        }
    }
    
    
    func saveChanges(barButton: UIBarButtonItem) {
        var params = [String: Any]()
        
        if let firstName = app.user?.firstName {
            params["firstName"] = firstName
        }
        
        if let lastName = app.user?.secondName {
            params[lastNameCell] = lastName
        }
        
        if let middleName = app.user?.patronymicName {
            params["middleName"] = middleName
        }
        
        if app.user?.type == .some(.doctor) {
            app.api.user.updateDoctor(params: params, callback: { (_) in
                self.navigationItem.rightBarButtonItem = nil
                app.alert(message: "Профиль успешно обновлен.".localized)
            }, fallback: { (error) in
                app.alert(error: error)
            })
        } else {
            app.api.user.updatePatient(params: params, callback: { (_) in
                self.navigationItem.rightBarButtonItem = nil
                app.alert(message: "Профиль успешно обновлен.".localized)
            }, fallback: { (error) in
                app.alert(error: error)
            })
        }
    }
    
    
    @IBAction func handlerOf(tapGesture: UITapGestureRecognizer) {
        hideKeyboard()
    }
    
    
    @IBAction func handlerOf(newAvaButton: UIButton) {
        if isAvatarDownloading {
            return
        }
        
        let actions = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        actions.addAction(UIAlertAction(title: "Снять фото".localized, style: .default, handler: { (action) in
            actions.dismiss(animated: true, completion: nil)
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .camera
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        actions.addAction(UIAlertAction(title: "Выбрать фото".localized, style: .default, handler: { (action) in
            actions.dismiss(animated: true, completion: nil)
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.delegate = self
                imagePicker.sourceType = .photoLibrary
                imagePicker.allowsEditing = false
                self.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        /*actions.addAction(UIAlertAction(title: "Удалить фото".localized, style: .default, handler: { (action) in
            actions.dismiss(animated: true, completion: nil)
            app.api.user.updatePatient(params: ["avatar": nil], callback: { () in
                self.avatarButton.image = #imageLiteral(resourceName: "patient-ava")
                self.isAvatarDownloading = false
            }, fallback: { (error) in
                self.isAvatarDownloading = false
                self.alert(error: error)
            })
        }))*/
        
        actions.addAction(UIAlertAction(title: "Отмена".localized, style: .cancel, handler: { (action) in
            actions.dismiss(animated: true, completion: nil)
        }))
        
        present(actions, animated: true, completion: nil)
    }
    
    
    func downloadAvatarIfNeeded(file: File?) {
        guard let path = file?.url else {
            return
        }
        
        let completion = { (image: UIImage) in
            OperationQueue.main.addOperation {
                self.avatarButton.image = image
                self.isAvatarDownloading = false
            }
        }
        
        if let image = app.getCachedImage(path: path) {
            completion(image)
        } else {
            if isAvatarDownloading {
                return
            }
            
            isAvatarDownloading = true
            
            OperationQueue().addOperation {
                app.loadImage(path: path, callback: completion, fallback: { (error) in
                    OperationQueue.main.addOperation {
                        self.isAvatarDownloading = false
                        self.alert(error: error)
                    }
                })
            }
        }
    }
    
    
    func showChangeProfile(type: ChangeProfileType) {
        let controller = VCChangeProfile(type: type)
        navigationController?.pushViewController(controller, animated: true)
    }
    
    
    
    //MARK: - NAV
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == showAddMID {
            if let controller = segue.destination as? VCAddMID {
                controller.switcher = sender as? UISwitch
            }
        }
    }
    
}



//MARK: - UITabelViewDataSoutce

extension VCProfile: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return pointers.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        
        if let count = pointers[section]?.count {
            number = count
        }
        
        return number
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var id = ""
    
        switch pointers[indexPath.section]?[indexPath.row] {
        case .some(medIDCell):
            id = "switcherCell"
        case .some(logoutCell):
            id = "logoutCell"
        case .some(passwordCell), .some(phoneCell), .some(emailCell):
            id = "detailCell"
        default:
            id = "formCell"
        }
        
        return tableView.dequeueReusableCell(withIdentifier: id, for: indexPath)
    }
    
}



//MARK: - UITableViewDelegate

extension VCProfile: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let divider = TableDividerView(style: .header)
        
        switch section {
        case 0:
            if app.user?.type == .some(.patient) {
                divider.text = "Медкарта".uppercased().localized
            } else {
                divider.text = "Личные данные".uppercased().localized
            }
        case 1:
            if app.user?.type == .some(.patient) {
                divider.text = "Личные данные".uppercased().localized
            } else {
                divider.text = "Аккаунт".uppercased().localized
            }
        case 2:
            if app.user?.type == .some(.patient) {
                divider.text = "Аккаунт".uppercased().localized
            }
        default:
            break
        }
        
        return divider
    }
    
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if app.user?.type == .some(.patient) && section == 0 {
            let divider = TableDividerView(style: .footer)
            divider.text = "Предоставить врачам доступ к вашей медицинской карте.".localized
            return divider
        } else {
            return nil
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 3 {
            return 15.0
        } else {
            return 40.0
        }
    }
    
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == 0 {
            return 30.0
        } else {
            return 15.0
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
    
        switch pointers[indexPath.section]?[indexPath.row] {
        case .some(medIDCell):
            let cell = cell as? SwitcherCell
            cell?.title = "Прикрепить к профилю".localized
            
            cell?.delegate = self
            
            if let isMCAttached = app.user?.isMCAttached {
                cell?.isOn = isMCAttached
                cell?.switcher.isEnabled = !isMCAttached
            } else {
                cell?.isOn = false
                cell?.switcher.isEnabled = false
            }
        case .some(lastNameCell):
            let cell = cell as? FormCell
            cell?.delegate = self
            cell?.leftTitle = "Фамилия".localized
            cell?.title = app.user?.secondName
        case .some(nameCell):
            let cell = cell as? FormCell
            cell?.delegate = self
            cell?.leftTitle = "Имя".localized
            cell?.title = app.user?.firstName
        case .some(patronymicNameCell):
            let cell = cell as? FormCell
            cell?.delegate = self
            cell?.leftTitle = "Отчество".localized
            cell?.title = app.user?.patronymicName
        case .some(passwordCell):
            let cell = cell as? DetailCell
            cell?.title = "Изменить пароль".localized
            cell?.rightTitle = nil
        case .some(phoneCell):
            let cell = cell as? DetailCell
            cell?.title = "Телефон".localized
            cell?.rightTitle = app.user?.phone
        case .some(emailCell):
            let cell = cell as? DetailCell
            cell?.title = "Эл. почта".localized
            cell?.rightTitle = app.user?.email
        case .some(logoutCell):
            let cell = cell as? LogoutCell
            cell?.button.addTarget(self, action: #selector(logout), for: .touchUpInside)
        default:
            break
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let pointer = pointers[indexPath.section]?[indexPath.row]
        
        switch pointer {
        case .some(passwordCell):
            performSegue(withIdentifier: app.segues.showChangePassword, sender: nil)
        case .some(phoneCell):
            showChangeProfile(type: .phone)
        case .some(emailCell):
            showChangeProfile(type: .email)
        default:
            break
        }
    }
}


extension VCProfile: FormCellDelegate {
    
    func didChange(textField: UITextField, for cell: FormCell) {
        
        if navigationItem.rightBarButtonItem == nil {
            navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохранить".localized, style: .plain, target: self, action: #selector(saveChanges(barButton:)))
        }
        
        if let indexPath = profileTable.indexPath(for: cell) {
            switch pointers[indexPath.section]?[indexPath.row] {
            case .some(lastNameCell):
                app.user?.secondName = textField.text
            case .some(nameCell):
                app.user?.firstName = textField.text
            case .some(patronymicNameCell):
                app.user?.patronymicName = textField.text
            default:
                break
            }
        }
    }
    
    func didEndEditing(textField: UITextField, for cell: FormCell) {
        
    }

}



//MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate

extension VCProfile: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            if let url = app.save(image: image) {
                isAvatarDownloading = true
                app.api.files.upload(fileUrl: url, callback: { (file) in
                    app.user?.avatar = file
                    if let id = file.id {
                        app.api.user.updatePatient(params: ["avatar": id], callback: { (_) in
                            if let path = file.url {
                                app.setCachedImage(image: image, path: path)
                                self.avatarButton.image = app.getCachedImage(path: path)
                                self.isAvatarDownloading = false
                            } else {
                                self.isAvatarDownloading = false
                            }
                        }, fallback: { (error) in
                            self.isAvatarDownloading = false
                            self.alert(error: error)
                        })
                    }
                }, fallback: { (error) in
                    self.isAvatarDownloading = false
                    self.alert(error: error)
                })
            }
        } else {
            // error message
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}



//MARK: - SwitcherCellDelegate

extension VCProfile: SwitcherCellDelegate {
    
    func valueChangedOf(switcherCell: SwitcherCell) {
        performSegue(withIdentifier: showAddMID, sender: switcherCell.switcher)
    }
    
}
