//
//  AppDelegate.swift
//  phr.kz
//
//  Created by ivan on 15/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import TwilioChatClient

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    //MARK: - VARS
    
    var window: UIWindow?
    var background: AuthBackground?
    
    
    
    //MARK: - LIFE
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        Fabric.with([Crashlytics.self])
        Bundle.set(locale: app.locale)
        setupWindow()
        
        return true
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        app.stack.save()
    }
    
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        app.stack.save()
    }
    
    
    
    //MARK: - FUNCS
    
    private func setupWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.tintColor = UIColor(hex: 0x495787)
        showBackground()
        
        
        if app.ud.bool(forKey: app.keys.notFirstLaunch) {
            if app.ud.bool(forKey: app.keys.authorization) {
                app.user = app.stack.user()
                window?.rootViewController = getTabs()
            } else {
                showBackground()
                window?.rootViewController = UIStoryboard(name: "Auth", bundle: nil).instantiateInitialViewController()
            }
        } else {
            showBackground()
            window?.rootViewController = UIStoryboard(name: "Splash", bundle: nil).instantiateInitialViewController()
        }
        
        window?.makeKeyAndVisible()
    }
    
    
    func transitToAuth() {
        guard let root = window?.rootViewController else {
            return
        }
        
        guard let login = UIStoryboard(name: "Auth", bundle: nil).instantiateInitialViewController() as? VCLogin else {
            return
        }
        
        login.view.transform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0)
        window?.addSubview(login.view)
        UIView.animate(withDuration: 0.2, animations: {
            root.view.transform = CGAffineTransform(translationX: -UIScreen.main.bounds.width, y: 0.0)
            login.view.transform = CGAffineTransform.identity
        }) { (finished) in
            if finished {
                self.window?.rootViewController = login
            }
        }
    }
    
    
    func transitLogout() {
        guard let root = window?.rootViewController else {
            return
        }
        
        guard let login = UIStoryboard(name: "Auth", bundle: nil).instantiateInitialViewController() as? VCLogin else {
            return
        }
        
        login.view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        login.view.alpha = 0.0
        window?.insertSubview(login.view, belowSubview: root.view)
        
        UIView.animate(withDuration: 0.5, animations: {
            root.view.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
            root.view.alpha = 0.0
            login.view.transform = CGAffineTransform.identity
            login.view.alpha = 1.0
        }) { (finished) in
            if finished {
                self.window?.rootViewController = login
            }
        }
    }
    
    
    func transitToStart() {
        guard let root = window?.rootViewController else {
            return
        }
        
        guard let start = getTabs() else {
            return
        }
        
        start.view.alpha = 0.0
        window?.addSubview(start.view)
        
        UIView.animate(withDuration: 0.2, animations: { 
            root.view.alpha = 0.0
            start.view.alpha = 1.0
        }) { (finished) in
            if finished {
                self.window?.rootViewController = start
            }
        }
    }
    
    
    func showBackground() {
        if background == nil {
            background = AuthBackground(frame: UIScreen.main.bounds)
        }
        
        if let background = background {
            if background.superview == nil {
                window?.insertSubview(background, at: 0)
                background.translatesAutoresizingMaskIntoConstraints = false
                
                self.window?.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "V:|-(0)-[background]-(0)-|",
                    options: .zero,
                    metrics: nil,
                    views: ["background": background]))
                
                self.window?.addConstraints(NSLayoutConstraint.constraints(
                    withVisualFormat: "H:|-(0)-[background]-(0)-|",
                    options: .zero,
                    metrics: nil,
                    views: ["background": background]))
            }
            
            background.showPattern(animated: false)
        }
    }
    
    
    func hideBackground() {
        background?.removeFromSuperview()
    }
    
    
    func getTabs() -> UITabBarController? {
        let story = UIStoryboard(name: "Main", bundle: nil)
        let tabs = story.instantiateViewController(withIdentifier: "tabs") as? UITabBarController
        
        var controllers = [UIViewController]()
        controllers.append(story.instantiateViewController(withIdentifier: "consultationsNav"))
        
        if app.user?.type == .some(.patient) {
            controllers.append(story.instantiateViewController(withIdentifier: "secondOpinionNav"))
        }
        
        controllers.append(story.instantiateViewController(withIdentifier: "profileNav"))
        tabs?.setViewControllers(controllers, animated: false)
        return tabs
    }

}

