//
//  AttachmentCell.swift
//  phr.kz
//
//  Created by ivan on 31/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

protocol AttachmentCellDelegate {
    func didTap(deleteItem: ImageButton, cell: AttachmentCell)
}

class AttachmentCell: UICollectionViewCell {
    
    //MARK: - VARS
    
    @IBOutlet weak var imageView: UIImageView!
    var delegate: AttachmentCellDelegate?
    
    var image: UIImage? {
        get {
            return imageView.image
        }
        
        set {
            imageView.image = newValue
        }
    }
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(deleteItem: ImageButton) {
        delegate?.didTap(deleteItem: deleteItem, cell: self)
    }
    
}
