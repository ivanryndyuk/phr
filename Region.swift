//
//  Region.swift
//  phr.kz
//
//  Created by ivan on 07/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import CoreData

class Region: NSManagedObject {

    //MARK: - VARS
    
    @NSManaged var id: String?
    @NSManaged var name: String?
    
    
    
    //MARK: - FUNCS
    
    func apply(json: JsonObject) {
        id = json["id"] as? String
        name = json["name"] as? String
    }
    
    
    class func list(fromJson json: JsonArray) -> [Region] {
        var result = [Region]()
        
        if app.stack.removeAll(forEntity: Region.self) {
            for case let item as JsonObject in json {
                if let region = app.stack.region(forJson: item) {
                    result.append(region)
                }
            }
        } else {
            result = app.stack.items(forEntity: Region.self)
        }
        
        return result
    }
}
