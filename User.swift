//
//  User.swift
//  phr.kz
//
//  Created by ivan on 07/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import CoreData

class User: NSManagedObject {

    //MARK: - VARS
    
    @NSManaged var key: String
    @NSManaged var id: String
    @NSManaged var firstName: String?
    @NSManaged var secondName: String?
    @NSManaged var patronymicName: String?
    @NSManaged var email: String?
    @NSManaged var phone: String?
    @NSManaged var avatar: File?
    
    @NSManaged var chatSID: String?
    @NSManaged var chatIdentity: String?
    @NSManaged var chatServiceSID: String?
    @NSManaged var chatUserName: String?
    @NSManaged var typeRaw: NSNumber
    @NSManaged var isMCAttached: Bool
    
    var type: UserType {
        get {
            return UserType.typeWith(raw: typeRaw)
        }
        
        set {
            typeRaw = NSNumber(integerLiteral: newValue.hashValue)
        }
    }
    
    
    
    //MARK: - LIFE
    
    /* Applies params from json. */
    
    func apply(params: JsonObject) {
        if let id = params["id"] as? String {
            self.id = id
        }
        
        if let email = params["email"] as? String {
            self.email = email
        }
        
        if let phone = params["phone"] as? String {
            self.phone = phone
        }
        
        if let secondName = params["lastName"] as? String {
            self.secondName = secondName
        }
        
        if let firstName = params["firstName"] as? String {
            self.firstName = firstName
        }
        
        if let patronymicName = params["middleName"] as? String {
            self.patronymicName = patronymicName
        }
        
        if let type = params["entity_type"] as? String {
            if type == "doctor" {
                self.type = .doctor
            } else {
                self.type = .patient
            }
        }
        
        if let chatUser = params["chatUser"] as? JsonObject {
            chatSID = chatUser["sid"] as? String
            chatIdentity = chatUser["identity"] as? String
            chatServiceSID = chatUser["serviceSid"] as? String
            chatUserName = chatUser["name"] as? String
        }
        
        if let avatarJson = params["avatar"] as? JsonObject {
            avatar = app.stack.file(forJson: avatarJson)
        }
        
        if let isMCAttached = params["medical_card"] as? Bool {
            self.isMCAttached = isMCAttached
        }
    }
    
    
    /* Reset all params and add new id. */
    
    func reset(newId: String) {
        id = newId
        firstName = nil
        secondName = nil
        patronymicName = nil
    }
    
}



//MARK: - User type

enum UserType {
    case patient, doctor
    
    static func typeWith(raw: NSNumber) -> UserType {
        if raw.intValue == 1 {
            return .doctor
        } else {
            return .patient
        }
    }
}
