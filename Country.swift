//
//  Country.swift
//  phr.kz
//
//  Created by ivan on 08/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class Country: NSObject {
    
    //MARK: - VARS
    
    var countryCode: String
    var title: String
    var telephoneCode: String
    
    var image: UIImage? {
        get {
            switch countryCode {
            case "ru":
                return #imageLiteral(resourceName: "ru")
            case "kz":
                return #imageLiteral(resourceName: "kz")
            case "es":
                return #imageLiteral(resourceName: "es")
            default:
                return nil
            }
        }
    }
    
    
    
    //MARK: - LIFE
    
    init(countryCode: String, title: String, telephoneCode: String) {
        self.countryCode = countryCode
        self.title = title
        self.telephoneCode = telephoneCode
    }

}
