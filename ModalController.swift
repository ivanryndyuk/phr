//
//  ModalController.swift
//  phr.kz
//
//  Created by ivan on 04/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ModalController: UIViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var blurView: UIVisualEffectView?
    @IBOutlet weak var contentView: UIView?
    @IBOutlet weak var contentHeight: NSLayoutConstraint?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
