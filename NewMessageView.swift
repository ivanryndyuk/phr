//
//  NewMessageView.swift
//  phr.kz
//
//  Created by ivan on 24/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

protocol NewMessageViewDelegate {
    func didTap(additionsButton: UIButton)
    func didTap(sendButton: UIButton, text: String?)
    func didChangeEditing(textView: UITextView)
    func didTapReturnKey(textView: UITextView)
}

class NewMessageView: UIView {
    
    //MARK: - VARS
    
    var container = NewMessageViewContainer.instance()
    var delegate: NewMessageViewDelegate?
    var height: NSLayoutConstraint?
    var attachments = [UIImage]()
    let reuseId = "attachmentCell"
    let placeholder = "Введите текст".localized
    var placeholderAttrs = [String: Any]()
    var textAttrs = [String: Any]()
    
    
    var sendButtonHidden = true {
        willSet {
            //container.sendButton.isHidden = newValue
            //container.voiceMessageButton.isHidden = !newValue
        }
    }
    
    var isAttachmentsHidden = true {
        didSet {
            if isAttachmentsHidden {
                container.attachmentsHeight.constant = 0.0
            } else {
                container.attachmentsHeight.constant = 60.0
            }
            updateHeight()
        }
    }
    
    
    
    //MARK: - LIFE
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupContainer()
        designSubviews()
        
        /*let ps = NSMutableParagraphStyle()
        ps.lineBreakMode = .byWordWrapping
        ps.lineSpacing = 0
        ps.minimumLineHeight = 22.0
        ps.maximumLineHeight = 22.0
        
        let font = UIFont.with(name: app.theme.fontMuseoSansCyrl, size: 15.0)
        
        placeholderAttrs[NSParagraphStyleAttributeName] = ps
        placeholderAttrs[NSFontAttributeName] = font
        placeholderAttrs[NSForegroundColorAttributeName] = UIColor(r: 144, g: 140, b: 132, a: 0.5)
        
        textAttrs[NSParagraphStyleAttributeName] = ps
        textAttrs[NSFontAttributeName] = font
        textAttrs[NSForegroundColorAttributeName] = UIColor(hex: 030303)*/
    }
    
    
    
    //MARK: - FUNCS
    
    func setupContainer() {
        addSubview(container)
        container.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[container]-0-|",
            options: .zero,
            metrics: nil,
            views: ["container": container]))
        
        addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[container]-0-|",
            options: .zero,
            metrics: nil,
            views: ["container": container]))
    }
    
    
    func designSubviews() {
        container.textView.delegate = self
        container.sendButton.addTarget(self, action: #selector(handlerOf(sendButton:)), for: .touchUpInside)
        container.actionsButton.addTarget(self, action: #selector(handlerOf(actionsButton:)), for: .touchUpInside)
        
        container.attachmentsCollection.register(UINib(nibName: "AttachmentCell", bundle: nil), forCellWithReuseIdentifier: reuseId)
        container.attachmentsCollection.contentInset = UIEdgeInsets(top: 0.0, left: 20.0, bottom: 0.0, right: 20.0)
        container.attachmentsCollection.dataSource = self
        container.attachmentsCollection.delegate = self
    }
    
    
    func reset() {
        attachments = [UIImage]()
        container.attachmentsCollection.reloadData()
        isAttachmentsHidden = true
        sendButtonHidden = true
        
        if container.textView.isFirstResponder {
            container.textView.text = ""
        } else {
            container.textView.text = placeholder
            container.textView.textColor = UIColor(r: 144, g: 140, b: 132, a: 0.5)
        }
        
        updateHeight()
    }
    
    
    func handlerOf(sendButton: UIButton) {
        delegate?.didTap(sendButton: sendButton, text: container.textView.text)
    }
    
    
    func handlerOf(actionsButton: UIButton) {
        delegate?.didTap(additionsButton: actionsButton)
    }
    
    
    func updateHeight() {
        var textHeight = container.textView.intrinsicContentSize.height
        textHeight = max(40.0, textHeight)
        textHeight = min(120.0, textHeight)
        
        var totalHeight = textHeight + 12.0
        
        if !isAttachmentsHidden {
            totalHeight += 60.0
        }
        
        if self.height?.constant != .some(totalHeight) {
            UIView.animate(withDuration: 0.2, animations: {
                self.height?.constant = totalHeight
                self.superview?.layoutIfNeeded()
                self.layoutIfNeeded()
            })
        }
    }
    
    
    func add(attachment: UIImage) {
        if isAttachmentsHidden {
            isAttachmentsHidden = false
        }
        
        container.attachmentsCollection.performBatchUpdates({ 
            self.attachments.append(attachment)
            self.container.attachmentsCollection.reloadSections(IndexSet(integer: 0))
        }, completion: nil)
    }
    
    
}



//MARK: - UICollectionViewDataSource

extension NewMessageView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return attachments.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath)
    }
    
}



//MARK: - UICollectionViewDelegate

extension NewMessageView: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as? AttachmentCell
        cell?.image = attachments[indexPath.item]
        cell?.delegate = self
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 60.0, height: 60.0)
    }
    
}



//MARK: - AttachmentCellDelegate

extension NewMessageView: AttachmentCellDelegate {
    
    func didTap(deleteItem: ImageButton, cell: AttachmentCell) {
        if let indexPath = container.attachmentsCollection.indexPath(for: cell) {
            container.attachmentsCollection.performBatchUpdates({ 
                self.attachments.remove(at: indexPath.row)
                self.container.attachmentsCollection.reloadSections(IndexSet(integer: 0))
            }, completion: { finished in
                if self.attachments.count == 0 {
                    self.isAttachmentsHidden = true
                }
            })
        }
    }
    
}



//MARK: - UITextViewDelegate

extension NewMessageView: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeholder {
            textView.text = ""
            textView.textColor = UIColor(hex: 0x030303)
            //textView.attributedText = NSAttributedString(string: "", attributes: textAttrs)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = placeholder
            textView.textColor = UIColor(r: 144, g: 140, b: 132, a: 0.5)
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        updateHeight()
        delegate?.didChangeEditing(textView: textView)
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        return true
    }
    
}



//MARK: - NewMessageViewContainer

class NewMessageViewContainer: UIView {
    
    //MARK: - VARS
    
    @IBOutlet weak var voiceMessageButton: UIButton!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var actionsButton: UIButton!
    @IBOutlet weak var textView: CenteredTextView!
    @IBOutlet weak var attachmentsCollection: UICollectionView!
    @IBOutlet weak var attachmentsHeight: NSLayoutConstraint!
    
    var isActionsViewHidden = true
    
    
    
    //MARK: - LIFE
    
    class func instance() -> NewMessageViewContainer {
        if let instance = Bundle.main.loadNibNamed("NewMessageView", owner: nil, options: nil)?.first as? NewMessageViewContainer {
            return instance
        } else {
            return NewMessageViewContainer()
        }
    }

}
