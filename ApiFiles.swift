//
//  ApiFiles.swift
//  phr.kz
//
//  Created by ivan on 09/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ApiFiles: ApiSection {

    //MARK: - FUNCS
    
    func file(id: String, callback: @escaping Block<File>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .get, path: "files/\(id)", token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                if let file = app.stack.file(forJson: data) {
                    callback(file)
                } else {
                    fallback(Api.Errors.wrongJsonFormat)
                }
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
    }
    
    
    func upload(fileUrl url: URL, channelSid: String? = nil, callback: @escaping Block<File>, fallback: @escaping Block<Error>) {
        processFile(atUrl: url, callback: { (filename, data, mime) in
            var comps = URLComponents()
            if let sid = channelSid {
                comps.path = "api/\(app.localeCode)/files/\(sid)"
            } else {
                comps.path = "api/\(app.localeCode)/files"
            }
            
            guard let url = comps.url(relativeTo: self.parent.baseURL) else {
                fallback(NSError.with(description: "".localized))
                return
            }
            
            let boundary = "Boundary-\(UUID().uuidString)"
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            request.setValue(self.parent.token, forHTTPHeaderField: "x-token")
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            request.httpBody = self.fileUploadRequestBody(parameters: [:], boundary: boundary, data: data, mimeType: mime, filename: filename)
            
            self.parent.pass(request: request, callback: { (json) in
                if let data = json["data"] as? JsonObject {
                    if let file = app.stack.file(forJson: data) {
                        callback(file)
                    } else {
                        fallback(Api.Errors.wrongJsonFormat)
                    }
                } else {
                    fallback(Api.Errors.wrongJsonFormat)
                }
                
            }, fallback: fallback)
        }, fallback: fallback)
    }
    
    
    /* Get file info and attributes at specified url. */
    
    func processFile(atUrl url: URL, callback: @escaping Block<(String, Data, String)>, fallback: @escaping Block<Error>) {
        guard let filename = url.absoluteString.components(separatedBy: "/").last else {
            fallback(NSError.with(description: "Ошибка чтения файла.".localized))
            return
        }
        
        let fileRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 0.1)
        
        let task = URLSession(configuration: .default).dataTask(with: fileRequest) { (data, response, error) in
            if let error = error {
                print(error)
                fallback(error)
            } else {
                guard let response = response else {
                    fallback(NSError.with(description: "Ошибка чтения файла.".localized))
                    return
                }
                
                guard let mime = response.mimeType else {
                    fallback(NSError.with(description: "Ошибка чтения файла.".localized))
                    return
                }
                
                guard let data = data else {
                    fallback(NSError.with(description: "Ошибка чтения файла.".localized))
                    return
                }
                
                callback((filename, data, mime))
            }
        }
        
        task.resume()
    }
    
    
    /* Create body for file upload request. */
    
    func fileUploadRequestBody(parameters: [String: Any], boundary: String, data: Data, mimeType: String, filename: String) -> Data {
        let body = NSMutableData()
        
        let boundaryPrefix = "--\(boundary)\r\n"
        
        for (key, value) in parameters {
            body.append(string: boundaryPrefix)
            body.append(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
            body.append(string: "\(value)\r\n")
        }
        
        body.append(string: boundaryPrefix)
        body.append(string: "Content-Disposition: form-data; name=\"file\"; filename=\"\(filename)\"\r\n")
        body.append(string: "Content-Type: \(mimeType)\r\n\r\n")
        body.append(data)
        body.append(string: "\r\n--\(boundary)--\r\n")
        
        return body as Data
    }
    
    
    /* Order file translation. */
    
    func translate(fileId: String, messageSid: String, callback: @escaping Block<Translation>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .post, path: "files/translate/\(fileId)", params: ["messageSid": messageSid], token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                if let translation = Translation(json: data) {
                    callback(translation)
                } else {
                    fallback(Api.Errors.wrongJsonFormat)
                }
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
        
        
        
        /*
        "id": "0180b6cd-7668-4237-b06e-adc265ffabad",
        "status": {
            "name": "Отправлен на перевод"
        },
        "sourceFile": {
            "title": "shot_12-06-17_at_05.08.18.jpg",
            "fileSize": 372008,
            "fileType": "image/jpeg",
            "imageWidth": 1668,
            "imageHeight": 2500,
            "videoProcess": null,
            "duration": null,
            "url": "http://storage.phrkz.kr.digital/phrkz/8/7/18a84d23-99d1-43ee-bc25-f5af3a9750c2.jpg",
            "id": "4a1ced7a-7cfb-468b-8770-37ec6704d5bc"
        },
        "translatedFile": null*/
    }
    
}
