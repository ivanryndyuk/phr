//
//  VCSignup.swift
//  phr.kz
//
//  Created by ivan on 19/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCSignup: UIViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var topBorder: UIView!
    @IBOutlet weak var bottomOffset: NSLayoutConstraint!
    @IBOutlet weak var formBlock: UIView!
    @IBOutlet weak var confirmBlock: UIView!
    @IBOutlet weak var firstNameField: UITextField!
    @IBOutlet weak var secondNameField: UITextField!
    @IBOutlet weak var patronymicNameField: UITextField!
    @IBOutlet weak var phoneField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var passwordAgainField: UITextField!
    @IBOutlet weak var codeField: UITextField!
    @IBOutlet weak var firstNameBorder: UIView!
    @IBOutlet weak var secondNameBorder: UIView!
    @IBOutlet weak var patronymicNameBorder: UIView!
    @IBOutlet weak var phoneBorder: UIView!
    @IBOutlet weak var codeBorder: UIView!
    @IBOutlet weak var passwordBorder: UIView!
    @IBOutlet weak var passwordAgainBorder: UIView!
    @IBOutlet weak var blockHeight: NSLayoutConstraint!
    @IBOutlet weak var countryButton: UIButton!
    @IBOutlet weak var countryCodeLabel: UILabel!
    
    var fields = [UITextField]()
    var borders = [UIView]()
    let tranimator = Tranimator(style: .slideUp)
    var keyboardBounds = CGRect.zero
    
    var login = ""
    var password = ""
    var country = app.countries[0]
    
    
    
    //MARK: - LIFE

    override func viewDidLoad() {
        super.viewDidLoad()
        fields = [firstNameField, secondNameField, patronymicNameField, phoneField, passwordField, passwordAgainField]
        borders = [firstNameBorder, secondNameBorder, patronymicNameBorder, phoneBorder, passwordBorder, passwordAgainBorder]
        
        for field in fields {
            field.delegate = self
        }
        
        codeField.delegate = self
        blockHeight.constant = 0
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillShow:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillHide:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        app.nc.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if blockHeight.constant == 0 {
            blockHeight.constant = UIScreen.main.bounds.height - 44.5 - topLayoutGuide.length
        }
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(backItem: UIButton) {
        hideKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func handlerOf(tapGesture: UITapGestureRecognizer) {
        hideKeyboard()
    }
    
    
    @IBAction func handlerOf(sendPassButton: Button) {
        signup()
    }
    
    
    @IBAction func handlerOf(confirmButton: Button) {
        confirm()
    }
    
    
    func hideKeyboard() {
        view.endEditing(false)
    }
    
    
    func handlerOf(keyboardWillShow note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = self.keyboardBounds.height - self.bottomLayoutGuide.length
            if self.formBlock.isHidden {
                self.blockHeight.constant = max(UIScreen.main.bounds.height - 44.5 - self.keyboardBounds.height - self.topLayoutGuide.length, 140.0)
            } else {
                self.blockHeight.constant = max(UIScreen.main.bounds.height - 44.5 - self.keyboardBounds.height - self.topLayoutGuide.length, 510.0)
            }
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func handlerOf(keyboardWillHide note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = 0.0
            if self.formBlock.isHidden {
                self.blockHeight.constant = max(UIScreen.main.bounds.height - 44.5 - self.topLayoutGuide.length, 140.0)
            } else {
                self.blockHeight.constant = max(UIScreen.main.bounds.height - 44.5 - self.topLayoutGuide.length, 510.0)
            }
            self.view.layoutIfNeeded()
        }, completion: { finished in
            if finished {
                self.keyboardBounds = .zero
            }
        })
    }
    
    
    @IBAction func chooseCountry(button: UIButton) {
        let actions = UIAlertController(title: nil, message: "Выберите страну".localized, preferredStyle: .actionSheet)
        
        for country in app.countries {
            actions.addAction(UIAlertAction(title: country.title, style: .default, handler: { (action) in
                actions.dismiss(animated: true, completion: nil)
                self.change(country: country)
            }))
        }
        
        actions.addAction(UIAlertAction(title: "Отмена".localized, style: .cancel, handler: { (action) in
            actions.dismiss(animated: true, completion: nil)
        }))
        
        present(actions, animated: true, completion: nil)
    }
    
    
    func change(country: Country) {
        self.country = country
        countryCodeLabel.text = country.telephoneCode
        countryButton.setImage(country.image, for: .normal)
    }
    
    
    func showConfirm() {
        confirmBlock.isHidden = false
        var constant = UIScreen.main.bounds.height - 44.5 - topLayoutGuide.length
        
        if self.keyboardBounds != .zero {
            constant -= self.keyboardBounds.height
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            self.formBlock.alpha = 0.0
            self.confirmBlock.alpha = 1.0
            self.blockHeight.constant = constant
            self.view.layoutIfNeeded()
        }) { (finished) in
            if finished {
                self.formBlock.isHidden = true
                self.codeField.becomeFirstResponder()
            }
        }
    }
    
    
    func confirm() {
        guard let code = codeField.text else {
            app.alert(message: "Укажите код из сообщения.".localized)
            return
        }
        
        if code.isEmpty {
            app.alert(message: "Укажите код из сообщения.".localized)
            return
        }
        
        guard let id = app.user?.id else {
            app.alert(message: "У нового пользователя отсутствует идентификатор. Обратитесь в службу поддержки.".localized)
            return
        }
        
        print(id)
        print(code)
        
        app.api.user.confirm(code: code, id: id, action: .registration, callback: { (success) in
            self.showSuccess()
        }) { (error) in
            app.alert(error: error)
        }
    }
    
    
    func signup() {
        guard let phone = phoneField.text else {
            app.alert(message: "Укажите номер телефона.".localized)
            return
        }
        
        if phone.isEmpty {
            app.alert(message: "Укажите номер телефона.".localized)
            return
        }
        
        if !app.validator.validate(phone: "\(country.telephoneCode)\(phone)") {
            app.alert(message: "Проверьте правильность номера телефона.".localized)
            return
        }
        
        guard let password = passwordField.text else {
            app.alert(message: "Укажите пароль.".localized)
            return
        }
        
        if password.isEmpty {
            app.alert(message: "Укажите пароль.".localized)
            return
        }
        
        guard let passwordAgain = passwordAgainField.text else {
            app.alert(message: "Укажите пароль ещё раз.".localized)
            return
        }
        
        if passwordAgain.isEmpty {
            app.alert(message: "Укажите пароль ещё раз.".localized)
            return
        }
        
        if password != passwordAgain {
            app.alert(message: "Пароль и подтверждение не совпадают.".localized)
            return
        }
        
        self.login = "\(country.telephoneCode)\(phone)"
        self.password = password
        
        app.api.user.register(type: "patient", login: login, password: password, callback: { (user) in
            user.firstName = self.firstNameField.text
            user.secondName = self.secondNameField.text
            user.patronymicName = self.patronymicNameField.text
            app.user = user
            app.stack.save()
            self.showConfirm()
        }) { (error) in
            app.alert(error: error)
        }
    }
    
    
    func showSuccess() {
        let alert = UIAlertController(title: nil, message: "Поздравляем, вы успешно зарегистрировали личный кабинет пациента! Теперь вы можете войти.".localized, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (action) in
            self.hideKeyboard()
            
            if !self.login.isEmpty && !self.password.isEmpty {
                app.api.user.authorize(login: self.login, password: self.password, callback: {
                    self.dismiss(animated: true, completion: { 
                        app.delegate.transitToStart()
                    })
                }, fallback: { (error) in
                    log(error: error)
                    self.dismiss(animated: true, completion: nil)
                })
            } else {
                self.dismiss(animated: true, completion: nil)
            }
            
        }))
        present(alert, animated: true, completion: nil)
    }
    
    
    
    //MARK: - NAV
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        hideKeyboard()
        
        if segue.identifier == app.segues.presentConfirmRegistrantion {
            segue.destination.transitioningDelegate = self
            segue.destination.modalPresentationStyle = .custom
        }
    }

}



//MARK: - UITextFieldDelegate

extension VCSignup: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if let i = fields.index(of: textField) {
            for (j, border) in borders.enumerated() {
                if i == j {
                    border.backgroundColor = UIColor(hex: 0x79EDBA)
                } else {
                    border.backgroundColor = UIColor(r: 209, g: 204, b: 209, a: 0.3)
                }
            }
        } else if textField == codeField {
            codeBorder.backgroundColor = UIColor(hex: 0x79EDBA)
        }
    }
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let i = fields.index(of: textField) {
            borders[i].backgroundColor = UIColor(r: 209, g: 204, b: 209, a: 0.3)
        } else {
            codeBorder.backgroundColor = UIColor(r: 209, g: 204, b: 209, a: 0.3)
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let i = fields.index(of: textField) {
            if i == fields.count - 1 {
                signup()
            } else {
                fields[i + 1].becomeFirstResponder()
            }
        } else if textField == codeField {
            confirm()
        }
        
        return true
    }
    
}



//MARK: - ScrollViewDelegate

extension VCSignup: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > 0 {
            if topBorder.alpha == 0.0 {
                UIView.animate(withDuration: 0.2, animations: { 
                    self.topBorder.alpha = 1.0
                })
            }
        } else {
            if topBorder.alpha == 1.0 {
                UIView.animate(withDuration: 0.2, animations: { 
                    self.topBorder.alpha = 0.0
                })
            }
        }
    }
    
}



//MARK: - UIViewControllerTransitioningDelegate

extension VCSignup: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        tranimator.presenting = true
        return tranimator
    }
    
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        tranimator.presenting = false
        return tranimator
    }
    
}
