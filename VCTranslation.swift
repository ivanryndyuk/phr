//
//  VCTranslation.swift
//  phr.kz
//
//  Created by ivan on 13/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import TwilioChatClient

class VCTranslation: ModalController {
    
    //MARK: - VARS
    
    var message: Message?
    var channel: TCHChannel?
    
    

    //MARK: - FUNCS
    
    @IBAction func handlerOf(closeButton: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func handlerOf(orderButton: UIButton) {
        translate()
    }
    
    
    func translate() {
        if let attachments = message?.attributes["attachments"] as? JsonArray {
            if attachments.count > 0 {
                if let original = (attachments[0] as? JsonObject)?["original"] as? JsonObject {
                    if let id = original["id"] as? String {
                        if let messageSid = message?.sid {
                            app.api.files.translate(fileId: id, messageSid: messageSid, callback: { (translation) in
                                
                                let alert = UIAlertController(title: nil, message: "Запрос на перевод успешно отправлен.".localized, preferredStyle: .alert)
                                
                                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (action) in
                                    self.dismiss(animated: true, completion: nil)
                                }))
                                
                                self.present(alert, animated: true, completion: nil)
                                
                            }, fallback: { (error) in
                                app.alert(error: error)
                            })
                        }
                    }
                }
            }
        }
    }

}
