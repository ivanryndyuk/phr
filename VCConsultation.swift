//
//  VCConsultation.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import TwilioChatClient
import AudioToolbox
import SafariServices

class VCConsultation: ViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var bottomOffset: NSLayoutConstraint!
    @IBOutlet weak var messagesTable: UITableView!
    @IBOutlet weak var topOffset: NSLayoutConstraint!
    @IBOutlet weak var newMessageView: NewMessageView!
    @IBOutlet weak var newMessageViewHeight: NSLayoutConstraint!
    @IBOutlet weak var additionsView: MessageAdditionsView!
    @IBOutlet weak var additionsOffset: NSLayoutConstraint!
    @IBOutlet weak var checkList: CheckListContainer!
    @IBOutlet weak var checkListHeight: NSLayoutConstraint!
    @IBOutlet weak var conclusionButton: ImageButton!
    @IBOutlet weak var contractButton: ImageButton!
    @IBOutlet weak var aboutButton: ImageButton!
    @IBOutlet weak var complaintButton: ImageButton!
    
    let tabsId = "tabs"
    let reuseId = "messageCell"
    let df = DateFormatter()
    let animator = Tranimator(style: .modal)
    var isLoading = false
    
    var consultation: Consultation!
    var chat: TCHChannel?
    var messages = [Message]()
    var membersNames = [String: String]()
    var messagesColors = [UIColor(hex: 0xFAEF9E), UIColor(hex: 0xE1F7B3), UIColor(hex: 0xB1EAFF), UIColor(hex: 0xFFCDA4)]
    var membersColors = [String: UIColor]()
    var consumptionIndexes = [String: NSNumber]()
    var cachedImages = [String: UIImage]()
    var pendingImages = [String: UIImage]()
    var translatedIndexPaths = [IndexPath]()
    
    let messageAttrs = [NSFontAttributeName: UIFont.with(name: app.theme.fontMuseoSansCyrl, size: 15.0)]
    let messageTimeAttrs = [NSFontAttributeName: UIFont.with(name: app.theme.fontLatoRegular, size: 10.0)]
    var messageSpaceWidth: CGFloat = 0.0
    
    
    
    //MARK: - LIFE

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if consultation == nil {
            navigationController?.popViewController(animated: false)
            return
        }
        
        if let members = consultation.channel?.members {
            var colorIndex = 0
            
            for member in members {
                membersNames[member.identity] = member.name
                
                if app.user?.chatIdentity == .some(member.identity) {
                    membersColors[member.identity] = UIColor(hex: 0xF3F4F1)
                } else {
                    if colorIndex == messagesColors.count {
                        colorIndex = 0
                    }
                    
                    membersColors[member.identity] = messagesColors[colorIndex]
                    colorIndex += 1
                }
            }
        }
        
        navigationTitleView?.titleColor = UIColor(hex: 0x344758)
        navigationTitleView?.isDetailHidden = false
        navigationTitleView?.delegate = self
        navigationTitleView?.title = consultation?.getTitle()
        navigationTitleView?.subtitle = consultation?.getSubtitle()
        
        if consultation.isKind(of: Contract.self) {
            conclusionButton.isHidden = true
            contractButton.isHidden = false
        } else if let so = consultation as? SO {
            conclusionButton.isHidden = so.conclusion == nil
            contractButton.isHidden = true
        }
        
        setupCheckList()
        chat?.delegate = self
        
        if app.user?.type == .some(.doctor) {
            aboutButton.isHidden = true
            complaintButton.isHidden = true
            conclusionButton.isHidden = true
            
            if consultation.isKind(of: SO.self) {
                navigationTitleView?.isDetailHidden = true
                navigationTitleView?.delegate = nil
            }
        } else {
            let imageView = UIImageView(image: #imageLiteral(resourceName: "doctor-ava").withRenderingMode(.alwaysOriginal))
            imageView.frame = CGRect(x: 0.0, y: 0.0, width: 30.0, height: 30.0)
            imageView.radius = 15.0
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            
            let item = ImageButton(frame: imageView.frame)
            item.addSubview(imageView)
            item.imageView = imageView
            item.addTarget(self, action: #selector(presentDoctor), for: .touchUpInside)
            
            navigationItem.rightBarButtonItem = UIBarButtonItem(customView: item)
        }
        
        navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        
        messagesTable.register(UINib(nibName: "MessageCell", bundle: nil), forCellReuseIdentifier: reuseId)
        messagesTable.dataSource = self
        messagesTable.delegate = self
        
        df.dateFormat = "HH:mm"
        newMessageView.height = newMessageViewHeight
        newMessageView.delegate = self
        additionsView.delegate = self
        
        messageSpaceWidth = "_".widthForHeight(height: 16.0, attributes: messageAttrs)
        
        setupChannel()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = UIColor(hex: 0xFFFFFF)
        navigationController?.navigationBar.tintColor = UIColor(hex: 0x495787)
        (navigationController as? NavigationController)?.useLightStatusBar = false
        
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillShow:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillHide:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        app.nc.addObserver(self, selector: #selector(applicationWillEnterforeground(note:)), name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(channelDeleted:)), name: app.notification(name: app.keys.channelDeletedNote), object: nil)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        app.nc.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        app.nc.removeObserver(self, name: Notification.Name.UIApplicationWillEnterForeground, object: nil)
        app.nc.removeObserver(self, name: Notification.Name(rawValue: app.keys.channelDeletedNote), object: nil)
        consultation.unconsumedMessagesCount = 0
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(backItem: BarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func handlerOf(tapGesture: UITapGestureRecognizer) {
        hideKeyboard()
    }
    
    
    func handlerOf(channelDeleted note: Notification) {
        if let channel = note.userInfo?["channel"] as? TCHChannel {
            if chat?.sid == .some(channel.sid) {
                navigationController?.popViewController(animated: true)
            }
        }
    }
    
    
    func handlerOf(checkListButton: ImageButton) {
        var height: CGFloat
        
        if checkList.isExpanded {
            height = 44.0
            checkListButton.image = #imageLiteral(resourceName: "small-arrow-down")
        } else {
            height = mainView.frame.height
            checkListButton.image = #imageLiteral(resourceName: "small-arrow-up")
        }
        
        UIView.animate(withDuration: 0.2, animations: { 
            self.checkListHeight.constant = height
            self.view.layoutIfNeeded()
        }) { (finished) in
            self.checkList.isExpanded = !self.checkList.isExpanded
        }
    }
    
    
    func applicationWillEnterforeground(note: Notification) {
        setupChannel()
    }
    
    
    func setupCheckList() {
        checkList.checkListButton.addTarget(self, action: #selector(handlerOf(checkListButton:)), for: .touchUpInside)
        checkList.viewController = self
        checkList.canEdit = app.user?.type == .some(.doctor)
        
        if let so = consultation as? SO {
            if let items = so.checkListItems {
                if items.count > 0 {
                    checkList.isHidden = false
                    messagesTable.contentInset = UIEdgeInsetsMake(44.0, 0.0, 0.0, 0.0)
                    checkList.update(so: so)
                    return
                }
                
            }
        }
        
        checkList.isHidden = true
        messagesTable.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
    }
    
    
    func setupChannel() {
        if let chat = consultation.chat {
            add(chat: chat)
        } else if let sid = consultation.channel?.sid {
            add(chatSID: sid)
        }
    }
    
    
    func add(chat: TCHChannel) {
        self.chat = chat
        chatAdded()
    }
    
    
    func add(chatSID: String) {
        app.twilio.channel(withSID: chatSID, callback: { (chat) in
            self.chat = chat
            self.chatAdded()
        }, fallback: { (error) in
            app.alert(error: error)
        })
    }
    
    
    func chatAdded() {
        self.chat?.delegate = self
        loadConsumptionIndexes {
            self.messages.removeAll()
            self.loadNextMessages()
        }
    }
    
    
    func hideKeyboard() {
        view.endEditing(false)
    }
    
    
    func slideBack() {
        let consultations = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: tabsId)
        
        consultations.view.transform = CGAffineTransform(translationX: -UIScreen.main.bounds.width, y: 0.0)
        app.delegate.window?.addSubview(consultations.view)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.navigationController?.view.transform = CGAffineTransform(translationX: UIScreen.main.bounds.width, y: 0.0)
            consultations.view.transform = CGAffineTransform.identity
        }) { (finished) in
            if finished {
                app.delegate.window?.rootViewController = consultations
            }
        }
    }
    
    
    func handlerOf(keyboardWillShow note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = keyboardBounds.height - self.bottomLayoutGuide.length
            self.view.layoutIfNeeded()
        }, completion: { finished in
            self.scrollToBottom()
        })
    }
    
    
    func handlerOf(keyboardWillHide note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = 0.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func loadConsumptionIndexes(completion: @escaping Block<Void>) {
        if let chat = chat {
            var blockReference: TCHMemberPaginatorCompletion?
            
            let block: TCHMemberPaginatorCompletion = { (result: TCHResult?, paginator: TCHMemberPaginator?) in
                if let paginator = paginator {
                    let members = paginator.items()
                    for member in members {
                        self.consumptionIndexes[member.identity] = member.lastConsumedMessageIndex
                    }
                    
                    if paginator.hasNextPage() {
                        if let  blockReference = blockReference {
                            paginator.requestNextPage(completion: blockReference)
                        }
                    }
                }
            }
            
            blockReference = block
            chat.members.members(completion: block)
            completion()
        } else {
            completion()
        }
    }
    
    
    func loadNextMessages() {
        if isLoading {
            return
        }
        
        isLoading = true
        
        guard let chat = chat else {
            return
        }
        
        OperationQueue().addOperation {
            if chat.synchronizationStatus == .all {
                chat.messages.getLastWithCount(50) { (result, items) in
                    self.add(messages: items)
                    //self.cachedImages.removeAll()
                    self.isLoading = false
                    self.messagesTable.reloadData()
                    self.scrollToBottom()
                    /*self.loadImagesRecursively {
                        OperationQueue.main.addOperation {
                            self.isLoading = false
                            self.messagesTable.reloadData()
                            self.scrollToBottom()
                        }
                    }*/
                }
            }
        }
    }
    
    
    func loadBeforeMessages() {
        if isLoading {
            return
        }
        
        isLoading = true
        
        guard let chat = chat else {
            return
        }
        
        OperationQueue().addOperation {
            if chat.synchronizationStatus == .all {
                if self.messages.count > 0 {
                    chat.messages.getBefore(self.messages[0].index.uintValue, withCount: 50, completion: { (result, items) in
                        self.addBefore(messages: items)
                        //self.add(messages: items)
                        self.cachedImages.removeAll()
                        self.isLoading = false
                        self.messagesTable.reloadData()
                        /*self.loadImagesRecursively {
                            OperationQueue.main.addOperation {
                                self.isLoading = false
                                self.messagesTable.reloadData()
                            }
                        }*/
                    })
                }
            }
        }
    }
    
    
    /*func loadMessages() {
        if isLoading {
            return
        }
        
        isLoading = true
        
        guard let channel = channel else {
            return
        }
        
        OperationQueue().addOperation {
            if channel.synchronizationStatus == .all {
                if self.messages.count == 0 {
                    channel.messages.getLastWithCount(50) { (result, items) in
                        self.add(messages: items)
                        self.loadImagesRecursively {
                            OperationQueue.main.addOperation {
                                self.isLoading = false
                                self.messagesTable.reloadData()
                            }
                        }
                    }
                } else {
                    let last = self.messages[self.messages.count - 1]
                    channel.messages.getBefore(self.messages[0].index.uintValue, withCount: 50, completion: { (result, items) in
                        self.add(messages: items)
                        self.loadImagesRecursively {
                            OperationQueue.main.addOperation {
                                self.isLoading = false
                                self.messagesTable.reloadData()
                            }
                        }
                    })
                }
            }
        }
    }*/
    
    
    /*func process(messages: [TCHMessage]?) {
        if let items = messages {
            var result = [Message]()
            var imagesUrls = [String]()
            
            for item in items {
                if let attachments = item.attributes()["attachments"] as? JsonArray {
                    if attachments.count > 0 {
                        if let original = (attachments[0] as? JsonObject)?["original"] as? JsonObject {
                            if let path = original["url"] as? String {
                                imagesUrls.append(path)
                            }
                        }
                    }
                }
                result.append(Message(message: item))
            }
            
            if imagesUrls.count == 0 {
                self.add(messages: result)
            } else {
                
            }
        }
    }*/
    
    
    /*func loadImagesRecursively(loop: Int = 0, completion: @escaping () -> Void) {
        if loop == messages.count {
            completion()
        } else {
            if let attachments = messages[loop].attributes["attachments"] as? JsonArray {
                if attachments.count > 0 {
                    if let original = (attachments[0] as? JsonObject)?["original"] as? JsonObject {
                        if let path = original["url"] as? String {
                            if let image = app.getCachedImage(path: path) {
                                self.cachedImages[IndexPath(row: loop, section: 0)] = image
                                loadImagesRecursively(loop: loop + 1, completion: completion)
                                return
                            } else {
                                app.loadImage(path: path, callback: { (image) in
                                    app.setCachedImage(image: image, path: path)
                                    self.cachedImages[IndexPath(row: loop, section: 0)] = image
                                    self.loadImagesRecursively(loop: loop + 1, completion: completion)
                                }, fallback: { (error) in
                                    self.loadImagesRecursively(loop: loop + 1, completion: completion)
                                })
                                return
                            }
                        }
                    }
                }
            }
            
            loadImagesRecursively(loop: loop + 1, completion: completion)
        }
    }*/
    
    
    func send(message: String) {
        if message == "" || message == "Введите текст".localized {
            return
        }
        
        if let msg = chat?.messages.createMessage(withBody: message) {
            msg.setAttributes(["__v": 1], completion: { (result) in
                self.chat?.messages.send(msg) { result in
                    if let error = result?.error {
                        app.alert(error: error)
                    }
                }
            })
        }
    }
    
    
    func recursivelySend(text: String?, withAttachments attachments: [UIImage], loop: Int = 0) {
        if let text = text {
            if text != "" {
                if let msg = chat?.messages.createMessage(withBody: text) {
                    msg.setAttributes(["__v": 1], completion: { (result) in
                        self.chat?.messages.send(msg, completion: { result in
                            self.recursivelySend(text: nil, withAttachments: attachments, loop: 0)
                        })
                    })
                }
            } else {
                self.recursivelySend(text: nil, withAttachments: attachments, loop: 0)
            }
        } else if loop == attachments.count {
            self.loadNextMessages()
        } else {
            if let msg = chat?.messages.createMessage(withBody: nil) {
                
                msg.setAttributes(["url": "https://images.pexels.com/photos/115045/pexels-photo-115045.jpeg?w=940&h=650&auto=compress&cs=tinysrgb"], completion: { result in
                    self.chat?.messages.send(msg) { result in
                        self.recursivelySend(text: nil, withAttachments: attachments, loop: loop + 1)
                    }
                })
            }
        }
    }
    
    
    func addBefore(messages: [TCHMessage]?) {
        guard let items = messages else {
            return
        }
        
        var result = [Message]()
        
        for item in items {
            let message = Message(message: item)
            var isRead = false
            
            for (key, value) in consumptionIndexes {
                if app.user?.chatIdentity != .some(key) {
                    isRead = message.index.intValue <= value.intValue
                    if isRead {
                        break
                    }
                }
            }
            
            message.type = app.user?.chatIdentity == .some(message.author) ? .outgoing : .incoming
            message.isRead = isRead
            result.append(message)
        }
        
        result.sort(by: { $0.timestampAsDate > $1.timestampAsDate })
        self.messages.insert(contentsOf: result, at: 0)
        self.sortMessages()
    }
    
    
    func add(messages: [TCHMessage]?) {
        if let items = messages {
            var result = [Message]()
            for item in items {
                let message = Message(message: item)
                var isRead = false
                
                for (key, value) in consumptionIndexes {
                    if app.user?.chatIdentity != .some(key) {
                        isRead = message.index.intValue <= value.intValue
                        if isRead {
                            break
                        }
                    }
                }
                
                message.type = app.user?.chatIdentity == .some(message.author) ? .outgoing : .incoming
                message.isRead = isRead
                result.append(message)
            }
            
            self.messages.append(contentsOf: result)
            self.sortMessages()
            
            /*if let sid = channel?.sid {
                if self.messages.count > 50 {
                    let suffix: [Message] = Array(self.messages.suffix(50))
                    app.stack.setHistory(forChannelSid: sid, messages: suffix)
                } else {
                    app.stack.setHistory(forChannelSid: sid, messages: self.messages)
                }
            }*/
            
            if let last = self.messages.last {
                chat?.messages.advanceLastConsumedMessageIndex(last.index)
            }
        }
    }
    
    
    func sortMessages() {
        messages = messages.sorted { a, b in a.timestamp < b.timestamp }
    }
    
    
    func scrollToBottom(animated: Bool = false) {
        if messages.count > 0 {
            let i = messagesTable.numberOfRows(inSection: 0)
            let indexPath = IndexPath(row: i - 1, section: 0)
            messagesTable.scrollToRow(at: indexPath, at: .bottom, animated: animated)
        }
    }
    
    
    func add(photoURL: String, toMessage message: TCHMessage) {
        if var attrs = message.attributes() {
            attrs[app.keys.messageAdditionPhoto] = photoURL
            message.setAttributes(attrs, completion: nil)
        }
    }
    
    
    @IBAction func handlerOf(doctor: ImageButton) {
        presentDoctor()
    }
    
    
    @IBAction func handlerOf(conclusion: ImageButton) {
        performSegue(withIdentifier: app.segues.presentConclusion, sender: nil)
    }
    
    
    @IBAction func handlerOf(contract: ImageButton) {
        performSegue(withIdentifier: app.segues.presentContract, sender: nil)
    }
    
    
    @IBAction func handlerOf(complaint: ImageButton) {
        performSegue(withIdentifier: app.segues.presentComplaint, sender: nil)
    }
    
    
    func slideActions() {
        var constant: CGFloat = 0.0
        
        if additionsOffset.constant == 0.0 {
            constant = -72.0
        } else {
            constant = 0.0
        }
        
        UIView.animate(withDuration: 0.2) {
            self.additionsOffset.constant = constant
            self.view.layoutIfNeeded()
        }
    }
    
    
    /*func process(attributes: [String: Any], indexPath: IndexPath) {
        OperationQueue().addOperation {
            if let attachments = attributes["attachments"] as? JsonArray {
                if attachments.count > 0 {
                    if let original = (attachments[0] as? JsonObject)?["original"] as? JsonObject {
                        if let path = original["url"] as? String {
                            if let image = app.getCachedImage(path: path) {
                                self.cachedImages[indexPath] = image
                                OperationQueue.main.addOperation {
                                    self.messagesTable.reloadRows(at: [indexPath], with: .fade)
                                }
                            }
                        }
                    }
                }
            }
        }
    }*/
    
    
    /*func load(fileId id: String, cell: MessageCell?) {
        if let item = app.stack.item(forEntity: File.self, predicateFormat: "id == '\(id)'") {
            if let local = item.localUrl {
                if let image = app.image(fromPath: local) {
                    cell?.attachmentView.image = image
                    return
                }
            }
            
            if let urlString = item.url {
                cell?.attachmentView.image = app.image(fromPath: urlString)
            }
        } else {
            cell?.isLoading = true
            app.api.files.file(id: id, callback: { (file) in
                
            }, fallback: { (error) in
                app.alert(error: error)
            })
            //app.api.files.
        }
        
        
        
        
        
        
        if let message = messages[indexPath.row].attributes()["url"] as? String {
            OperationQueue().addOperation {
                if let url = URL(string: message) {
                    do {
                        let data = try Data(contentsOf: url)
                        
                        if let image = UIImage(data: data) {
                            self.images[indexPath] = image
                            OperationQueue.main.addOperation {
                                self.messagesTable.reloadRows(at: [indexPath], with: .fade)
                            }
                        }
                    } catch let error {
                        print(error)
                    }
                }
            }
        }
    }*/
    
    
    func presentDoctor() {
        performSegue(withIdentifier: app.segues.presentDoctorInfo, sender: consultation?.doctor)
    }
    
    
    func send(attachments: [UIImage]) {
        if let chat = chat {
            var messages = [UploadingMessage]()
            
            for attachment in attachments {
                if let url = app.save(image: attachment) {
                    let message = UploadingMessage(url: url, channelSid: chat.sid)
                    message.preview = attachment
                    messages.append(message)
                }
            }
            
            app.uploadingMessages[chat.sid] = messages
            
            messagesTable.reloadSections(IndexSet(integer: 0), with: .fade)
            scrollToBottom()
            
            OperationQueue().addOperation {
                app.uploadMessages(channelSid: chat.sid, completion: { (success, message) in
                    OperationQueue.main.addOperation {
                        if success {
                            app.stack.save()
                            if let msg = chat.messages.createMessage(withBody: "") {
                                if let attributes = message.getAttributes() {
                                    msg.setAttributes(attributes, completion: { (result) in
                                        chat.messages.send(msg) { result in
                                            if let image = message.preview {
                                                if let path = message.file?.url {
                                                    app.setCachedImage(image: image, path: path)
                                                    print("index \(msg.sid)")
                                                    self.pendingImages[msg.sid] = image
                                                }
                                            }
                                            
                                            if let error = result?.error {
                                                app.alert(error: error)
                                            }
                                        }
                                    })
                                }
                            }
                        } else {
                            //try? app.fm.removeItem(at: message.url)
                            app.alert(title: "Ошибка".localized, message: "Не удалось загрузить файл \(message.url.lastPathComponent). Попробуйте позднее или обратитесь в службу поддержки.".localized)
                            self.messagesTable.reloadSections(IndexSet(integer: 0), with: .fade)
                        }
                    }
                })
            }
        }
    }
    
    
    func getUploadingCount() -> Int {
        if let sid = chat?.sid {
            if let uploading = app.uploadingMessages[sid] {
                return uploading.count
            }
        }
        
        return 0
    }
    
    func setupAttachmentView(image: UIImage, cell: MessageCell?) {
        cell?.setupStatus(forImageMessage: true)
        cell?.attachmentView.image = image
    }
    
    var downloadingFiles = [String]()
    
    func download(file: String, indexPath: IndexPath, cell: MessageCell?) {
        OperationQueue().addOperation {
            if self.downloadingFiles.contains(file) {
                return
            }
            
            if let image = app.getCachedImage(path: file) {
                OperationQueue.main.addOperation {
                    self.cachedImages[file] = image
                    let height = self.tableView(self.messagesTable, heightForRowAt: indexPath)
                    self.messagesTable.reloadRows(at: [indexPath], with: .fade)
                    self.messagesTable.contentOffset.y += height
                }
            } else {
                cell?.isLoading = true
                self.downloadingFiles.append(file)
                app.loadImage(path: file, callback: { (image) in
                    if let index = self.downloadingFiles.index(of: file) {
                        self.downloadingFiles.remove(at: index)
                    }
                    OperationQueue.main.addOperation {
                        self.cachedImages[file] = image
                        let height = self.tableView(self.messagesTable, heightForRowAt: indexPath)
                        self.messagesTable.reloadRows(at: [indexPath], with: .fade)
                        self.messagesTable.contentOffset.y += height
                    }
                }, fallback: { (error) in
                    if let index = self.downloadingFiles.index(of: file) {
                        self.downloadingFiles.remove(at: index)
                    }
                    OperationQueue.main.addOperation {
                        app.alert(error: error)
                    }
                })
            }
        }
    }
    
    
    func show(file: String, indexPath: IndexPath, cell: MessageCell?) {
        if let image = cachedImages[file] {
            cell?.attachmentView.image = image
            cell?.messageViewWidth.constant = messages[indexPath.row].preferredHeight * image.size.width / image.size.height
        } else {
            cell?.messageViewWidth.constant = 100.0
            download(file: file, indexPath: indexPath, cell: cell)
        }
    }
    
    
    func getHeight(for image: UIImage) -> CGFloat {
        var height: CGFloat = 100.0
        
        if image.size.width > image.size.height {
            if image.size.width > MessageCell.preferredImageSide {
                height = ceil(MessageCell.preferredImageSide * image.size.height / image.size.width)
            } else {
                height = image.size.height
            }
        } else {
            if image.size.height > MessageCell.preferredImageSide {
                height = MessageCell.preferredImageSide
            } else {
                height = image.size.height
            }
        }
        
        return height
    }
    
    
    func translate(message: Message) {
        if let id = message.fileId {
            app.showLoader(inView: view)
            app.api.files.translate(fileId: id, messageSid: message.sid, callback: { (translation) in
                let alert = UIAlertController(title: nil, message: "Запрос на перевод успешно отправлен.".localized, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                }))
                
                self.present(alert, animated: true, completion: nil)
                app.hideLoader()
                self.update(message: message)
            }, fallback: { (error) in
                app.hideLoader()
                app.alert(error: error)
            })
        }
    }
    
    func update(message: Message) {
        OperationQueue().addOperation {
            self.chat?.messages.message(withIndex: message.index, completion: { (result, msg) in
                if let index = self.messages.index(of: message) {
                    if let msg = msg {
                        self.messages[index] = Message(message: msg)
                        OperationQueue.main.addOperation {
                            self.messagesTable.reloadRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                        }
                    }
                }
            })
        }
    }
    
    
    /** Открыть мед. карту */
    
    func grantAccessToMID() {
        guard let id = consultation.id else { return }
        app.api.mid.grantAccess(consultationID: id, callback: { () in
            app.alert(title: nil, message: "Доступ к медицинской карте разрешен.")
        }) { (error) in
            app.alert(error: error)
        }
    }
    
    
    func openMID() {
        guard let id = consultation.id else { return }
        app.api.mid.getLink(consultationID: id, callback: { (link) in
            if let url = URL(string: link) {
                let web = SFSafariViewController(url: url)
                self.present(web, animated: true, completion: nil)
            }
        }) { (error) in
            app.alert(error: error)
        }
    }
    
    
    
    //MARK: - NAV
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == app.segues.presentConclusion {
            segue.destination.transitioningDelegate = self
            segue.destination.modalPresentationStyle = .custom
            
            let destination = segue.destination as? VCConclusion
            destination?.header = "Заключение".localized
            destination?.text = (consultation as? SO)?.conclusion
            
        } else if segue.identifier == app.segues.presentContract {
            segue.destination.transitioningDelegate = self
            segue.destination.modalPresentationStyle = .custom
            (segue.destination as? VCContract)?.contract = consultation as? Contract
        } else if segue.identifier == app.segues.presentDoctorInfo {
            segue.destination.transitioningDelegate = self
            segue.destination.modalPresentationStyle = .custom
            
            let controller = segue.destination as? VCDoctorInfo
            controller?.doctor = sender as? Doctor
            controller?.canStartSO = false
        } else if segue.identifier == app.segues.presentTranslation {
            segue.destination.transitioningDelegate = self
            segue.destination.modalPresentationStyle = .custom
            let destination = segue.destination as? VCTranslation
            destination?.message = sender as? Message
            destination?.channel = chat
        } else if segue.identifier == app.segues.presentEdit {
            segue.destination.transitioningDelegate = self
            segue.destination.modalPresentationStyle = .custom
            
            let destination = segue.destination as? VCEdit
            destination?.editedItem = sender as? CheckListItem
            destination?.soid = consultation?.id
            destination?.sender = self
        } else if segue.identifier == app.segues.presentComplaint {
            let destination = (segue.destination as? NavigationController)?.viewControllers[0] as? VCComplaint
            destination?.consultation = consultation
        }
    }
    
}



//MARK: - UITableViewDataSource

extension VCConsultation: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count + getUploadingCount()
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath)
    }
    
}



//MARK: - UITableViewDelegate

extension VCConsultation: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 60.0
        let width = MessageCell.preferredImageSide //tableView.frame.width * 0.7
        
        if indexPath.row >= messages.count {
            height = width * 3 / 4
        } else {
            let message = messages[indexPath.row]
            
            if let file = message.file {
                height = 100
                
                if let translation = message.fileTranslation {
                    if translatedIndexPaths.contains(indexPath) {
                        if let image = cachedImages[translation] {
                            message.preferredHeight = getHeight(for: image)
                            height = message.preferredHeight + 40.0
                        }
                    } else {
                        if let image = cachedImages[file] {
                            message.preferredHeight = getHeight(for: image)
                            height = message.preferredHeight + 40.0
                        }
                    }
                } else {
                    if let image = cachedImages[file] {
                        if message.type == .incoming {
                            message.preferredHeight = getHeight(for: image)
                            height = message.preferredHeight + 40.0
                        } else {
                            message.preferredHeight = getHeight(for: image)
                            height = message.preferredHeight + 20.0
                        }
                    }
                }
            } else {
                var text: String?
                
                if translatedIndexPaths.contains(indexPath) {
                    text = message.bodyTranslation
                } else {
                    text = message.body
                }
                
                var statusWidth: CGFloat
                
                if message.type == .outgoing {
                    statusWidth = 33.0
                } else {
                    statusWidth = 12.0
                }
                
                statusWidth += df.string(from: message.timestampAsDate).widthForHeight(height: 16.0, attributes: messageTimeAttrs)
                let spacesCount = Int(ceil(statusWidth / messageSpaceWidth))
                for _ in 0..<spacesCount {
                    text?.append("_")
                }
                
                if let text = text {
                    MessageCell.textViewPattern.text = text
                    height = MessageCell.textViewPattern.selfFitsSize().height + 32.0
                }
            }
            
            if message.bodyTranslation != nil {
                height += 20.0
            }
            
            
            if message.type == .incoming {
                if indexPath.row > 0 {
                    let previousMessage = messages[indexPath.row - 1]
                    
                    if message.author != previousMessage.author {
                        height += 30.0
                    }
                } else {
                    height += 30.0
                }
            }
        }
        
        return height
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        let cell = cell as? MessageCell
        cell?.reset()
        cell?.delegate = self
        
        if indexPath.row >= messages.count {
            if let sid = chat?.sid {
                let message = app.uploadingMessages[sid]?[indexPath.row - messages.count]
                cell?.attachmentView.image = message?.preview
                cell?.isLoading = true
                cell?.type = .outgoing
                cell?.setupStatus(forImageMessage: true)
            }
        } else {
            let message = messages[indexPath.row]
            cell?.fillColor = membersColors[message.author]
            cell?.isRead = message.isRead
            cell?.type = message.type
            cell?.timestamp = df.string(from: message.timestampAsDate)
            
            if let file = message.file {
                if let translation = message.fileTranslation {
                    if translatedIndexPaths.contains(indexPath) {
                        show(file: translation, indexPath: indexPath, cell: cell)
                        cell?.set(translationStatus: "Показать оригинал".localized)
                    } else {
                        show(file: file, indexPath: indexPath, cell: cell)
                        cell?.set(translationStatus: "Показать перевод".localized)
                    }
                } else {
                    show(file: file, indexPath: indexPath, cell: cell)
                    
                    if message.type == .incoming {
                        if let status = message.translationStatus {
                            cell?.set(translationStatus: status)
                            cell?.translationButton.isEnabled = false
                        } else {
                            cell?.set(translationStatus: "Заказать перевод".localized)
                        }
                    }
                }
                
                cell?.setupStatus(forImageMessage: true)
                
                // TODO: Check and add if needed
                /*
                 if let image = pendingImages[message.sid] {
                 preview = image
                 cachedImages[indexPath] = image
                 pendingImages.removeValue(forKey: message.sid)
                 }
                */
            } else {
                var text: String?
                
                if translatedIndexPaths.contains(indexPath) {
                    cell?.message = message.bodyTranslation
                    text = message.bodyTranslation
                } else {
                    cell?.message = message.body
                    text = message.body
                }
                
                var width: CGFloat
                
                if message.type == .outgoing {
                    width = 33.0
                } else {
                    width = 12.0
                }
                
                width += df.string(from: message.timestampAsDate).widthForHeight(height: 16.0, attributes: messageTimeAttrs)
                
                let spacesCount = Int(ceil(width / messageSpaceWidth))
                for _ in 0..<spacesCount {
                    text?.append("_")
                }
                
                //cell?.message = text
                
                if let text = text {
                    MessageCell.textViewPattern.text = text
                    cell?.messageViewWidth.constant = MessageCell.textViewPattern.selfFitsSize().width + 12.0
                }
                
                if message.bodyTranslation != nil {
                    if translatedIndexPaths.contains(indexPath) {
                        cell?.set(translationStatus: "Показать оригинал".localized)
                    } else {
                        cell?.set(translationStatus: "Показать перевод".localized)
                    }
                }
            }
            
            if message.type == .incoming {
                if indexPath.row > 0 {
                    let previousMessage = messages[indexPath.row - 1]
                    
                    if message.author != previousMessage.author {
                        cell?.header = membersNames[message.author]
                    }
                } else {
                    cell?.header = membersNames[message.author]
                }
            }
        }
    }
}



//MARK: - UIScrollViewDelegate

extension VCConsultation: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        //print(scrollView.contentOffset.y)
        //if scrollView.contentOffset.y < 1.0 {
         //   loadBeforeMessages()
        //}
    }
    
}



//MARK: - NavigationTitleViewDelegate

extension VCConsultation: NavigationTitleViewDelegate {
    
    func didTapOn(navigationTitle: NavigationTitleView) {
        var newOffset: CGFloat = 0.0
        
        if topOffset.constant == 0 {
            newOffset = -57.0
            navigationTitle.detail = #imageLiteral(resourceName: "small-arrow-down")
        } else {
            navigationTitle.detail = #imageLiteral(resourceName: "small-arrow-up")
        }
        
        UIView.animate(withDuration: 0.2) { 
            self.topOffset.constant = newOffset
            self.view.layoutIfNeeded()
        }
    }
    
}



//MARK: - UIImagePickerControllerDelegate, UINavigationControllerDelegate

extension VCConsultation: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            newMessageView.add(attachment: image)
        } else {
            // error message
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
}



//MARK: - MessageAdditionsViewDelegate

extension VCConsultation: MessageAdditionsViewDelegate {
    
    func didTap(cameraButton: ImageButton) {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = false
            slideActions()
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func didTap(photoButton: ImageButton) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            slideActions()
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func didTap(medrecButton: ImageButton) {
        guard let user = app.user else { return }
        
        if user.type == .patient {
            if user.isMCAttached {
                let alert = UIAlertController(title: nil, message: "Предоставить доступ к медицинской карте?".localized, preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "Да".localized, style: .default, handler: { (action) in
                    self.grantAccessToMID()
                })
                
                let cancelAction = UIAlertAction(title: "Нет".localized, style: .default, handler: { (action) in
                    alert.dismiss(animated: true, completion: nil)
                })
                
                alert.addAction(cancelAction)
                alert.addAction(confirmAction)
                self.present(alert, animated: true, completion: nil)
            } else {
                let alert = UIAlertController(title: nil, message: "Медкарта не привязана к профилю. Привязать ее сейчас?".localized, preferredStyle: .alert)
                
                let confirmAction = UIAlertAction(title: "Да".localized, style: .default, handler: { (action) in
                    self.performSegue(withIdentifier: "showAddMID", sender: nil)
                })
                
                let cancelAction = UIAlertAction(title: "Нет".localized, style: .default, handler: { (action) in
                    alert.dismiss(animated: true, completion: nil)
                })
                
                alert.addAction(cancelAction)
                alert.addAction(confirmAction)
                self.present(alert, animated: true, completion: nil)
            }
        } else {
            openMID()
        }
    }
    
}



//MARK: - NewMessageViewDelegate

extension VCConsultation: NewMessageViewDelegate {
    
    func didTap(sendButton: UIButton, text: String?) {
        if newMessageView.attachments.count > 0 {
            if let text = text {
                send(message: text)
            }
            
            send(attachments: newMessageView.attachments)
        } else {
            if let text = text {
                send(message: text)
            }
        }
        
        newMessageView.reset()
    }
    
    
    func didTap(additionsButton: UIButton) {
        slideActions()
    }
    
    
    func didChangeEditing(textView: UITextView) {
        newMessageView.sendButtonHidden = textView.text.characters.count == 0
    }
    
    
    func didTapReturnKey(textView: UITextView) {
        if textView.text !=  "" {
            send(message: textView.text)
            newMessageView.reset()
        }
    }
    
}



//MARK: - Twilio updates

extension VCConsultation {
    
}



//MARK: - MessageCellDelegate

extension VCConsultation: MessageCellDelegate {
    
    func translate(for cell: MessageCell) {
        if let indexPath = messagesTable.indexPath(for: cell) {
            self.performSegue(withIdentifier: app.segues.presentTranslation, sender: messages[indexPath.row])
        }
    }
    
    func showTranslation(for cell: MessageCell) {
        if let indexPath = messagesTable.indexPath(for: cell) {
            let message = messages[indexPath.row]
            
            if message.file != nil {
                if message.fileTranslation != nil {
                    if let index = translatedIndexPaths.index(of: indexPath) {
                        translatedIndexPaths.remove(at: index)
                    } else {
                        translatedIndexPaths.append(indexPath)
                    }
                } else {
                    let alert = UIAlertController(title: nil, message: "Вы уверены, что хотите отправить этот файл на перевод?".localized, preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Отмена".localized, style: .cancel, handler: { (action) in
                        alert.dismiss(animated: true, completion: nil)
                    }))
                    
                    alert.addAction(UIAlertAction(title: "Да".localized, style: .default, handler: { (action) in
                        self.translate(message: message)
                    }))
                    
                    self.present(alert, animated: true, completion: nil)
                }
            } else if let index = translatedIndexPaths.index(of: indexPath) {
                translatedIndexPaths.remove(at: index)
            } else {
                translatedIndexPaths.append(indexPath)
            }
            
            messagesTable.reloadRows(at: [indexPath], with: .fade)
        }
    }
    
}



//MARK: - UIGestureRecognizerDelegate

extension VCConsultation: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}



//MARK: - UIViewControllerTransitioningDelegate

extension VCConsultation: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.presenting = true
        return animator
    }
    
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        animator.presenting = false
        return animator
    }
    
}



//MARK: - TCHChannelDelegate

extension VCConsultation: TCHChannelDelegate {
    
    func chatClient(_ client: TwilioChatClient!, channel: TCHChannel!, updated: TCHChannelUpdate) {
        
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channelDeleted channel: TCHChannel!) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channel: TCHChannel!, synchronizationStatusUpdated status: TCHChannelSynchronizationStatus) {
        if status == .all {
            
        }
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channel: TCHChannel!, member: TCHMember!, updated: TCHMemberUpdate) {
        consumptionIndexes[member.identity] = member.lastConsumedMessageIndex
        
        if app.user?.chatIdentity != .some(member.identity) {
            for message in messages {
                message.isRead = message.index.intValue <= member.lastConsumedMessageIndex.intValue
            }
        }
        
        self.messagesTable.reloadData()
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channel: TCHChannel!, messageAdded message: TCHMessage!) {
        if app.user?.chatIdentity == .some(message.author) {
            AudioServicesPlayAlertSound(1004)
        } else {
            AudioServicesPlayAlertSound(1003)
        }
        
        add(messages: [message])
        messagesTable.reloadData()
        scrollToBottom(animated: true)
        consultation.lastMessage = message
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channel: TCHChannel!, message: TCHMessage!, updated: TCHMessageUpdate) {
        for (i, msg) in messages.enumerated() {
            if msg.index == message.index {
                let new = Message(message: message)
                var isRead = false
                
                for (key, value) in consumptionIndexes {
                    if app.user?.chatIdentity != .some(key) {
                        isRead = new.index.intValue <= value.intValue
                        if isRead {
                            break
                        }
                    }
                }
                
                new.type = app.user?.chatIdentity == .some(message.author) ? .outgoing : .incoming
                new.isRead = isRead
                self.messages[i] = new
                self.messagesTable.reloadRows(at: [IndexPath(row: i, section: 0)], with: .fade)
                break
            }
        }
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channel: TCHChannel!, messageDeleted message: TCHMessage!) {
        for (i, message) in messages.enumerated() {
            if message.index == message.index {
                self.messages.remove(at: i)
                self.messagesTable.reloadSections(IndexSet(integer: 0), with: .fade)
                break
            }
        }
    }
    
    
    func chatClient(_ client: TwilioChatClient!, typingStartedOn channel: TCHChannel!, member: TCHMember!) {
        
    }
    
    
    func chatClient(_ client: TwilioChatClient!, typingEndedOn channel: TCHChannel!, member: TCHMember!) {
        
    }
    
    /*
    
    – chatClient:channel:memberJoined:
    
    – chatClient:channel:memberLeft:
    
    – chatClient:channel:member:user:updated:
    
    – chatClient:channel:member:userSubscribed:
    
    – chatClient:channel:member:userUnsubscribed:
    
    */
}
