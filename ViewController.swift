//
//  ViewController.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    //MARK: - VARS
    
    var navigationTitleView: NavigationTitleView? {
        return navigationItem.titleView as? NavigationTitleView
    }
    
    
    
    //MARK: - LIFE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let titleView = NavigationTitleView.instance()
        navigationItem.titleView = titleView
    }

}
