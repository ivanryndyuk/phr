//
//  Meta.swift
//  phr.kz
//
//  Created by ivan on 07/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class Meta: NSObject {

    //MARK: - VARS
    
    var itemsOnPage: Int
    var currentPage: Int
    var totalPages: Int
    var total: Int
    var sort: String
    var direction: String
    
    
    
    //MARK: - LIFE
    
    init?(json: JsonObject) {
        guard let itemsOnPage = json["items_per_page"] as? Int else {
            return nil
        }
        
        guard let currentPage = json["current_page"] as? Int else {
            return nil
        }
        
        guard let totalPages = json["total_pages"] as? Int else {
            return nil
        }
        
        guard let total = json["total"] as? Int else {
            return nil
        }
        
        guard let sort = json["sort"] as? String else {
            return nil
        }
        
        guard let direction = json["direction"] as? String else {
            return nil
        }
        
        self.itemsOnPage = itemsOnPage
        self.currentPage = currentPage
        self.totalPages = totalPages
        self.total = total
        self.sort = sort
        self.direction = direction
    }
    
}
