//
//  ApiSO.swift
//  phr.kz
//
//  Created by ivan on 16/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ApiSO: ApiSection {

    //MARK: - FUNCS
    
    /* Получить информацию по конкретному SO. */
    
    func itemSO() {
        
    }
    
    
    /* Получить список SO. */
    
    func listSO(limit: Int = 100, page: Int = 1, sort: String = "entity.createdAt", direction: String = "desc", callback: @escaping Block<(Meta, [SO])>, fallback: @escaping Block<Error>) {
        let params: [String: Any] = ["limit": limit, "page": page, "sort": sort, "direction": direction]
        let request = parent.request(withMethod: .get, path: "secondopinions", params: params, token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                guard let metaJson = data["meta"] as? JsonObject else {
                    fallback(Api.Errors.wrongJsonFormat)
                    return
                }
                
                guard let meta = Meta(json: metaJson) else {
                    fallback(Api.Errors.wrongJsonFormat)
                    return
                }
                
                guard let itemsJson = data["items"] as? JsonArray else {
                    fallback(Api.Errors.wrongJsonFormat)
                    return
                }
                
                let items = app.stack.soList(fromJson: itemsJson)
                callback(meta, items)
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
    }
    
    
    /* Подать зявку на SO и начать чат. */
    
    func newSO(requestDoctor: String?, callback: @escaping Block<(SO)>, fallback: @escaping Block<Error>) {
        var params: [String: Any]? = nil
        
        if let id = requestDoctor {
            params = ["requestDoctor": id]
        }
        
        let request = parent.request(withMethod: .post, path: "secondopinions", params: params, token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                if let so = app.stack.so(forJson: data) {
                    callback(so)
                } else {
                    fallback(Api.Errors.wrongJsonFormat)
                }
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
    }


    /* Добавить пункт в чек-лист к SO. */
    
    func add(checkListItem item: CheckListItem, SOID: String, callback: @escaping Block<(SO)>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .post, path: "secondopinions/\(SOID)/checklists", params: ["name": item.name, "complete": NSNumber(value: item.complete)], token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                if let so = app.stack.so(forJson: data) {
                    callback(so)
                } else {
                    fallback(Api.Errors.wrongJsonFormat)
                }
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
    }
    
    
    /* Удалить пункт из чек-листа к SO. */
    
    func delete(checkListItem item: CheckListItem, SOID: String, callback: @escaping Block<(SO)>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .delete, path: "secondopinions/\(SOID)/checklists/\(item.id)", token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                if let so = app.stack.so(forJson: data) {
                    callback(so)
                } else {
                    fallback(Api.Errors.wrongJsonFormat)
                }
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
    }
    
    
    /* Обновить пункт в чек-листе к SO. */
    
    func update(checkListItem item: CheckListItem, newName: String? = nil, newComplete: Bool? = nil, SOID: String, callback: @escaping Block<(SO)>, fallback: @escaping Block<Error>) {
        var params: [String: Any]? = nil
        
        if let newName = newName {
            params = ["name": newName]
        }
        
        if let newComplete = newComplete {
            if params == nil {
                params = [String: Any]()
            }
            
            params?["complete"] = NSNumber(value: newComplete)
        }
        
        let request = parent.request(withMethod: .patch, path: "secondopinions/\(SOID)/checklists/\(item.id)", params: params, token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                if let so = app.stack.so(forJson: data) {
                    callback(so)
                } else {
                    fallback(Api.Errors.wrongJsonFormat)
                }
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
    }
    
}
