//
//  App.swift
//  phr.kz
//
//  Created by ivan on 16/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import TwilioChatClient

var kBundleKey: CChar = 0
let app = App()

func radians(from: Int) -> CGFloat {
    return CGFloat(from) * .pi / 180.0
}

class App: NSObject {
    
    //MARK: - VARS
    
    let shared = UIApplication.shared
    let delegate = UIApplication.shared.delegate as! AppDelegate
    let ud = UserDefaults.standard
    let nc = NotificationCenter.default
    let fm = FileManager.default
    let theme = Theme()
    let segues = Segues()
    let keys = Keys()
    let locales = [Locale(identifier: "ru_RU"), Locale(identifier: "en"), Locale.init(identifier: "kk_KZ")]
    let twilio = Twilio()
    let api = Api()
    let stack = DataStack()
    let reachability = Reachability()
    
    var channelToken: String?
    let df = DateFormatter()
    
    lazy var validator: Validator = {
        return Validator()
    }()
    
    lazy var loader: LoadingView = {
        return LoadingView.instance(message: "")
    }()
    
    var user: User?
    let countries = [Country(countryCode: "ru", title: "Россия".localized, telephoneCode: "+7"), Country(countryCode: "kk", title: "Казахстан".localized, telephoneCode: "+7"), Country(countryCode: "es", title: "Испания".localized, telephoneCode: "+34")]
    
    override init() {
        super.init()
        reachability?.whenReachable = { reachability in
            DispatchQueue.main.async {
                app.nc.post(name: app.notification(name: app.keys.reachabilityChangeNote), object: nil)
            }
        }
        
        reachability?.whenUnreachable = { reachability in
            DispatchQueue.main.async {
                app.nc.post(name: app.notification(name: app.keys.reachabilityChangeNote), object: nil)
            }
        }
    }
    
    
    /*
    var localizedBundle: Bundle {
        get {
            var lang: String
            
            switch app.lang {
            case .ru:
                lang = "ru"
            case .en:
                lang = "en"
            case .kz:
                lang = "kk-KZ"
            }
            
            let path = Bundle.main.path(forResource: lang, ofType: "lproj")
            
            if let path = path {
                if let bundle = Bundle(path: path) {
                    return bundle
                }
            }
            
            return Bundle.main
        }
    }*/
    
    var documentsDirectory: URL? {
        return FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first?.appendingPathComponent("phr.kz")
    }
    
    var locale: Locale {
        get {
            var id = "ru"
            
            if let appLocale = app.ud.string(forKey: app.keys.appLocale) {
                id = appLocale
            }
            
            return Locale(identifier: id)
        }
        
        set {
            app.ud.set(newValue.identifier, forKey: app.keys.appLocale)
            app.ud.synchronize()
            Bundle.set(locale: newValue)
        }
    }
    
    var localeCode: String {
        get {
            switch locale.identifier {
            case "en":
                return "en"
            case "kk_KZ":
                return "kk"
            default:
                return "ru"
            }
        }
    }
    
    
    //MARK: - FUNCS
    
    /* Show alert with params */
    
    func alert(title: String? = nil, message: String? = nil) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK".localized, style: .cancel) { (action) in
            controller.dismiss(animated: true, completion: nil)
        }
        
        controller.addAction(action)
        controller.view.tintColor = UIColor(hex: 0x495787)
        app.delegate.window?.rootViewController?.present(controller, animated: true, completion: nil)
        //app.delegate.window?.rootViewController?.show(controller, sender: nil)
    }
    
    
    /* Show alert with error */
    
    func alert(error: Error) {
        let controller = UIAlertController(title: "Ошибка".localized, message: error.localizedDescription, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK".localized, style: .cancel) { (action) in
            controller.dismiss(animated: true, completion: nil)
        }
        
        controller.addAction(action)
        controller.view.tintColor = UIColor(hex: 0x495787)
        app.delegate.window?.rootViewController?.show(controller, sender: nil)
    }
    
    
    /* Get message for setting language */
    
    func settingLanguageMessage(forLocale locale: Locale) -> String {
        var result: String
        
        switch locale.languageCode {
        case .some("en"):
            result = "Setting language..."
        case .some("kk"):
            result = "Тілді орнату..."
        default:
            result = "Установка языка..."
        }
        
        return result
    }
    
    
    /* New message note. */
    
    func notification(name: String) -> Notification.Name {
        return Notification.Name(rawValue: name)
    }
    
    func newMessageNote(channel: TCHChannel, message: TCHMessage) -> Notification {
        return Notification(name: notification(name: app.keys.newMessageNote), object: nil, userInfo: ["channel": channel, "message": message])
    }
    
    
    func updateMessageNote(channel: TCHChannel, message: TCHMessage) -> Notification {
        return Notification(name: notification(name: keys.messageUpdateNote), object: nil, userInfo: ["channel": channel, "message": message])
    }
    
    
    func deleteMessageNote(channel: TCHChannel, message: TCHMessage) -> Notification {
        return Notification(name: notification(name: keys.messageDeleteNote), object: nil, userInfo: ["channel": channel, "message": message])
    }
    
    
    func chatMemberUpdateNote(channel: TCHChannel, member: TCHMember) -> Notification {
        return Notification(name: notification(name: app.keys.chatMemberUpdate), object: nil, userInfo: ["channel": channel, "member": member])
    }
    
    
    func showLoader(withMessage: String? = nil, inView: UIView? = nil) {
        var container: UIView
        
        if let view = inView {
            container = view
        } else if let view = app.delegate.window {
            container = view
        } else {
            return
        }
        
        if let message = withMessage {
            loader.message = message
        }
        
        loader.alpha = 0.0
        container.addSubview(loader)
        loader.translatesAutoresizingMaskIntoConstraints = false
        
        container.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[loader]-0-|",
            options: .zero,
            metrics: nil,
            views: ["loader": loader]))
        
        container.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[loader]-0-|",
            options: .zero,
            metrics: nil,
            views: ["loader": loader]))
        
        UIView.animate(withDuration: 0.2) { 
            self.loader.alpha = 1.0
        }
    }
    
    
    func hideLoader() {
        UIView.animate(withDuration: 0.2, animations: { 
            self.loader.alpha = 0.0
        }) { (finished) in
            if finished {
                self.loader.removeFromSuperview()
            }
        }
    }
    
    
    func connectTwilio(callback: @escaping Block<Void>, fallback: Block<Error>? = nil) {
        twilio.connect(completion: { (success, error) in
            if success {
                callback()
            } else {
                if let error = error {
                    fallback?(error)
                } else {
                    fallback?(NSError.with(description: "Неизвестная ошибка.".localized))
                }
            }
        })
    }
    
    
    
    //MARK: - IMAGES
    
    func loadImage(path: String?, callback: @escaping Block<UIImage>, fallback: @escaping Block<Error>) {
        if let path = path {
            if let url = URL(string: path) {
                if let image = UIImage(contentsOf: url) {
                    setCachedImage(image: image, path: path)
                    callback(image)
                    return
                }
            }
        }
        
        fallback(NSError.with(description: "Ошибка при загрузке изображения.".localized))
    }
    
    
    func getCachedImage(path: String?) -> UIImage? {
        guard let path = path else {
            return nil
        }
        
        guard let url = URL(string: path) else {
            return nil
        }
        
        guard let imageUrl = cachedImageUrl(forUrl: url) else {
            return nil
        }
        
        let fm = FileManager.default
        
        if !fm.fileExists(atPath: imageUrl.path) {
            return nil
        }
        
        return UIImage(contentsOfFile: imageUrl.path)
    }
    
    
    func setCachedImage(image: UIImage, path: String?) {
        guard let path = path else {
            return
        }
        
        guard let url = URL(string: path) else {
            return
        }
        
        if let jpgData = UIImageJPEGRepresentation(image, 1) {
            if let cachedImageUrl = cachedImageUrl(forUrl: url) {
                do {
                    try jpgData.write(to: cachedImageUrl, options: .atomic)
                } catch let error {
                    NSLog(error.localizedDescription)
                }
            }
        }
    }
    
    
    func cachedImageUrl(forUrl url: URL?) -> URL? {
        guard let url = url else {
            return nil
        }
        
        do {
            var result = try app.fm.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
            
            if var imageName = url.absoluteString.sha256() {
                imageName += ".jpg"
                result.appendPathComponent(imageName)
                return result
            } else {
                return nil
            }
        } catch let error {
            log(error: error)
            return nil
        }
    }
    
    
    
    //MARK: - FILES
    
    var uploadingMessages = [String: [UploadingMessage]]()
    
    
    func image(fromPath path: String) -> UIImage? {
        guard let url =  documentsDirectory?.appendingPathComponent(path) else {
            return nil
        }
        
        do {
            let data = try Data(contentsOf: url)
            return UIImage(data: data)
        } catch let error {
            log(error: error)
            return nil
        }
    }
    
    
    func save(image: UIImage) -> URL? {
        if let data = UIImageJPEGRepresentation(image, 0.4) {
            if let url = app.documentsDirectory {
                if !app.fm.fileExists(atPath: url.path) {
                    try? app.fm.createDirectory(at: url, withIntermediateDirectories: true, attributes: nil)
                }
                
                let imageUrl = url.appendingPathComponent(app.newFileName())
                
                if app.fm.createFile(atPath: imageUrl.path, contents: data, attributes: nil) {
                    return imageUrl
                }
            }
        }
        
        return nil
    }
    
    
    /* Completion if true message was uploaded successfully. */
    
    func uploadMessages(channelSid: String, completion: @escaping Block<(Bool, UploadingMessage)>) {
        if var messages = uploadingMessages[channelSid] {
            if let message = messages.first {
                app.api.files.upload(fileUrl: message.url, channelSid: message.channelSid, callback: { (file) in
                    let item = messages.remove(at: 0)
                    file.localUrl = message.url.lastPathComponent
                    item.file = file
                    completion((true, item))
                    self.uploadingMessages[channelSid] = messages
                    self.uploadMessages(channelSid: channelSid, completion: completion)
                }, fallback: { (error) in
                    log(error: error)
                    let item = messages.remove(at: 0)
                    
                    if message.uploadAttempt < 3 {
                        item.uploadAttempt += 1
                        messages.append(item)
                    } else {
                        completion((false, item))
                    }
                    
                    self.uploadingMessages[channelSid] = messages
                    self.uploadMessages(channelSid: channelSid, completion: completion)
                })
            }
        }
    }
    
    
    func newFileName() -> String {
        var name = "shot_"
        df.dateFormat = "dd-MM-yy"
        name += df.string(from: Date())
        df.dateFormat = "HH.mm.ss"
        name += "_at_\(df.string(from: Date())).jpg"
        return name
    }

}
