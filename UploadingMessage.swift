//
//  UploadingMessage.swift
//  phr.kz
//
//  Created by ivan on 09/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class UploadingMessage: NSObject {

    //MARK: - VARS
    
    var url: URL
    var channelSid: String
    var preview: UIImage?
    var file: File?
    var uploadAttempt = 0
    
    
    
    //MARK: - LIFE
    
    init(url: URL, channelSid: String) {
        self.url = url
        self.channelSid = channelSid
    }
    
    
    
    //MARK: - FUNCS
    
    func getAttributes() -> JsonObject? {
        if let file = file {
            guard let id = file.id else {
                return nil
            }
            
            guard let type = file.fileType else {
                return nil
            }
            
            guard let url = file.url else {
                return nil
            }
            
            let original = ["id": id, "type": type, "url": url]
            let attachments: [String: Any] = ["original" : original]
            return ["attachments": [attachments], "__v": 1]
        }
        
        return nil
    }
    
}
