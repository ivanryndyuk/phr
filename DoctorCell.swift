//
//  DoctorCell.swift
//  phr.kz
//
//  Created by ivan on 24/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class DoctorCell: UITableViewCell {
    
    //MARK: - VARS
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var border: UIView!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var doctorTypeLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var title: String? {
        get {
            return headerLabel.text
        }
        
        set {
            headerLabel.text = newValue
        }
    }
    
    
    var price: String? {
        get {
            return priceLabel.text
        }
        
        set {
            priceLabel.text = newValue
        }
    }
    
    
    var specialities: String? {
        get {
            return doctorTypeLabel.text
        }
        
        set {
            doctorTypeLabel.text = newValue
        }
    }
    
    
    var statement: String? {
        get {
            return descriptionLabel.text
        }
        
        set {
            descriptionLabel.text = newValue
        }
    }
    
}
