//
//  Segues.swift
//  phr.kz
//
//  Created by ivan on 18/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class Segues: NSObject {

    let presentResetPassword = "presentResetPassword"
    let presentSignup = "presentSignup"
    let presentConfirmRegistrantion = "presentConfirmRegistrantion"
    let showChangePassword = "showChangePassword"
    let presentDoctorInfo = "presentDoctorInfo"
    let presentFilters = "presentFilters"
    let presentConclusion = "presentConclusion"
    let presentContract  = "presentContract"
    let presentComplaint = "presentComplaint"
    let showConsultation = "showConsultation"
    let presentTranslation = "presentTranslation"
    let presentEdit = "presentEdit"
    
}
