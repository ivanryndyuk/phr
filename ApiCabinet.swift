//
//  ApiCabinet.swift
//  phr.kz
//
//  Created by ivan on 08/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ApiCabinet: ApiSection {

    //MARK: - FUNCS
    
    /* Get user info */
    
    func selfuser(callback: @escaping Block<JsonObject>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .get, path: "selfuser", token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                callback(data)
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
        
    }
    
    
    
    /* Get twilio channel token */
    
    func channelToken(callback: @escaping Block<String>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .post, path: "selfuser/channel/token", token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let token = json["data"] as? String {
                callback(token)
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
    }
    
}
