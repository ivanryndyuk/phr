//
//  VCEdit.swift
//  phr.kz
//
//  Created by ivan on 15/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCEdit: ModalController {
    
    //MARK: - VARS
    
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bottomOffset: NSLayoutConstraint!
    
    var soid: String?
    var editedItem: CheckListItem?
    var sender: VCConsultation?
    var isLoading = false
    
    
    
    //MARK: - LIFE

    override func viewDidLoad() {
        super.viewDidLoad()
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillShow:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillHide:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        textField.text = editedItem?.name
        
        if editedItem == nil {
            titleLabel.text = "Новый пункт".localized
        } else {
            titleLabel.text = "Редактирование".localized
        }
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(cancelButton: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func handlerOf(applyButton: UIButton) {
        guard let name = textField.text else {
            app.alert(title: "Ошибка".localized, message: "Поле не должно быть пустым.".localized)
            return
        }
        
        guard let soid = soid else {
            app.alert(title: "Ошибка".localized, message: "Не указан номер Second opinion.".localized)
            return
        }
        
        if isLoading {
            return
        }
        
        isLoading = true
        
        if let item = editedItem {
            app.api.so.update(checkListItem: item, newName: name, SOID: soid, callback: { (so) in
                self.successCompletion(so: so)
            }, fallback: { (error) in
                self.dismiss(animated: true, completion: {
                    app.alert(error: error)
                })
            })
        } else {
            let newItem = CheckListItem(name: name, complete: false)
            
            app.api.so.add(checkListItem: newItem, SOID: soid, callback: { (so) in
                self.successCompletion(so: so)
            }, fallback: { (error) in
                self.dismiss(animated: true, completion: {
                    app.alert(error: error)
                })
            })
        }
    }
    
    
    func successCompletion(so: SO) {
        app.stack.save()
        sender?.checkList.update(so: so)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func handlerOf(tapGesture: UITapGestureRecognizer) {
        hideKeyboard()
    }
    
    
    func hideKeyboard() {
        view.endEditing(false)
    }
    
    
    func handlerOf(keyboardWillShow note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = keyboardBounds.height - self.bottomLayoutGuide.length
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func handlerOf(keyboardWillHide note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = 0.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }

}
