//
//  NavigationTitleView.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

@objc protocol NavigationTitleViewDelegate {
    func didTapOn(navigationTitle: NavigationTitleView)
}

class NavigationTitleView: UIView {

    //MARK: - VARS
    
    @IBOutlet weak var frameView: UIView!
    @IBOutlet weak var loaderView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var detailView: UIImageView!
    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var loader: Loader!
    @IBOutlet weak var loaderLabel: UILabel!
    
    var delegate: NavigationTitleViewDelegate?
    
    var isDetailHidden = true {
        willSet {
            if newValue {
                imageWidth.constant = 0.0
            } else {
                imageWidth.constant = 15.0
            }
        }
    }
    
    var title: String? {
        get {
            return titleLabel.text
        }
        
        set {
            titleLabel.text = newValue
        }
    }
    
    var subtitle: String? {
        get {
            return subtitleLabel.text
        }
        
        set {
            subtitleLabel.text = newValue
        }
    }
    
    
    var detail: UIImage? {
        get {
            return detailView.image
        }
        
        set {
            detailView.image = newValue
        }
    }
    
    
    var titleColor: UIColor? {
        get {
            return titleLabel.textColor
        }
        
        set {
            titleLabel.textColor = newValue
            loaderLabel.textColor = newValue
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return frame.size
    }
    
    
    
    //MARK: - FUNCS
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2) {
            self.alpha = 0.35
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2) {
            self.alpha = 1.0
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2) {
            self.alpha = 1.0
        }
    }
    
    
    
    //MARK: - LIFE
    
    class func instance() -> NavigationTitleView {
        guard let instance = Bundle.main.loadNibNamed("NavigationTitleView", owner: self, options: nil)?.first as? NavigationTitleView else {
            return NavigationTitleView()
        }
        
        return instance
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(tapGeusture: UIGestureRecognizer) {
        delegate?.didTapOn(navigationTitle: self)
    }
    
    
    func startLoading(message: String = "Загрузка...".localized) {
        loaderLabel.text = message
        
        if loader.isAnimating {
            return
        }
        
        loaderView.isHidden = false
        loader.startAnimating()
        
        UIView.animate(withDuration: 0.2, animations: { 
            self.loaderView.alpha = 1.0
            self.frameView.alpha = 0.0
        }) { (finished) in
            self.frameView.isHidden = true
        }
    }
    
    
    func stopLoading() {
        self.frameView.isHidden = false
        
        UIView.animate(withDuration: 0.2, animations: {
            self.loaderView.alpha = 0.0
            self.frameView.alpha = 1.0
        }) { (finished) in
            self.loader.stopAnimating()
            self.loaderView.isHidden = true
        }
    }
}
