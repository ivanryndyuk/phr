//
//  VCSplash.swift
//  phr.kz
//
//  Created by ivan on 15/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCSplash: UIViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var splashCollection: UICollectionView!
    @IBOutlet weak var pageControl: PageControl!
    
    let reuseId = "splashCell"
    
    
    
    //MARK: - LIFE

    override func viewDidLoad() {
        super.viewDidLoad()
        splashCollection.register(UINib(nibName: "SplashCell", bundle: nil), forCellWithReuseIdentifier: reuseId)
        splashCollection.dataSource = self
        splashCollection.delegate = self
        
        pageControl.numberOfPages = 4
        pageControl.currentPage = 0
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(changing pageControl: PageControl) {
        pageControl.updateDots()
        scrollSplashCollection()
    }
    
    
    @IBAction func handlerOf(nextButton: Button) {
        if pageControl.currentPage == 3 {
            app.ud.set(true, forKey: app.keys.notFirstLaunch)
            app.delegate.transitToAuth()
        } else {
            pageControl.currentPage += 1
            scrollSplashCollection()
        }
    }
    
    
    func scrollSplashCollection() {
        let indexPath = IndexPath(item: pageControl.currentPage, section: 0)
        splashCollection.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }

}



//MARK: - UICollectionViewDataSource

extension VCSplash: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 4
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return collectionView.dequeueReusableCell(withReuseIdentifier: reuseId, for: indexPath)
    }
    
}



//MARK: - UICollectionViewDelegate

extension VCSplash: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return collectionView.frame.size
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if scrollView == splashCollection {
            let pageWidth = splashCollection.frame.size.width
            pageControl.currentPage = Int(splashCollection.contentOffset.x / pageWidth)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cell = cell as? SplashCell
        cell?.title = "Консультации в режиме реального времени"
    }
    
}
