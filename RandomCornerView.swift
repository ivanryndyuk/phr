//
//  RandomCornerView.swift
//  phr.kz
//
//  Created by ivan on 04/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

@IBDesignable class RandomCornerView: UIView {

    //MARK: - VARS
    
    @IBInspectable var cornerRadius: CGFloat = 0
    @IBInspectable var roundTopLeft: Bool = false
    @IBInspectable var roundTopRight: Bool = false
    @IBInspectable var roundBottomLeft: Bool = false
    @IBInspectable var roundBottomRight: Bool = false
    
    
    
    //MARK: - FUNCS
    
    override func layoutSubviews() {
        super.layoutSubviews()
        roundCorners()
    }
    
    func roundCorners() {
        let layer = CAShapeLayer()
        layer.frame = bounds
        layer.path = getCornerPath()
        self.layer.mask = layer
    }
    
    func getCornerPath() -> CGPath {
        return UIBezierPath(roundedRect: bounds, byRoundingCorners: getCorners(), cornerRadii: CGSize(width: cornerRadius, height: cornerRadius)).cgPath
    }
    
    func getCorners() -> UIRectCorner {
        var result = UIRectCorner()
        
        if roundTopLeft {
            result.insert(.topLeft)
        }
        
        if roundTopRight {
            result.insert(.topRight)
        }
        
        if roundBottomLeft {
            result.insert(.bottomLeft)
        }
        
        if roundBottomRight {
            result.insert(.bottomRight)
        }
        
        return result
    }

}
