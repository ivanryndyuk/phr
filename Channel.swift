//
//  Channel.swift
//  phr.kz
//
//  Created by ivan on 08/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class Channel: NSObject, NSCoding {

    //MARK: - VARS
    
    var sid: String
    var name: String
    var uniqueName: String
    var serviceSid: String
    var members: [ChannelMember]
    
    
    
    //MARK: - LIFE
    
    init?(json: JsonObject) {
        guard let sid = json["sid"] as? String else {
            return nil
        }
        
        guard let name = json["name"] as? String else {
            return nil
        }
        
        guard let uniqueName = json["uniqueName"] as? String else {
            return nil
        }
        
        guard let serviceSid = json["serviceSid"] as? String else {
            return nil
        }
        
        guard let membersJson = json["members"] as? JsonArray else {
            return nil
        }
        
        var members = [ChannelMember]()
        
        for case let memberItem as JsonObject in membersJson {
            if let member = ChannelMember(json: memberItem) {
                members.append(member)
            }
        }
        
        self.sid = sid
        self.name = name
        self.uniqueName = uniqueName
        self.serviceSid = serviceSid
        self.members = members
    }
    
    required init?(coder aDecoder: NSCoder) {
        guard let sid = aDecoder.decodeObject(forKey: "sid") as? String else {
            return nil
        }
        
        guard let name = aDecoder.decodeObject(forKey: "name") as? String else {
            return nil
        }
        
        guard let uniqueName = aDecoder.decodeObject(forKey: "uniqueName") as? String else {
            return nil
        }
        
        guard let serviceSid = aDecoder.decodeObject(forKey: "serviceSid") as? String else {
            return nil
        }
        
        guard let members = aDecoder.decodeObject(forKey: "members") as? [ChannelMember] else {
            return nil
        }
        
        self.sid = sid
        self.name = name
        self.uniqueName = uniqueName
        self.serviceSid = serviceSid
        self.members = members
    }
    
    
    
    //MARK: - FUNCS
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(sid, forKey: "sid")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(uniqueName, forKey: "uniqueName")
        aCoder.encode(serviceSid, forKey: "serviceSid")
        aCoder.encode(members, forKey: "members")
    }
    
}
