//
//  VCComplaint.swift
//  phr.kz
//
//  Created by ivan on 31/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import TwilioChatClient

class VCComplaint: ViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var bottomOffset: NSLayoutConstraint!
    @IBOutlet weak var textView: UITextView!
    
    var consultation: Consultation?
    
    
    
    //MARK: - LIFE

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitleView?.titleLabel.textColor = UIColor(hex: 0xFFFFFF)
        navigationTitleView?.title = "Подать жалобу".localized
        
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillShow:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillHide:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        textView.contentInset = .zero
        textView.textContainerInset = UIEdgeInsetsMake(20.0, 20.0, 20.0, 20.0)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        textView.becomeFirstResponder()
    }
    
    
    
    //MARK: - FUNCS

    @IBAction func handlerOf(backItem: UIButton) {
        hideKeyboard()
        dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func handlerOf(tapGesture: UITapGestureRecognizer) {
        hideKeyboard()
    }
    
    
    func hideKeyboard() {
        view.endEditing(false)
    }
    
    
    func handlerOf(keyboardWillShow note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = keyboardBounds.height - self.bottomLayoutGuide.length
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func handlerOf(keyboardWillHide note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = 0.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    @IBAction func complaint(button: UIBarButtonItem) {
        hideKeyboard()
        
        guard let consultationId = consultation?.id else {
            return
        }
        
        guard let consultationType = consultation?.type else {
            return
        }
        
        if textView.text.isEmpty {
            let alert = UIAlertController(title: nil, message: "Напишите текст жалобы.".localized, preferredStyle: .alert)
            self.present(alert, animated: true, completion: { 
                DispatchQueue.main.asyncAfter(deadline: .now() + .seconds(2), execute: {
                    alert.dismiss(animated: true, completion: nil)
                })
            })
            return
        }
        
        let alert = UIAlertController(title: nil, message: "Вы уверены, что хотите пожаловаться на этого врача?".localized, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Нет".localized, style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Да".localized, style: .default, handler: { (action) in
            app.showLoader()
            app.api.complaint.post(complaint: self.textView.text, id: consultationId, type: consultationType, callback: { () in
                app.hideLoader()
                let confirm = UIAlertController(title: nil, message: "Ваша жалоба успешно отправлена.".localized, preferredStyle: .alert)
                
                confirm.addAction(UIAlertAction(title: "ОК".localized, style: .default, handler: { (action) in
                    self.dismiss(animated: true, completion: nil)
                }))
                
                self.present(confirm, animated: true, completion: nil)
            }, fallback: { (error) in
                app.hideLoader()
                self.alert(error: error)
            })
        }))
        
        present(alert, animated: true, completion: nil)
    }

}
