//
//  Keys.swift
//  phr.kz
//
//  Created by ivan on 18/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class Keys: NSObject {
    
    let appLocale = "appLocale"
    let notFirstLaunch = "notFirstLaunchKey"
    let authorization = "authorizationKey"
    let userName = "userName"
    let messageAdditionPhoto = "messageAdditionPhoto"
    let authorizationToken = "authorizationToken"
    
    let newMessageNote = "newMessageNote"
    let chatMemberUpdate = "chatMemberUpdate"
    let checkListTableDidScrollNote = "checkListTableDidScrollNote"
    let channelAddedNote = "channelAddedNote"
    let channelDeletedNote = "channelDeletedNote"
    let channelUpdatedNote = "channelUpdatedNote"
    let synchronizationCompleted = "synchronizationCompleted"
    let messageUpdateNote = "messageUpdateNote"
    let messageDeleteNote = "messageDeleteNote"
    let reachabilityChangeNote = "reachabilityChangeNote"
    
}
