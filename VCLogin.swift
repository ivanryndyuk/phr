//
//  VCLogin.swift
//  phr.kz
//
//  Created by ivan on 18/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCLogin: UIViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var controlsBlock: UIView!
    @IBOutlet weak var formBlock: UIView!
    @IBOutlet weak var languageButton: UIButton!
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    @IBOutlet weak var loginBorder: UIView!
    @IBOutlet weak var passwordBorder: UIView!
    
    let tranimator = Tranimator(style: .slideUp)
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    //MARK: - LIFE

    override func viewDidLoad() {
        super.viewDidLoad()
        
        switch app.locale.identifier {
        case "en":
            languageButton.setImage(#imageLiteral(resourceName: "en"), for: .normal)
        case "kk_KZ":
            languageButton.setImage(#imageLiteral(resourceName: "kz"), for: .normal)
        default:
            languageButton.setImage(#imageLiteral(resourceName: "ru"), for: .normal)
        }
        
        loginTextField.delegate = self
        passwordField.delegate = self
        
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillShow:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillHide:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        app.delegate.background?.showPattern()
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(tapGesture: UITapGestureRecognizer) {
        hideKeyboard()
    }
    
    
    @IBAction func handlerOf(signInButton: Button) {
        login()
    }
    
    
    @IBAction func handlerOf(languageButton: UIButton) {
        let actions = UIAlertController(title: nil, message: "Выберите язык приложения".localized, preferredStyle: .actionSheet)
        
        for locale in app.locales {
            if let code = locale.languageCode {
                actions.addAction(UIAlertAction(title: locale.localizedString(forLanguageCode: code)?.capitalized, style: .default, handler: { (action) in
                    actions.dismiss(animated: true, completion: nil)
                    self.change(locale: locale)
                }))
            }
        }
        
        actions.addAction(UIAlertAction(title: "Отмена".localized, style: .cancel, handler: { (action) in
            actions.dismiss(animated: true, completion: nil)
        }))
        
        present(actions, animated: true, completion: nil)
    }
    
    
    func hideKeyboard() {
        view.endEditing(false)
    }
    
    
    func change(locale: Locale) {
        if app.locale.identifier == locale.identifier {
            return
        }
        app.locale = locale
        app.delegate.background?.hidePattern()
        
        if let auth = UIStoryboard(name: "Auth", bundle: nil).instantiateInitialViewController() {
            auth.view.alpha = 0.0
            
            let loading = LoadingView.instance(message: app.settingLanguageMessage(forLocale: locale))
            loading.frame = UIScreen.main.bounds
            loading.alpha = 0.0
            loading.loader.startAnimating()
            app.delegate.window?.addSubview(loading)
            
            UIView.animate(withDuration: 0.4, animations: {
                loading.alpha = 1.0
                app.delegate.window?.rootViewController?.view.alpha = 0.0
            }, completion: { (finished) in
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.5, execute: {
                    app.delegate.window?.insertSubview(auth.view, belowSubview: loading)
                    UIView.animate(withDuration: 0.4, animations: {
                        loading.alpha = 0.0
                        auth.view.alpha = 1.0
                    }, completion: { (finished) in
                        app.delegate.window?.rootViewController = auth
                        app.delegate.background?.showPattern()
                    })
                })
            })
        }
    }
    
    
    func handlerOf(keyboardWillShow note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        let oldCenterY = self.formBlock.center.y
        let newCenterY = (self.view.bounds.height - keyboardBounds.height) / 2.0
        let offset = newCenterY - oldCenterY
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.controlsBlock.alpha = 0.0
            self.formBlock.transform = CGAffineTransform(translationX: 0, y: offset)
        }, completion: nil)
    }
    
    
    func handlerOf(keyboardWillHide note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.controlsBlock.alpha = 1.0
            self.formBlock.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    
    func login() {
        guard let login = loginTextField.text else {
            app.alert(message: "Укажите логин.".localized)
            return
        }
        
        if login.isEmpty {
            app.alert(message: "Укажите логин.".localized)
            return
        }
        
        if !app.validator.validate(phone: login) && !app.validator.validate(email: login) {
            app.alert(message: "Логином должен быть номер телефона или адрес электронной почты.".localized)
            return
        }
        
        guard let password = passwordField.text else {
            app.alert(message: "Укажите пароль.".localized)
            return
        }
        
        if password.isEmpty {
            app.alert(message: "Укажите пароль.".localized)
            return
        }
        
        app.api.user.authorize(login: login, password: password, callback: {
            app.api.cabinet.selfuser(callback: { (json) in
                app.user = app.stack.user()
                app.user?.apply(params: json)
                app.stack.save()
                app.delegate.transitToStart()
            }, fallback: { (error) in
                self.alert(error: error)
            })
        }) { (error) in
            self.alert(error: error)
        }
        
        //app.ud.set(text, forKey: "AUTHTOKEN")
        //app.twilio.messagingManager.login(withUserName: "AC61f53f121967d329d3d332a463d0ccb6")
    }
    
    
    
    //MARK: - NAV
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == app.segues.presentResetPassword {
            segue.destination.transitioningDelegate = self
            segue.destination.modalPresentationStyle = .custom
        } else if segue.identifier == app.segues.presentSignup {
            segue.destination.transitioningDelegate = self
            segue.destination.modalPresentationStyle = .custom
        }
    }

}



//MARK: - UITextFieldDelegate

extension VCLogin: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == loginTextField {
            loginBorder.backgroundColor = UIColor(hex: 0x79EDBA)
            passwordBorder.backgroundColor = UIColor(r: 209, g: 204, b: 209, a: 0.3)
        } else if textField == passwordField {
            loginBorder.backgroundColor = UIColor(r: 209, g: 204, b: 209, a: 0.3)
            passwordBorder.backgroundColor = UIColor(hex: 0x79EDBA)
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField == loginTextField {
            loginBorder.backgroundColor = UIColor(r: 209, g: 204, b: 209, a: 0.3)
        } else if textField == passwordField {
            passwordBorder.backgroundColor = UIColor(r: 209, g: 204, b: 209, a: 0.3)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == loginTextField {
            passwordField.becomeFirstResponder()
        } else if textField == passwordField {
            login()
        }
        
        return true
    }
    
}



//MARK: - UIViewControllerTransitioningDelegate

extension VCLogin: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        tranimator.presenting = true
        tranimator.hideBackgroundPattern = true
        return tranimator
    }
    
    
    
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        tranimator.presenting = false
        tranimator.hideBackgroundPattern = true
        return tranimator
    }
    
}
