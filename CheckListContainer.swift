//
//  CheckListContainer.swift
//  phr.kz
//
//  Created by ivan on 14/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class CheckListContainer: UIView {
    
    //MARK: - VARS
    
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var checkListButton: ImageButton!
    @IBOutlet weak var progressBar: ProgressBarExtended!
    @IBOutlet weak var statusTitle: UILabel!
    
    var checkListView: CheckListView!
    var isExpanded = false
    var canEdit = false {
        didSet {
            checkListView.addButton.isHidden = !canEdit
        }
    }
    var hasChanges = false
    var soid: String?
    
    var list = [CheckListItem]()
    var selectedStatus = 0 {
        didSet{
            progressBar.bar.set(progress: (CGFloat(selectedStatus) + 1.0) / 3.0, for: progressBar.frame)
            statusTitle.text = so?.status?.name
        }
    }
    
    let loading = LoadingView.instance(message: "")
    let empty = EmptyView.instance(text: "Чек-лист пуст.".localized)
    var viewController: UIViewController?
    var isLoading = false
    var so: SO?
    
    

    //MARK: - FUNCS
    
    override func awakeFromNib() {
        super.awakeFromNib()
        checkListView = CheckListView.instance(dataSource: self, delegate: self)
        setupCheckList()
        checkListView.addButton.addTarget(self, action: #selector(add(button:)), for: .touchUpInside)
        empty.color = UIColor(r: 255, g: 255, b: 255, a: 0.5)
    }
    
    
    func setupCheckList() {
        container.addSubview(checkListView)
        checkListView.translatesAutoresizingMaskIntoConstraints = false
        
        container.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:|-0-[check]-0-|",
            options: .zero,
            metrics: nil,
            views: ["check": checkListView]))
        
        container.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:|-0-[check]-0-|",
            options: .zero,
            metrics: nil,
            views: ["check": checkListView]))
        
        checkListView.checkListTable.tableFooterView = UIView()
    }
    
    
    func update(so: SO) {
        self.so = so
        
        if let items = so.checkListItems {
            list = items.sorted(by: { $0.name > $1.name })
        }
        
        soid = so.id
        selectedStatus = 0
        checkListButton.title = "Чек-лист \(countSelectedItems())/\(list.count)"
        checkListView.checkListTable.reloadData()
        
        if let status = so.status {
            switch status.code {
            case "in_progress":
                selectedStatus = 1
            case "completed":
                selectedStatus = 2
            default:
                selectedStatus = 0
            }
        }
        
        if list.count == 0 {
            checkListView.checkListTable.backgroundView = empty
        } else {
            checkListView.checkListTable.backgroundView = nil
        }
    }
    
    
    func willDisplay(statusCell: CheckListStatusCell?, indexPath: IndexPath) {
        statusCell?.selectionStyle = .none
        
        if indexPath.row <= selectedStatus {
            statusCell?.mode = .done
        } else {
            if indexPath.row == selectedStatus + 1 {
                statusCell?.mode = .current
            } else {
                statusCell?.mode = .inactive
            }
        }
        
        switch indexPath.row {
        case 0:
            statusCell?.title = "Заявка".localized
        case 1:
            statusCell?.title = "Активно".localized
        case 2:
            statusCell?.title = "Завершено".localized
        default:
            break
        }
    }
    
    
    func willDisplay(itemCell: CheckListItemCell?, indexPath: IndexPath) {
        itemCell?.selectionStyle = .none
        
        let item = list[indexPath.item]
        
        itemCell?.title = item.name
        itemCell?.parent = checkListView.checkListTable
        itemCell?.delegate = self
        itemCell?.isChecked = item.complete
        itemCell?.canEdit = canEdit
    }
    
    
    func save(button: UIButton) {
        self.hasChanges = false
        app.alert(title: nil, message: "Изменения успешно сохранены.")
    }
    
    
    func add(button: UIButton) {
        viewController?.performSegue(withIdentifier: app.segues.presentEdit, sender: nil)
    }
    
    
    func countSelectedItems() -> Int {
        var count = 0
        
        for item in list {
            if item.complete {
                count += 1
            }
        }
        
        return count
    }
    
    
    func showLoading() {
        isLoading = true
        loading.frame = frame
        addSubview(loading)
    }
    
    
    func hideLoading() {
        isLoading = false
        loading.removeFromSuperview()
    }
    
    
    func update(item: CheckListItem, newComplete: Bool) {
        if isLoading {
            return
        }
        
        guard let soid = soid else {
            return
        }
        
        showLoading()
        
        app.api.so.update(checkListItem: item, newComplete: newComplete, SOID: soid, callback: { (so) in
            app.stack.save()
            self.hideLoading()
            self.update(so: so)
        }) { (error) in
            self.hideLoading()
            app.alert(error: error)
        }
    }
    
    
    func delete(item: CheckListItem) {
        if isLoading {
            return
        }
        
        guard let soid = soid else {
            return
        }
        
        showLoading()
        
        app.api.so.delete(checkListItem: item, SOID: soid, callback: { (so) in
            self.update(so: so)
            self.hideLoading()
        }) { (error) in
            self.hideLoading()
            app.alert(error: error)
        }
    }

}



//MARK: - UITableViewDataSource

extension CheckListContainer: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == checkListView.statusTable {
            return 3
        } else {
            return list.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var id: String
        
        if tableView == checkListView.statusTable {
            id = "checkListStatusCell"
        } else {
            id = "checkListItemCell"
        }
        
        return tableView.dequeueReusableCell(withIdentifier: id, for: indexPath)
    }
    
}



//MARK: - UITableViewDelegate

extension CheckListContainer: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == checkListView.statusTable {
            willDisplay(statusCell: cell as? CheckListStatusCell, indexPath: indexPath)
        } else {
            willDisplay(itemCell: cell as? CheckListItemCell, indexPath: indexPath)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !canEdit {
            return
        }
        
        if !hasChanges {
            hasChanges = true
        }
        
        //TODO: Can't edit status yet.
        
        /*if tableView == checkListView.statusTable {
            selectedStatus = indexPath.row
            tableView.reloadSections(IndexSet(integer: 0), with: .fade)
        } else*/
        
        if tableView == checkListView.checkListTable {
            update(item: list[indexPath.row], newComplete: !list[indexPath.row].complete)
        }
    }
    
    
    /*func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        if !canEdit {
            return nil
        }
        
        if tableView == checkListView.checkListTable {
            let edit = UITableViewRowAction(style: .default, title: "", handler: { (action, indexPath) in
                self.editHandler(rowAction: action, indexPath: indexPath)
            })
            edit.backgroundColor = UIColor(patternImage: #imageLiteral(resourceName: "icon-edit"))
            
            let delete = UITableViewRowAction(style: .default, title: "", handler: { (action, indexPath) in
                self.editHandler(rowAction: action, indexPath: indexPath)
            })
            delete.backgroundColor = UIColor(r: 0, g: 0, b: 0, a: 0.1)
            
            return [edit, delete]
        }
        
        return nil
    }*/
    
}



extension CheckListContainer: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView == checkListView.checkListTable {
            app.nc.post(name: app.notification(name: app.keys.checkListTableDidScrollNote), object: nil)
        }
    }
    
}



//MARK: - CheckListItemCellDelegate

extension CheckListContainer: CheckListItemCellDelegate {
    
    func editItem(forCell cell: CheckListItemCell) {
        if !hasChanges {
            hasChanges = true
        }
        
        if let indexPath = checkListView.checkListTable.indexPath(for: cell) {
            viewController?.performSegue(withIdentifier: app.segues.presentEdit, sender: list[indexPath.row])
        }
    }
    
    
    func deleteItem(forCell cell: CheckListItemCell) {
        if let indexPath = checkListView.checkListTable.indexPath(for: cell) {
            let item = list[indexPath.row]
            let alert = UIAlertController(title: "ОСТОРОЖНО".localized, message: "Вы действительно хотите удалить этот пункт?".localized + " \n\(item.name)", preferredStyle: .alert)
            
            let confirm = UIAlertAction(title: "Да".localized, style: .destructive) { (action) in
                let item = self.list[indexPath.row]
                self.delete(item: item)
            }
            
            let cancel = UIAlertAction(title: "Отмена".localized, style: .default) { (action) in
                alert.dismiss(animated: true, completion: nil)
            }
            
            alert.addAction(confirm)
            alert.addAction(cancel)
            
            viewController?.present(alert, animated: true, completion: nil)
        }
    }
    
}


class Item: NSObject {
    
    var text: String
    var isSelected = false
    
    init(text: String) {
        self.text = text
    }
    
}
