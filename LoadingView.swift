//
//  LoadingView.swift
//  phr.kz
//
//  Created by ivan on 07/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    //MARK: - VARS
    
    @IBOutlet weak var loader: Loader!
    @IBOutlet weak var messageLabel: UILabel!
    
    var message: String? {
        get {
            return messageLabel.text
        }
        
        set {
            messageLabel.text = newValue
        }
    }
    
    
    
    //MARK: - LIFE
    
    class func instance(message: String) -> LoadingView {
        if let instance = Bundle.main.loadNibNamed("LoadingView", owner: nil, options: nil)?.first as? LoadingView {
            instance.message = message
            instance.loader.startAnimating()
            return instance
        } else {
            return LoadingView()
        }
    }

}
