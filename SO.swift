//
//  SO.swift
//  phr.kz
//
//  Created by ivan on 16/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import CoreData
import TwilioChatClient

class SO: Consultation {
    
    //MARK: - VARS
    
    @NSManaged var requestDoctor: Doctor?
    @NSManaged var status: SOStatus?
    @NSManaged var checkListItems: [CheckListItem]?
    @NSManaged var conclusionText: String?
    @NSManaged var conclusion: String?
    
    
    
    //MARK: - FUNCS
    
    override func getTitle() -> String {
        var result = "SO"
        
        if app.user?.type == .some(.doctor) {
            if let name = patient?.getName() {
                result = name
            }
        } else {
            if let name = doctor?.getName() {
                result = name
            } else if let name = requestDoctor?.getName() {
                result = name
            }
        }
        
        return result
    }
    
}


/*
//MARK: - SOStatus

enum SOStatus {
    case bid, active, closed
    
    static func with(hash: Int) -> SOStatus {
        switch hash {
        case 0:
            return .bid
        case 1:
            return .active
        default:
            return .closed
        }
    }
}*/
