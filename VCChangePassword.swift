//
//  VCChangePassword.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCChangePassword: ViewController {

    //MARK: - VARS
    
    @IBOutlet weak var changePasswordTable: UITableView!
    
    let reuseId = "textFieldCell"
    
    
    
    //MARK: - LIFE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitleView?.titleColor = UIColor.white
        navigationTitleView?.title = "Изменить пароль".localized
        
        changePasswordTable.dataSource = self
        changePasswordTable.delegate = self
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(backItem: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func handlerOf(saveItem: UIBarButtonItem) {
        // save acion and after that pop
        updatePassword()
    }
    
    func updatePassword() {
        guard let oldPassword = (changePasswordTable.cellForRow(at: IndexPath(row: 0, section: 0)) as? TextFieldCell)?.textField.text else {
            return
        }
        
        guard let newPassword = (changePasswordTable.cellForRow(at: IndexPath(row: 1, section: 0)) as? TextFieldCell)?.textField.text else {
            return
        }
        
        guard let repeatNewPassword = (changePasswordTable.cellForRow(at: IndexPath(row: 2, section: 0)) as? TextFieldCell)?.textField.text else {
            return
        }
        
        if oldPassword.isEmpty {
            app.alert(message: "Укажите текущий пароль.".localized)
            return
        }
        
        navigationTitleView?.startLoading(message: "Изменение пароля".localized)
        
        if app.user?.type == .some(.patient) {
            app.api.user.updatePatient(params: ["user[password]": oldPassword, "user[newPassword][first]": newPassword, "user[newPassword][second]": repeatNewPassword], callback: { (user) in
                self.navigationTitleView?.stopLoading()
                self.dismissAlert()
            }, fallback: { (error) in
                self.navigationTitleView?.stopLoading()
                app.alert(error: error)
            })
        } else {
            app.api.user.updateDoctor(params: ["user[password]": oldPassword, "user[newPassword][first]": newPassword, "user[newPassword][second]": repeatNewPassword], callback: { (user) in
                self.navigationTitleView?.stopLoading()
                self.dismissAlert()
            }, fallback: { (error) in
                self.navigationTitleView?.stopLoading()
                app.alert(error: error)
            })
        }
    }
    
    func dismissAlert() {
        let alert = UIAlertController(title: nil, message: "Пароль успешно изменён.".localized, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
            self.navigationController?.popViewController(animated: true)
        }))
        
        present(alert, animated: true, completion: nil)
    }

}



//MARK: - UITableViewDataSource

extension VCChangePassword: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath)
    }
    
}



//MARK: - UITableViewDelegate

extension VCChangePassword: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let cell = cell as? TextFieldCell
        cell?.textField.isSecureTextEntry = true
        
        switch indexPath.row {
        case 0:
            cell?.placeholder = "Старый пароль".localized
        case 1:
            cell?.placeholder = "Новый пароль".localized
        case 2:
            cell?.placeholder = "Еще раз новый пароль".localized
        default:
            break
        }
    }
    
}
