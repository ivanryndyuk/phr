//
//  Contract.swift
//  phr.kz
//
//  Created by ivan on 08/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import CoreData
import TwilioChatClient

class Contract: Consultation {

    //MARK: - VARS
    
    @NSManaged var startDate: Date?
    @NSManaged var endDate: Date?
    
    
    
    //MARK: - FUNCS
    
    func getSicknesses() -> [Sickness] {
        var result = [Sickness]()
        
        if let array = sicknesses?.sortedArray(using: [NSSortDescriptor(key: "name", ascending: true)]) as? [Sickness] {
            result = array
        }
        
        return result
    }
    
    
    func getSicknessesString() -> String {
        var result = ""
        
        for (i, sickness) in getSicknesses().enumerated() {
            if let name = sickness.name {
                if i == 0 {
                    result = name
                } else {
                    result += ", \(name)"
                }
            }
        }
        
        return result
    }
    
    
    func getOrganizationTitle() -> String {
        var result = ""
        
        if let name = organization?.name {
            result = name
        }
        
        return result
    }
    
}
