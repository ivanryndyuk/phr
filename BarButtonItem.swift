//
//  BarButtonItem.swift
//  phr.kz
//
//  Created by ivan on 18/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

@IBDesignable class BarButtonItem: UIBarButtonItem {

    @IBInspectable var useOriginalImage: Bool = false {
        didSet {
            if useOriginalImage {
                image = image?.withRenderingMode(.alwaysOriginal)
            } else {
                image = image?.withRenderingMode(.alwaysTemplate)
            }
        }
    }
    
    
    @IBInspectable var fontName: String = "Lato-Regular" {
        didSet {
            updateFont()
        }
    }
    
    
    @IBInspectable var fontSize: CGFloat = 17.0 {
        didSet {
            updateFont()
        }
    }
    
    
    
    //MARK: - FUNCS
    
    private func updateFont() {
        setTitleTextAttributes([NSFontAttributeName: UIFont.with(name: fontName, size: fontSize)], for: .normal)
    }
    
}
