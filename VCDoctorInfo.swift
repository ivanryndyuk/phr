//
//  VCDoctorInfo.swift
//  phr.kz
//
//  Created by ivan on 24/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCDoctorInfo: ModalController {
    
    //MARK: - VARS
    
    @IBOutlet weak var descriptionView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var specialitiesLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var startSOButton: UIButton!
    @IBOutlet weak var languageView: UIImageView!
    
    var canStartSO = false
    var descriptionAttrs = [String: Any]()
    var doctor: Doctor?
    let loadingView = LoadingView.instance(message: "")
    var isLoading = false
    
    
    
    //MARK: - LIFE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let ps = NSMutableParagraphStyle()
        ps.minimumLineHeight = 22.0
        ps.maximumLineHeight = 22.0
        ps.lineBreakMode = .byWordWrapping
        
        descriptionAttrs[NSFontAttributeName] = UIFont.with(name: app.theme.fontMuseoSansCyrl, size: 14.0)
        descriptionAttrs[NSForegroundColorAttributeName] = UIColor(hex: 0x757475)
        descriptionAttrs[NSParagraphStyleAttributeName] = ps
        
        descriptionView.attributedText = NSAttributedString(string: "Снижение синтеза белка: недостаточное поступление белка в организм с пищей (голодание, панкреатиты, энтероколиты, опухоли, последствия оперативных вмешательств); синдром мальабсорбции; заболевания печени (циррозы, гепатиты, карцинома и метастазы опухолей в печень, токсическое поражение печени) Снижение синтеза белка: недостаточное поступление белка в организм с пищей (голодание, панкреатиты, энтероколиты, опухоли, последствия оперативных вмешательств); синдром мальабсорбции; заболевания печени (циррозы, гепатиты, карцинома и метастазы опухолей в печень, токсическое поражение печени) Снижение синтеза белка: недостаточное поступление белка в организм с пищей (голодание, панкреатиты, энтероколиты, опухоли, последствия оперативных вмешательств); синдром мальабсорбции; заболевания печени (циррозы, гепатиты, карцинома и метастазы опухолей в печень, токсическое поражение печени)", attributes: descriptionAttrs)
        
        startSOButton.isHidden = !canStartSO
        
        if let doctor = doctor {
            setup(doctor: doctor)
        }
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(closeButton: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func handlerOf(startSOButton: UIButton) {
        let alert = UIAlertController(title: nil, message: "Вы уверены, что хотите создать новую заявку на консультацию Second Opinion?".localized, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Отмена".localized, style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Да".localized, style: .default, handler: { (action) in
            self.createNewSO()
            alert.dismiss(animated: true, completion: nil)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func setup(doctor: Doctor) {
        titleLabel.text = doctor.fullName()
        specialitiesLabel.text = doctor.getSpecialities()
        priceLabel.text = "\(doctor.price)$"
        languageView.image = doctor.getLanguageImage()
    }
    
    func showLoading() {
        isLoading = true
        loadingView.frame = view.frame
        view.addSubview(loadingView)
    }
    
    
    func hideLoading() {
        isLoading = false
        loadingView.removeFromSuperview()
    }
    
    
    func createNewSO() {
        if isLoading {
            return
        }
        
        if !canStartSO {
            return
        }
        
        showLoading()
        
        if let id = doctor?.id {
            app.api.so.newSO(requestDoctor: id, callback: { (so) in
                self.hideLoading()
                self.handlerOfSOCreation()
            }, fallback: { (error) in
                app.alert(error: error)
                self.hideLoading()
            })
        }
        
    }
    
    
    func handlerOfSOCreation() {
        app.nc.post(name: app.notification(name: "reloadContracts"), object: nil)
        let alert = UIAlertController(title: nil, message: "Новая заявка на консультацию Second Opinion успешно создана.".localized, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK".localized, style: .default, handler: { (action) in
            self.dismiss(animated: true, completion: nil)
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
