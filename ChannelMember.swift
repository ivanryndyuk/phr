//
//  ChannelMember.swift
//  phr.kz
//
//  Created by ivan on 08/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ChannelMember: NSObject, NSCoding {

    //MARK: - VARS
    
    var sid: String
    var identity: String
    var name: String
    var serviceSid: String
    
    
    
    //MARK: - LIFE
    
    init?(json: JsonObject) {
        guard let sid = json["sid"] as? String else {
            return nil
        }
        
        guard let identity = json["identity"] as? String else {
            return nil
        }
        
        guard let name = json["name"] as? String else {
            return nil
        }
        
        guard let serviceSid = json["serviceSid"] as? String else {
            return nil
        }
        
        self.sid = sid
        self.identity = identity
        self.name = name
        self.serviceSid = serviceSid
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        guard let sid = aDecoder.decodeObject(forKey: "sid") as? String else {
            return nil
        }
        
        guard let identity = aDecoder.decodeObject(forKey: "identity") as? String else {
            return nil
        }
        
        guard let name = aDecoder.decodeObject(forKey: "name") as? String else {
            return nil
        }
        
        guard let serviceSid = aDecoder.decodeObject(forKey: "serviceSid") as? String else {
            return nil
        }
        
        self.sid = sid
        self.identity = identity
        self.name = name
        self.serviceSid = serviceSid
    }
    
    
    
    //MARK: - FUNCS
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(sid, forKey: "sid")
        aCoder.encode(identity, forKey: "identity")
        aCoder.encode(name, forKey: "name")
        aCoder.encode(serviceSid, forKey: "serviceSid")
    }
    
}
