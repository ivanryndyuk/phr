//
//  FiltersCell.swift
//  phr.kz
//
//  Created by ivan on 24/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class FiltersCell: UITableViewCell {
    
    //MARK: - VARS
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    
    var title: String? {
        get {
            return titleLabel.text
        }
        
        set {
            titleLabel.text = newValue
        }
    }
    
    var detail: String? {
        get {
            return detailLabel.text
        }
        
        set {
            detailLabel.text = newValue
        }
    }
    
}
