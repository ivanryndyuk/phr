//
//  VCSearch.swift
//  phr.kz
//
//  Created by ivan on 24/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCSearch: ViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var doctorsTable: UITableView!
    @IBOutlet weak var filtersTable: UITableView!
    @IBOutlet weak var subMenuViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var searchItem: UIBarButtonItem!
    @IBOutlet weak var filterItem: UIBarButtonItem!
    @IBOutlet weak var bottomOffset: NSLayoutConstraint!
    
    var selectedItem: UIBarButtonItem?
    let tranimator = Tranimator(style: .modal)
    let reuseId = "doctorCell"
    let reuseFilterId = "filterCell"
    var doctors = [Int: [Doctor]]()
    var meta = Meta(json: [
        "items_per_page": 30,
        "current_page": 1,
        "total_pages": 1,
        "sort": "entity.lastName",
        "direction": "asc",
        "total": 0
        ]
    )
    var isLoading = false
    let emptyView = EmptyView.instance(text: "Список врачей пуст.", image: #imageLiteral(resourceName: "icon-doctor"))
    var searchName: String? = nil
    var specialityFilters = [Speciality]()
    var sicknessFilters = [Sickness]()
    var languageFilters = [Language]()
    let loadingView = LoadingView.instance(message: "")
    
    
    
    //MARK: - LIFE

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitleView?.titleColor = UIColor.white
        navigationTitleView?.title = "Second Opinion".localized
        
        doctorsTable.register(UINib(nibName: "DoctorCell", bundle: nil), forCellReuseIdentifier: reuseId)
        doctorsTable.dataSource = self
        doctorsTable.delegate = self
        
        filtersTable.dataSource = self
        filtersTable.delegate = self
        
        searchBar.delegate = self
        
        emptyView.set(delegate: self, actionTitle: "Обновить")
        
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillShow:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillHide:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        filtersTable.reloadData()
        loadDoctors()
    }
    
    
    
    //MARK: - NAV
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == app.segues.presentDoctorInfo {
            segue.destination.transitioningDelegate = self
            segue.destination.modalPresentationStyle = .custom
            
            let controller = segue.destination as? VCDoctorInfo
            controller?.doctor = sender as? Doctor
            controller?.canStartSO = true
        } else if segue.identifier == app.segues.presentFilters {
            let controller = (segue.destination as? NavigationController)?.viewControllers[0] as? VCFilters
            if let hash = sender as? Int {
                let mode = FiltersMode.with(hash: hash)
                controller?.mode = mode
                controller?.sender = self
                
                switch mode {
                case .speciality:
                    controller?.specialitiesSelected = specialityFilters
                case .sickness:
                    controller?.sicknessesSelected = sicknessFilters
                case .language:
                    controller?.languagesSelected = languageFilters
                }
            }
        }
    }
    
    
    
    //MARK: - FUNCS
    
    func loadDoctors(page: Int = 0) {
        guard let meta = meta else {
            return
        }
        
        if isLoading {
            return
        }
        
        isLoading = true
        
        app.api.doctors.doctors(limit: meta.itemsOnPage, page: page, sort: meta.sort, direction: meta.direction, name: searchName, specialities: specialityFilters, sicknesses: sicknessFilters, languages: languageFilters, callback: { (meta, doctors) in
            self.meta = meta
            self.updateTable(withDoctors: doctors, page: page)
            self.isLoading = false
        }) { (error) in
            app.alert(error: error)
            self.doctorsTable.reloadData()
            self.isLoading = false
        }
    }
    
    
    func updateTable(withDoctors doctors: [Doctor], page: Int) {
        var shouldInsert = true
        
        if page < self.doctors.count {
            shouldInsert = false
        }
        
        self.doctors[page] = doctors
        
        if doctors.isEmpty {
            doctorsTable.backgroundView = emptyView
        } else {
            doctorsTable.backgroundView = nil
        }
        
        if shouldInsert {
            doctorsTable.insertSections(IndexSet(integer: page), with: .fade)
        } else {
            doctorsTable.reloadSections(IndexSet(integer: page), with: .fade)
        }
    }
    
    
    @IBAction func handlerOf(item: UIBarButtonItem) {
        if let selected = selectedItem {
            if selected == item {
                selectedItem = nil
                hideSubMenuView()
                return
            }
        }
        
        selectedItem = item
        
        if item == searchItem {
            showSearchBar()
        } else if item == filterItem {
            showFiltersMenu()
        }
    }
    
    
    func hideKeyboard() {
        view.endEditing(false)
    }
    
    
    func handlerOf(keyboardWillShow note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = keyboardBounds.height + self.bottomLayoutGuide.length
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func handlerOf(keyboardWillHide note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds: CGRect = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = 0.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func showSearchBar() {
        searchItem.tintColor = UIColor(hex: 0xF5E9CD)
        filterItem.tintColor = UIColor(hex: 0x24E4FE)
        UIView.animate(withDuration: 0.2) { 
            self.subMenuViewHeight.constant = 46.0
            self.searchBar.alpha = 1.0
            self.filtersTable.alpha = 0.0
            self.view.layoutIfNeeded()
        }
    }
    
    
    func showFiltersMenu() {
        searchItem.tintColor = UIColor(hex: 0x24E4FE)
        filterItem.tintColor = UIColor(hex: 0xF5E9CD)
        UIView.animate(withDuration: 0.2) {
            self.subMenuViewHeight.constant = 136.0
            self.searchBar.alpha = 0.0
            self.filtersTable.alpha = 1.0
            self.view.layoutIfNeeded()
        }
    }
    
    
    func hideSubMenuView() {
        searchItem.tintColor = UIColor(hex: 0x24E4FE)
        filterItem.tintColor = UIColor(hex: 0x24E4FE)
        UIView.animate(withDuration: 0.2, animations: { 
            self.subMenuViewHeight.constant = 0.0
            self.view.layoutIfNeeded()
        }) { (finished) in
            if finished {
                self.filtersTable.alpha = 0.0
                self.searchBar.alpha = 0.0
                self.searchName = nil
                self.hideKeyboard()
                self.loadDoctors()
            }
        }
    }
    
    
    func willDisplay(doctorCell cell: DoctorCell?, atIndexPath indexPath: IndexPath) {
        guard let doctor = doctors[indexPath.section]?[indexPath.row] else {
            return
        }
        
        cell?.title = doctor.fullName()
        cell?.price = "\(doctor.price)$"
        cell?.specialities = doctor.getSpecialities()
    }
    
    
    @IBAction func startSO(button: Button) {
        let alert = UIAlertController(title: nil, message: "Вы уверены, что хотите создать новую заявку на консультацию Second Opinion?".localized, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Отмена".localized, style: .default, handler: { (action) in
            alert.dismiss(animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Да".localized, style: .default, handler: { (action) in
            self.createNewSO()
            alert.dismiss(animated: true, completion: nil)
        }))
        
        present(alert, animated: true, completion: nil)
    }
    
    
    func createNewSO() {
        if isLoading {
            return
        }
        
        showLoading()
        
        app.api.so.newSO(requestDoctor: nil, callback: { (so) in
            self.hideLoading()
            app.nc.post(name: app.notification(name: "reloadContracts"), object: nil)
            app.alert(message: "Новая заявка на консультацию Second Opinion успешно создана.")
        }, fallback: { (error) in
            app.alert(error: error)
            self.hideLoading()
        })
    }
    
    
    func showLoading() {
        isLoading = true
        loadingView.frame = view.frame
        view.addSubview(loadingView)
    }
    
    
    func hideLoading() {
        isLoading = false
        loadingView.removeFromSuperview()
    }

}



//MARK: - UITableViewDataSource

extension VCSearch: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var count = 1
        
        if tableView == doctorsTable {
            count = doctors.count
        }
        
        return count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        
        if tableView == filtersTable {
            number = 3
        } else if let count = doctors[section]?.count {
            number = count
        }
        
        return number
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == filtersTable {
            return tableView.dequeueReusableCell(withIdentifier: reuseFilterId, for: indexPath)
        } else {
            return tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath)
        }
    }
    
}



//MARK: - UITableViewDelegate

extension VCSearch: UITableViewDelegate {
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == filtersTable {
            return 40.0
        } else {
            return 150.0
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        hideKeyboard()
        if tableView == filtersTable {
            switch indexPath.row {
            case 0:
                performSegue(withIdentifier: app.segues.presentFilters, sender: 1)
            case 1:
                performSegue(withIdentifier: app.segues.presentFilters, sender: 0)
            case 2:
                performSegue(withIdentifier: app.segues.presentFilters, sender: 2)
            default:
                break
            }
        } else {
            if let doctor = doctors[indexPath.section]?[indexPath.row] {
                performSegue(withIdentifier: app.segues.presentDoctorInfo, sender: doctor)
            }
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if tableView == filtersTable {
            cell.selectionStyle = .none
            let cell = cell as? FiltersCell
            
            switch indexPath.row {
            case 0:
                cell?.title = "Заболевание"
                if sicknessFilters.count == 0 {
                    cell?.detail = "Любая".localized
                } else {
                    cell?.detail = "\(sicknessFilters.count) " + "выбрано".localized
                }
            case 1:
                cell?.title = "Специальноть"
                if specialityFilters.count == 0 {
                    cell?.detail = "Любая".localized
                } else {
                    cell?.detail = "\(specialityFilters.count) " + "выбрано".localized
                }
            case 2:
                cell?.title = "Язык"
                if languageFilters.count == 0 {
                    cell?.detail = "Любая".localized
                } else {
                    cell?.detail = "\(languageFilters.count) " + "выбрано".localized
                }
            default:
                break
            }
        } else {
            willDisplay(doctorCell: cell as? DoctorCell, atIndexPath: indexPath)
        }
    }
    
}



//MARK: - UIViewControllerTransitioningDelegate

extension VCSearch: UIViewControllerTransitioningDelegate {
    
    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        tranimator.presenting = true
        return tranimator
    }
    
    
    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        tranimator.presenting = false
        return tranimator
    }
    
}



//MARK: - EmptyViewDelegate

extension VCSearch: EmptyViewDelegate {
    
    func handlerOf(actionButton: Button, emptyView: EmptyView) {
        loadDoctors()
    }
    
}



//MARK: - SerchBarDelegate

extension VCSearch: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchName = searchBar.text
        loadDoctors()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchName = nil
        loadDoctors()
    }
    
}
