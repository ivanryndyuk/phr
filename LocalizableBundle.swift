//
//  LocalizableBundle.swift
//  phr.kz
//
//  Created by ivan on 18/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class LocalizableBundle: Bundle {
    
    override func localizedString(forKey key: String, value: String?, table tableName: String?) -> String {
        if let bundle = objc_getAssociatedObject(self, &kBundleKey) as? Bundle {
            return bundle.localizedString(forKey: key, value: value, table: tableName)
        } else {
            return super.localizedString(forKey: key, value: value, table: tableName)
        }
    }
    
}
