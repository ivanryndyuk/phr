//
//  File.swift
//  phr.kz
//
//  Created by ivan on 09/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import CoreData

class File: NSManagedObject {

    //MARK: - VARS
    
    @NSManaged var id: String?
    @NSManaged var url: String?
    @NSManaged var title: String?
    @NSManaged var fileType: String?
    @NSManaged var fileSize: NSNumber?
    @NSManaged var width: NSNumber?
    @NSManaged var height: NSNumber?
    @NSManaged var videoProcess: String?
    @NSManaged var duration: NSNumber?
    @NSManaged var localUrl: String?
    
    
    
    //MARK: - LIFE
    
    func apply(json: JsonObject) {
        if let id = json["id"] as? String {
            self.id = id
        }
        
        if let url = json["url"] as? String {
            self.url = url
        }
        
        if let title = json["title"] as? String {
            self.title = title
        }
        
        if let fileType = json["fileType"] as? String {
            self.fileType = fileType
        }
        
        if let fileSize = json["fileSize"] as? NSNumber {
            self.fileSize = fileSize
        }
        
        if let width = json["imageWidth"] as? NSNumber {
            self.width = width
        }
        
        if let height = json["imageHeight"] as? NSNumber {
            self.height = height
        }
        
        if let videoProcess = json["videoProcess"] as? String {
            self.videoProcess = videoProcess
        }
        
        if let duration = json["duration"] as? NSNumber {
            self.duration = duration
        }
    }
    
}
