//
//  ApiDoctors.swift
//  phr.kz
//
//  Created by ivan on 16/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ApiDoctors: ApiSection {

    //MARK: - FUNCS
    
    func doctors(limit: Int = 100, page: Int = 0, sort: String = "entity.lastName", direction: String = "desc", name: String? = nil, specialities: [Speciality], sicknesses: [Sickness], languages: [Language], callback: @escaping Block<(Meta, [Doctor])>, fallback: @escaping Block<Error>) {
        var params = [String: Any]()// = ["limit": limit, "page": page, "sort": sort, "direction": direction]
        
        if let name = name {
            params["name"] = name
        }
        
        for speciality in specialities {
            params["specialties[]"] = speciality.id
        }
        
        for sickness in sicknesses {
            params["diseases[]"] = sickness.id
        }
        
        for language in languages {
            params["languages[]"] = language.id
        }
        
        let request = parent.request(withMethod: .get, path: "doctors", params: params, token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                guard let metaJson = data["meta"] as? JsonObject else {
                    fallback(Api.Errors.wrongJsonFormat)
                    return
                }
                
                guard let itemsJson = data["items"] as? JsonArray else {
                    fallback(Api.Errors.wrongJsonFormat)
                    return
                }
                
                guard let meta = Meta(json: metaJson) else {
                    fallback(Api.Errors.wrongJsonFormat)
                    return
                }
                
                let items = Doctor.list(fromJson: itemsJson)
                callback(meta, items)
            } else {
                fallback(Api.Errors.wrongJsonFormat)
            }
        }, fallback: fallback)
    }
    
}
