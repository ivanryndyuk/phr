//
//  VCContract.swift
//  phr.kz
//
//  Created by ivan on 31/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCContract: ModalController {
    
    //MARK: - VARS
    
    @IBOutlet weak var circleProgress: CircleProgress!
    @IBOutlet weak var periodLabel: UILabel!
    @IBOutlet weak var periodHeight: NSLayoutConstraint!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var priceHeight: NSLayoutConstraint!
    @IBOutlet weak var clinicLabel: UILabel!
    @IBOutlet weak var clinicHeight: NSLayoutConstraint!
    @IBOutlet weak var sicknessLabel: UILabel!
    @IBOutlet weak var sicknessHeight: NSLayoutConstraint!
    @IBOutlet weak var daysLeftTitle: UILabel!
    @IBOutlet weak var daysLeftSubtitle: UILabel!
    
    var contract: Contract?
    var progress: CGFloat = 0.0
    
    

    //MARK: - LIFE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let contract = contract else  {
            return
        }
        
        if let price = contract.price?.intValue {
            priceLabel.text = "\(price)₽"
        }
        
        setupPeriod()
        setupOrganization()
        setupSicknesses()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        circleProgress.set(progress: progress, animated: true)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if let constant = periodLabel.text?.height(forWidth: periodLabel.frame.width, attributes: [NSFontAttributeName: periodLabel.font]) {
            periodHeight.constant = constant
        }
    }
    
    
    
    //MARK: - LIFE
    
    @IBAction func handlerOf(backItem: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func setupPeriod() {
        let df = DateFormatter()
        df.dateFormat = "dd.MM.yyyy"
        
        guard let startDate = contract?.startDate else {
            return
        }
        
        guard let endDate = contract?.endDate else {
            return
        }
        
        periodLabel.text = "c \(df.string(from: startDate)) по \(df.string(from: endDate))"
        
        let contractTime = endDate.timeIntervalSince1970 - startDate.timeIntervalSince1970
        let timeLeft = endDate.timeIntervalSince1970 - Date().timeIntervalSince1970
        
        progress = CGFloat(timeLeft / contractTime)
        
        let daysLeft = Int(floor(timeLeft / TimeInterval(86400.0)))
        daysLeftTitle.text = "\(daysLeft)"
        
        let digits = daysLeft.digits
        
        if digits.count > 0 {
            switch digits[digits.count - 1] {
            case 1:
                if digits.count > 2 {
                    if digits[digits.count - 2] == 1 {
                        daysLeftSubtitle.text = "дней".localized
                        break
                    }
                }
                daysLeftSubtitle.text = "день".localized
            case 2, 3, 4:
                if digits.count > 2 {
                    if digits[digits.count - 2] == 1 {
                        daysLeftSubtitle.text = "дней".localized
                        break
                    }
                }
                daysLeftSubtitle.text = "дня".localized
            default:
                daysLeftSubtitle.text = "дней".localized
            }
        }
    }
    
    
    func setupOrganization() {
        guard let organization = contract?.getOrganizationTitle() else {
            return
        }
        
        clinicLabel.text = organization
    }
    
    
    func setupSicknesses() {
        guard let sicknesses = contract?.getSicknessesString() else {
            return
        }
        
        sicknessLabel.text = sicknesses
    }

}
