//
//  FormCell.swift
//  phr.kz
//
//  Created by ivan on 22/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

protocol FormCellDelegate {
    func didChange(textField: UITextField, for cell: FormCell)
    func didEndEditing(textField: UITextField, for cell: FormCell)
}

class FormCell: UITableViewCell {

    //MARK: - VARS
    
    @IBOutlet weak var leftTitleLabel: UILabel!
    @IBOutlet weak var titleTextField: UITextField!
    
    var delegate: FormCellDelegate?
    
    var leftTitle: String? {
        get {
            return leftTitleLabel.text
        }
        
        set {
            leftTitleLabel.text = newValue
        }
    }
    
    var title: String? {
        get {
            return titleTextField.text
        }
        
        set {
            titleTextField.text = newValue
        }
    }
    
    
    //MARK: - LIFE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        titleTextField.delegate = self
        titleTextField.addTarget(self, action: #selector(handlerOfEditingChanged(textField:)), for: .editingChanged)
    }
    
    
    
    //MARK: - FUNCS
    
    func handlerOfEditingChanged(textField: UITextField) {
        delegate?.didChange(textField: textField, for: self)
    }

}



extension FormCell: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.didEndEditing(textField: textField, for: self)
    }
    
}
