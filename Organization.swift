//
//  Organization.swift
//  phr.kz
//
//  Created by ivan on 11/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import CoreData

class Organization: NSManagedObject {

    //MARK: - VARS
    
    @NSManaged var id: String?
    @NSManaged var name: String?
    @NSManaged var address: String?
    
}
