//
//  CheckListItemCell.swift
//  phr.kz
//
//  Created by ivan on 14/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

protocol CheckListItemCellDelegate {
    func editItem(forCell cell: CheckListItemCell)
    func deleteItem(forCell cell: CheckListItemCell)
}

class CheckListItemCell: UITableViewCell {

    //MARK: - VARS
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var content: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImage: UIImageView!
    
    var parent: UITableView?
    var delegate: CheckListItemCellDelegate?
    
    var title: String? {
        get {
            return titleLabel.attributedText?.string
        }
        
        set {
            setup(value: newValue, selected: isChecked)
        }
    }
    
    var isChecked: Bool = false {
        willSet {
            setup(value: title, selected: newValue)
        }
    }
    
    var canEdit: Bool = false {
        willSet {
            print("can edit \(newValue)")
            scrollView.isUserInteractionEnabled = canEdit
        }
    }
    
    
    
    //MARK: - LIFE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        scrollView.delegate = self
        let tap = UITapGestureRecognizer(target: self, action: #selector(handlerOf(tapGesture:)))
        tap.cancelsTouchesInView = false
        content.addGestureRecognizer(tap)
        app.nc.addObserver(self, selector: #selector(handlerOf(checkListTableDidScrollNote:)), name: app.notification(name: app.keys.checkListTableDidScrollNote), object: nil)
    }
    
    
    
    //MARK: - FUNCS
    
    func setup(value: String?, selected: Bool) {
        if selected {
            if let value = value {
                titleLabel.attributedText = NSAttributedString(string: value, attributes: [
                    NSForegroundColorAttributeName: UIColor(hex: 0xABB5D4),
                    NSStrikethroughStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue
                    ]
                )
            }
            checkImage.isHidden = false
        } else {
            if let value = value {
                titleLabel.attributedText = NSAttributedString(string: value, attributes: [NSForegroundColorAttributeName: UIColor(hex: 0xFFFFFF)])
            }
            checkImage.isHidden = true
        }
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        scrollView.setContentOffset(.zero, animated: false)
    }
    
    
    func handlerOf(checkListTableDidScrollNote note: Notification) {
        scrollView.setContentOffset(.zero, animated: true)
    }
    
    
    func handlerOf(tapGesture: UITapGestureRecognizer) {
        if scrollView.contentOffset.x > 0 {
            scrollView.setContentOffset(.zero, animated: true)
        } else if let indexPath = parent?.indexPath(for: self) {
            if let table = parent {
                table.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                table.delegate?.tableView?(table, didSelectRowAt: indexPath)
            }
        }
    }
    
    
    @IBAction func handlerOf(editButton: ImageButton) {
        delegate?.editItem(forCell: self)
        scrollView.setContentOffset(.zero, animated: true)
    }
    
    
    @IBAction func handlerOf(delete: ImageButton) {
        delegate?.deleteItem(forCell: self)
        scrollView.setContentOffset(.zero, animated: true)
    }
    
}



//MARK: - UIScrollViewDelegate

extension CheckListItemCell: UIScrollViewDelegate {
    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        if scrollView.contentOffset.x > 40.0 {
            targetContentOffset.pointee.x = 100.0
        }
        else {
            targetContentOffset.pointee = .zero
            
            OperationQueue().addOperation {
                OperationQueue.main.addOperation {
                    self.scrollView.setContentOffset(.zero, animated: true)
                }
            }
        }
    }
    
}
