//
//  VCConfirm.swift
//  phr.kz
//
//  Created by Ivan Ryndyuk on 14/09/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

protocol ConfirmDelegate {
    func didConfirm(result: Bool)
}

class VCConfirm: UIViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var codeField: UITextField!
    @IBOutlet weak var digit0: UILabel!
    @IBOutlet weak var digit1: UILabel!
    @IBOutlet weak var digit2: UILabel!
    @IBOutlet weak var digit3: UILabel!
    @IBOutlet weak var digit4: UILabel!
    @IBOutlet weak var digit5: UILabel!
    @IBOutlet weak var bottomOffset: NSLayoutConstraint!
    @IBOutlet weak var loader: Loader!
    @IBOutlet weak var errorLabel: UILabel!
    
    var digits = [UILabel]()
    var message: String?
    var delegate: ConfirmDelegate?
    
    
    
    //MARK: - LIFE
    
    init(message: String?) {
        self.message = message
        super.init(nibName: "VCConfirm", bundle: nil)
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        titleLabel.text = message
        digits = [digit0, digit1, digit2, digit3, digit4, digit5]
        codeField.delegate = self
        codeField.addTarget(self, action: #selector(didChange(textField:)), for: .editingChanged)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillShow:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(keyboardWillHide:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        codeField.becomeFirstResponder()
    }
    
    
    
    //MARK: - FUNCS
    
    func didChange(textField: UITextField) {
        if !errorLabel.isHidden {
            errorLabel.isHidden = true
        }
        
        if let charsView = textField.text?.characters {
            let chars: [Character] = Array(charsView)
            for (i, digit) in digits.enumerated() {
                if i < chars.count {
                    digit.text = String(chars[i])
                } else {
                    digit.text = nil
                }
            }
            
            if chars.count == 6 {
                codeField.endEditing(false)
                confirm(code: String(charsView))
            }
        }
    }
    
    
    func confirm(code: String) {
        guard let id = app.user?.id else {
            app.alert(title: "Ошибка".localized, message: "Невозможно подтвердить операцию для пользователя без ID.".localized)
            return
        }
        
        set(loading: true)
        
        app.api.user.confirm(code: code, id: id, action: .change, callback: { (result) in
            self.set(loading: false)
            self.delegate?.didConfirm(result: true)
            self.dismiss(animated: true, completion: nil)
        }) { (error) in
            self.set(loading: false)
            self.codeField.text = nil
            self.didChange(textField: self.codeField)
            self.codeField.becomeFirstResponder()
            self.show(error: error)
        }
    }
    
    
    func show(error: Error) {
        errorLabel.text = error.localizedDescription
        errorLabel.isHidden = false
    }
    
    
    @IBAction func handlerOf(backButton: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    func handlerOf(keyboardWillShow note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        var keyboardBounds = CGRect.zero
        (userInfo[UIKeyboardFrameEndUserInfoKey] as AnyObject).getValue(&keyboardBounds)
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = keyboardBounds.height
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func handlerOf(keyboardWillHide note: NSNotification) {
        guard let userInfo = note.userInfo else {
            return
        }
        
        let duration = userInfo[UIKeyboardAnimationDurationUserInfoKey] as AnyObject
        let curve = userInfo[UIKeyboardAnimationCurveUserInfoKey] as AnyObject
        
        UIView.animate(withDuration: duration.doubleValue, delay: 0, options: UIViewAnimationOptions(rawValue: curve.uintValue), animations: { () -> Void in
            self.bottomOffset.constant = 0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    
    func set(loading: Bool) {
        if loading {
            loader.startAnimating()
            loader.isHidden = false
        } else {
            loader.isHidden = true
            loader.stopAnimating()
        }
    }

}



extension VCConfirm: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == "" && range.length == 1 {
            return true
        } else {
            if let text = textField.text {
                if text.characters.count == 6 {
                    return false
                }
            }
            
            let pattern = NSPredicate(format: "SELF MATCHES %@", "[0-9]{1}")
            return pattern.evaluate(with: string)
        }
    }
    
}

