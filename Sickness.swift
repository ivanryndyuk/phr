//
//  Sickness.swift
//  phr.kz
//
//  Created by ivan on 07/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import CoreData

class Sickness: NSManagedObject {
    
    //MARK: - VARS

    @NSManaged var id: String?
    @NSManaged var mkbCode: String?
    @NSManaged var name: String?
    
    
    
    //MARK: - FUNCS
    
    func apply(json: JsonObject) {
        if let id = json["id"] as? String {
            self.id = id
        }
        name = json["name"] as? String
        mkbCode = json["mkbCode"] as? String
    }
    
}
