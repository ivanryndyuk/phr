//
//  TextFieldCell.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

protocol TextFieldDelegate {
    func didChangeTextField(cell: TextFieldCell)
}

class TextFieldCell: UITableViewCell {
    
    //MARK: - VARS
    
    @IBOutlet weak var textField: UITextField!
    
    var delegate: TextFieldDelegate?
    
    var title: String? {
        get {
            return textField.text
        }
        
        set {
            textField.text = newValue
        }
    }
    
    var placeholder: String? {
        get {
            return textField.attributedPlaceholder?.string
        }
        
        set {
            if let newValue = newValue {
                textField.attributedPlaceholder = NSAttributedString(string: newValue, attributes: [NSForegroundColorAttributeName: UIColor(hex: 0x757475)])
            }
        }
    }
    
    
    
    //MARK: - FUNCS
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textField.addTarget(self, action: #selector(didChange(textField:)), for: .editingChanged)
    }
    
    
    func didChange(textField: UITextField) {
        delegate?.didChangeTextField(cell: self)
    }

}
