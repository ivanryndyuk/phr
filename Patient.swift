//
//  Patient.swift
//  phr.kz
//
//  Created by ivan on 16/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class Patient: NSObject, NSCoding {

    //MARK: - VARS
    
    //var id: String
    var firstName: String?
    var secondName: String?
    var patronymicName: String?
    
    
    
    
    //MARK: - INIT
    
    init?(json: JsonObject) {
        self.firstName = json["firstName"] as? String
        self.secondName = json["lastName"] as? String
        self.patronymicName = json["middleName"] as? String
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        guard let firstName = aDecoder.decodeObject(forKey: "firstName") as? String? else {
            return nil
        }
        
        guard let secondName = aDecoder.decodeObject(forKey: "secondName") as? String? else {
            return nil
        }
        
        guard let patronymicName = aDecoder.decodeObject(forKey: "patronymicName") as? String? else {
            return nil
        }
        
        self.firstName = firstName
        self.secondName = secondName
        self.patronymicName = patronymicName
    }
    
    
    
    //MARK: - FUNCS
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(firstName, forKey: "firstName")
        aCoder.encode(secondName, forKey: "secondName")
        aCoder.encode(patronymicName, forKey: "patronymicName")
    }
    
    
    func getName() -> String {
        var result = ""
        
        if let secondName = secondName {
            result += secondName
        }
        
        if let firstName = firstName {
            if result.isEmpty {
                result += firstName
            } else {
                result += " \(firstName)"
            }
        }
        
        return result
    }
    
}
