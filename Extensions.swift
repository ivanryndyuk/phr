//
//  Extensions.swift
//  phr.kz
//
//  Created by ivan on 16/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import Security

//MARK: - UIColor

extension UIColor {
    
    convenience init(r: Int, g: Int, b: Int, a: CGFloat = 1.0) {
        self.init(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: a)
    }
    
    convenience init(hex: Int) {
        self.init(r: (hex >> 16) & 0xff, g: (hex >> 8) & 0xff, b: hex & 0xff)
    }
    
}



//MARK: - NSLayoutFormatOptions

extension NSLayoutFormatOptions {
    
    static var zero: NSLayoutFormatOptions {
        return .init(rawValue: 0)
    }
    
}



//MARK: - Bundle

extension Bundle {
    
    class func set(locale: Locale) {
        if !Bundle.main.isKind(of: LocalizableBundle.self) {
            object_setClass(Bundle.main, LocalizableBundle.self)
        }
        
        var value: Bundle?
        
        if let language = locale.languageCode {
            if let path = Bundle.main.path(forResource: language, ofType: "lproj") {
                value = Bundle(path: path)
            }
        }
        
        objc_setAssociatedObject(Bundle.main, &kBundleKey, value, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    }
    
}



//MARK: - UIView

extension UIView {
    
    @IBInspectable var radius: CGFloat {
        get {
            return layer.cornerRadius
        }
        
        set {
            layer.cornerRadius = newValue
        }
    }
    
    /*@IBInspectable var borderColor: UIColor? {
        get {
            if let cgcolor = layer.borderColor {
                return UIColor(cgColor: cgcolor)
            } else {
                return nil
            }
        }
        
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        
        set {
            layer.borderWidth = newValue
        }
    }*/
    
}



//MARK: - UIImage

extension UIImage {
    
    convenience init?(contentsOf url: URL) {
        do {
            let data = try Data(contentsOf: url)
            self.init(data: data)
        } catch let error {
            log(error: error)
            return nil
        }
    }
    
    func copy(with color: UIColor) -> UIImage? {
        let rect = CGRect(x: 0.0, y: 0.0, width: self.size.width, height: self.size.height)
        UIGraphicsBeginImageContextWithOptions(self.size, false, 0.0)
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.color)
        
        color.setFill()
        
        context?.fill(rect)
        context?.setBlendMode(CGBlendMode.destinationIn)
        
        if let cgImage = cgImage {
            context?.draw(cgImage, in: rect)
        }
        
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return result
    }
    
}



//MARK: - NSMutableData

extension NSMutableData {
    
    func append(string: String) {
        if let data = string.data(using: .utf8, allowLossyConversion: false) {
            append(data)
        }
    }
    
}



//MARK: - Int

extension Int {
    
    var digits: [Int] {
        return description.characters.flatMap{Int(String($0))}
    }
    
}



//MARK: - String

extension String {
    
    var length: Int {
        return characters.count
    }
    
    func sha256() -> String? {
        if let stringData = data(using: .utf8) {
            return hexStringFromData(input: digest(input: stringData as NSData))
        }
        return nil
    }
    
    private func digest(input : NSData) -> NSData {
        let digestLength = Int(CC_SHA256_DIGEST_LENGTH)
        var hash = [UInt8](repeating: 0, count: digestLength)
        CC_SHA256(input.bytes, UInt32(input.length), &hash)
        return NSData(bytes: hash, length: digestLength)
    }
    
    private  func hexStringFromData(input: NSData) -> String {
        var bytes = [UInt8](repeating: 0, count: input.length)
        input.getBytes(&bytes, length: input.length)
        
        var hexString = ""
        for byte in bytes {
            hexString += String(format:"%02x", UInt8(byte))
        }
        
        return hexString
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: self)
    }
    
    func height(forWidth width: CGFloat, options: NSStringDrawingOptions = .usesLineFragmentOrigin, attributes: [String: Any]? = nil, context: NSStringDrawingContext? = nil) -> CGFloat {
        return ceil((self as NSString).boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: options, attributes: attributes, context: context).height)
    }
    
    func widthForHeight(height: CGFloat, options: NSStringDrawingOptions = .usesLineFragmentOrigin, attributes: [String: Any]? = nil, context: NSStringDrawingContext? = nil) -> CGFloat {
        return ceil((self as NSString).boundingRect(with: CGSize(width: CGFloat.greatestFiniteMagnitude, height: height), options: options, attributes: attributes, context: context).width)
    }
    
}



//MARK: - NSAttributedString

extension NSAttributedString {
    
    func heightForWidth(width: CGFloat, options: NSStringDrawingOptions = .usesFontLeading, context: NSStringDrawingContext? = nil) -> CGFloat {
        return ceil(boundingRect(with: CGSize(width: width, height: CGFloat.greatestFiniteMagnitude), options: options, context: context).height)
    }
    
}



//MARK: - UILabel

extension UILabel {
    
    class func create(message: String? = nil, font: UIFont = UIFont.systemFont(ofSize: 14.0), color: UIColor = UIColor.black, frame: CGRect = CGRect.zero) -> UILabel {
        let result = UILabel(frame: frame)
        result.text = message
        result.font = font
        result.textColor = color
        return result
    }
    
}



//MARK: - UITextView

extension UITextView {
    
    func getMaxLineWidth() -> CGFloat {
        var width: CGFloat = 0.0
        layoutManager.enumerateLineFragments(forGlyphRange: NSMakeRange(0, text.characters.count), using: { (rect, usedRect, textContainer, glyphRange, stop) in
            width = max(width, ceil(usedRect.width))
        })
        
        return width
    }
    
    
    func selfFitsSize() -> CGSize {
        return sizeThatFits(frame.size)
    }
    
}



//MARK: - UIFont

extension UIFont {
    
    class func with(name: String, size: CGFloat) -> UIFont {
        var result: UIFont
        
        if let font = UIFont(name: name, size: size) {
            result = font
        } else {
            result = UIFont.systemFont(ofSize: size)
        }
        
        return result
    }
    
}



//MARK: - NSError 

extension NSError {
    
    class func with(description: String, code: Int = 0) -> NSError {
        return NSError(domain: "phr.kz", code: code, userInfo: [NSLocalizedDescriptionKey: description])
    }
    
}



//MARK: - Error

extension Error {
    
    static func with(description: String, code: Int = 0) -> Error {
        return NSError(domain: "phr.kz", code: code, userInfo: [NSLocalizedDescriptionKey: description]) as Error
    }
    
}



//MARK: - UIViewController

extension UIViewController {
    
    /* Show alert with params */
    
    func alert(title: String? = nil, message: String? = nil) {
        let controller = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK".localized, style: .cancel) { (action) in
            controller.dismiss(animated: true, completion: nil)
        }
        
        controller.addAction(action)
        controller.view.tintColor = UIColor(hex: 0x495787)
        present(controller, animated: true, completion: nil)
    }
    
    
    /* Show alert with error */
    
    func alert(error: Error) {
        let controller = UIAlertController(title: "Ошибка".localized, message: error.localizedDescription, preferredStyle: .alert)
        
        let action = UIAlertAction(title: "OK".localized, style: .cancel) { (action) in
            controller.dismiss(animated: true, completion: nil)
        }
        
        controller.addAction(action)
        controller.view.tintColor = UIColor(hex: 0x495787)
        present(controller, animated: true, completion: nil)
    }
    
}
