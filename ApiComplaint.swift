//
//  ApiComplaint.swift
//  phr.kz
//
//  Created by ivan on 26/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ApiComplaint: ApiSection {

    //MARK: - FUNCS
    
    func post(complaint: String, id: String, type: String, callback: @escaping Block<Void>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .post, path: "complaints", params: ["text": complaint, "treaty_id": id, "treaty_type": type], token: parent.token)
        parent.pass(request: request, callback: { (json) in
            callback()
        }, fallback: fallback)
    }
    
}
