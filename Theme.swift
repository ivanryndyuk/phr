//
//  Theme.swift
//  phr.kz
//
//  Created by ivan on 16/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class Theme: NSObject {

    //MARK: - VARS
    
    let fontMuseoSansCyrl = "MuseoSansCyrl-500"
    let fontLatoHeavy = "Lato-Heavy"
    let fontLatoMedium = "Lato-Medium"
    let fontLatoRegular = "Lato-Regular"
    let fontLatoSemibold = "Lato-Semibold"
    let fontLatoBold = "Lato-Bold"
    let fontLucidaGrande = "LucidaGrande"
    let fontRalewayExtraBold = "Raleway-ExtraBold"
    let fontTahoma = "Tahoma"
    
    
    
    lazy var skyBlue: UIColor = {
        return UIColor(hex: 0x329DD2)
    }()
    
    
    lazy var lightGreyControl: UIColor = {
        return UIColor(hex: 0xD8D8D8)
    }()
    
}
