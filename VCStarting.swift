//
//  VCStarting.swift
//  phr.kz
//
//  Created by ivan on 22/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCStarting: UIViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var loader: Loader!
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    
    
    //MARK: - LIFE

    override func viewDidLoad() {
        super.viewDidLoad()
        app.delegate.background?.hidePattern()
        loader.startAnimating()
        app.user = app.stack.user()
        //app.twilio.startingDelegate = self
        
        app.api.cabinet.selfuser(callback: { (json) in
            app.user?.apply(params: json)
            self.loadContracts()
            //self.connectTwilio()
        }) { (error) in
            log(error: error)
            self.loadContracts()
            //self.connectTwilio()
        }
    }
    
    
    
    //MARK: - FUNCS
    
    /* Setup connection to Twilio. */
    
    private func connectTwilio() {
        app.twilio.connect(completion: { (success, error) in
            if success {
                log(message: "Connected to Twilio successfuly.")
            } else {
                if let error = error {
                    log(error: error)
                }
            }
        })
    }
    
    
    /* Load contracts list. */
    
    func loadContracts() {
        app.api.contract.contracts(callback: { (meta, contracts) in
            self.loadSO()
            //self.loadChannelsRecursively(contracts: contracts)
        }) { (error) in
            app.alert(error: error)
            self.loadSO()
        }
    }
    
    
    func loadSO() {
        app.api.so.listSO(callback: { (meta, listSO) in
            self.presentTabs()
        }) { (error) in
            app.alert(error: error)
            self.presentTabs()
        }
    }
    
    
    /* Show main tabs view controller. */
    
    private func presentTabs() {
        guard let root = app.delegate.window?.rootViewController else {
            return
        }
        
        let tabs = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "tabs")
        
        if app.user?.type == .some(.doctor) {
            if let t = tabs as? UITabBarController {
                t.tabBar.items?.remove(at: 1)
            }
        }
        
        app.delegate.window?.addSubview(tabs.view)
        tabs.view.transform = CGAffineTransform(scaleX: 2.5, y: 2.5)
        tabs.view.alpha = 0.0
        
        UIView.animate(withDuration: 0.5, animations: { 
            tabs.view.transform = CGAffineTransform.identity
            tabs.view.alpha = 1.0
            root.view.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
            root.view.alpha = 0.0
        }) { (finished) in
            if finished {
                app.delegate.window?.rootViewController = tabs
            }
        }
    }

}



//MARK: - VCStartingDelegate

extension VCStarting: TwilioDelegate {
    
    func clientSyncCompleted() {
        loadContracts()
    }
    
}
