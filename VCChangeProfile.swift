//
//  VCChangeProfile.swift
//  phr.kz
//
//  Created by Ivan Ryndyuk on 13/09/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCChangeProfile: UIViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var changesTable: UITableView!
    
    var type: ChangeProfileType
    let textFieldCellId = "textFieldCell"
    var params = [String: Any]()
    let validator = Validator()
    
    
    
    //MARK: - LIFE
    
    init(type: ChangeProfileType) {
        self.type = type
        super.init(nibName: "VCChangeProfile", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        return nil
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupBarItems()
        
        changesTable.register(UINib(nibName: "TextFieldCell", bundle: nil), forCellReuseIdentifier: textFieldCellId)
        changesTable.dataSource = self
        changesTable.delegate = self
    }
    
    
    
    //MARK: - FUNCS
    
    func setupBarItems() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Отмена".localized, style: .plain, target: self, action: #selector(cancel(barButtonItem:)))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохранить", style: .done, target: self, action: #selector(save(barButtonItem:)))
    }
    
    
    func startLoading() {
        let loader = UIActivityIndicatorView(activityIndicatorStyle: .white)
        loader.startAnimating()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: loader)
    }
    
    
    func cancel(barButtonItem: UIBarButtonItem) {
        navigationController?.popViewController(animated: true)
    }
    
    
    func save(barButtonItem: UIBarButtonItem) {
        switch type {
        case .email:
            if let newEmail = params["user[email]"] as? String {
                if newEmail == "" || newEmail == app.user?.email {
                    app.alert(title: "Ошибка".localized, message: "Укажите новый адрес эл. почты.".localized)
                    return
                } else if !validator.validate(email: newEmail) {
                    app.alert(title: "Ошибка".localized, message: "Укажите корректный адрес эл. почты.".localized)
                    return
                }
            } else {
                app.alert(title: "Ошибка".localized, message: "Укажите новый адрес эл. почты.".localized)
                return
            }
        case .phone:
            if let newPhone = params["user[phone]"] as? String {
                if newPhone == "" || newPhone == app.user?.phone {
                    app.alert(title: "Ошибка".localized, message: "Укажите новый номер телефона.".localized)
                    return
                } else if !validator.validate(phone: newPhone) {
                    app.alert(title: "Ошибка".localized, message: "Укажите корректный номер телефона.".localized)
                    return
                }
            } else {
                app.alert(title: "Ошибка".localized, message: "Укажите новый номер телефона.".localized)
                return
            }
        }
        
        startLoading()
        
        if app.user?.type == .some(.doctor) {
            app.api.user.updateDoctor(params: params, needsConfirm: true, callback: { (message) in
                self.setupBarItems()
                self.presentConfirm(withType: self.type, message: message)
            }, fallback: { (error) in
                self.setupBarItems()
                app.alert(error: error)
            })
        } else {
            app.api.user.updatePatient(params: params, needsConfirm: true, callback: { (message) in
                self.setupBarItems()
                self.presentConfirm(withType: self.type, message: message)
            }, fallback: { (error) in
                self.setupBarItems()
                app.alert(error: error)
            })
        }
    }
    
    
    func presentConfirm(withType type: ChangeProfileType, message: String?) {
        let controller = VCConfirm(message: message)
        controller.delegate = self
        self.present(controller, animated: true, completion: nil)
    }
    
}



//MARK: - UITableViewDataSource

extension VCChangeProfile: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        var number = 0
        
        switch type {
        case .email, .phone:
            number = 1
        }
        
        return number
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var number = 0
        
        switch type {
        case .email, .phone:
            number = 1
        }
        
        return number
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch type {
        case .email, .phone:
            return tableView.dequeueReusableCell(withIdentifier: textFieldCellId, for: indexPath)
        }
    }
    
}



//MARK: - UITableViewDelegate

extension VCChangeProfile: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var result: String?
        
        switch type {
        case .email:
            result = "Новый адрес эл. почты".localized
        case .phone:
            result = "Новый номер телефона".localized
        }
        
        return result
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch type {
        case .email:
            let cell = cell as? TextFieldCell
            cell?.delegate = self
            cell?.placeholder = app.user?.email
            cell?.textField.keyboardType = .emailAddress
        case .phone:
            let cell = cell as? TextFieldCell
            cell?.delegate = self
            cell?.placeholder = app.user?.phone
            cell?.textField.keyboardType = .phonePad
        }
    }
    
}



//MARK: - TextFieldDelegate

extension VCChangeProfile: TextFieldDelegate {
    
    func didChangeTextField(cell: TextFieldCell) {
        switch type {
        case .email:
            if let newEmail = cell.title {
                if !newEmail.isEmpty {
                    params["user[email]"] = newEmail
                }
            }
        case .phone:
            if let newPhone = cell.title {
                if !newPhone.isEmpty {
                    params["user[phone]"] = newPhone
                }
            }
        }
    }
    
}



//MARK: - ConfirmDelegate

extension VCChangeProfile: ConfirmDelegate {
    
    func didConfirm(result: Bool) {
        if result {
            app.nc.post(name: app.notification(name: "reloadUserAfterChange"), object: nil)
            navigationController?.popViewController(animated: true)
        }
    }
    
}



//MARK: - ChangeProfileType

enum ChangeProfileType {
    case email, phone
}
