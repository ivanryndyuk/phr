//
//  Tranimator.swift
//  phr.kz
//
//  Created by ivan on 16/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

enum TranimatorStyle {
    case slideUp, slideDown, fadeAndSlideUp, modal
}

class Tranimator: NSObject {

    //MARK: - VARS
    
    var style: TranimatorStyle
    var presenting: Bool
    var hideBackgroundPattern = false
    
    
    
    //MARK: - INIT
    
    init(style: TranimatorStyle, dismissing: Bool = false) {
        self.style = style
        self.presenting = !dismissing
        super.init()
    }
}



//MARK: - UIViewControllerContextTransitioning

extension Tranimator: UIViewControllerAnimatedTransitioning {
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.35
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let container = transitionContext.containerView
        
        if style == .modal {
            var blurAlpha: CGFloat
            var transform: CGAffineTransform
            var animations = { }
            
            
            if presenting {
                blurAlpha = 1.0
                transform = CGAffineTransform.identity
                
                if let to = transitionContext.viewController(forKey: .to) as? ModalController {
                    to.blurView?.alpha = 0.0
                    to.contentView?.transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
                    container.addSubview(to.view)
                    animations = {
                        to.blurView?.alpha = blurAlpha
                        to.contentView?.transform = transform
                    }
                }
            } else {
                blurAlpha = 0.0
                transform = CGAffineTransform(scaleX: 0.01, y: 0.01)
                
                if let from = transitionContext.viewController(forKey: .from) as? ModalController {
                    animations = {
                        from.blurView?.alpha = blurAlpha
                        from.contentView?.transform = transform
                    }
                }
            }
            
            UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: animations, completion: { finished in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            })
        } else {
            let source = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!.view!
            let destination = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!.view!
            
            if style == .slideUp {
                let offset = source.frame.height
                destination.frame = source.frame
                //destination.alpha = 0.0
                
                if presenting {
                    destination.center.y += offset
                    container.addSubview(destination)
                } else {
                    destination.center.y -= offset
                    destination.isHidden = false
                }
                
                if hideBackgroundPattern {
                    if presenting {
                        app.delegate.background?.hidePattern()
                    } else {
                        app.delegate.background?.showPattern()
                    }
                }
                
                UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
                    //destination.alpha = 1.0
                    //source.alpha = 0.0
                    
                    if self.presenting {
                        source.center.y -= offset
                        destination.center.y -= offset
                    } else {
                        source.center.y += offset
                        destination.center.y += offset
                    }
                }, completion: { (finished) in
                    source.isHidden = true
                    transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                })
            } else if style == .fadeAndSlideUp {
                if presenting {
                    destination.alpha = 0.0
                    container.addSubview(destination)
                }
                
                UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
                    
                    
                    if self.presenting {
                        destination.alpha = 1.0
                    } else {
                        destination.alpha = 0.0
                    }
                }, completion: { (finished) in
                    transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                })
            }
        }
    }
    
}
