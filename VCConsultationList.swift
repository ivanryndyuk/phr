//
//  VCConsultationList.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import TwilioChatClient
import AudioToolbox

class VCConsultationList: ViewController {

    //MARK: - VARS
    
    @IBOutlet weak var currentButton: BadgeButton!
    @IBOutlet weak var finishedButton: BadgeButton!
    @IBOutlet weak var sliderLeading: NSLayoutConstraint!
    @IBOutlet weak var consultationsTable: UITableView!
    
    let reuseId = "consultationCell"
    let consultationNavigationId = "consultationNavigationId"
    
    var consultations = [Consultation]()
    var empty: EmptyView?
    var loading = LoadingView.instance(message: "Готовим консультации...".localized)
    
    var activeMode = true
    let df = DateFormatter()
    
    //let refresh = UIRefreshControl()
    //let refreshLoader = Loader(frame: .zero)
    
    
    //MARK: - LIFE
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationTitleView?.titleColor = UIColor.white
        navigationTitleView?.title = "Консультации".localized
        currentButton.isActive = true
        consultationsTable.dataSource = self
        consultationsTable.delegate = self
        consultationsTable.rowHeight = UITableViewAutomaticDimension
        consultationsTable.estimatedRowHeight = 96.0
        
        
        loading.backgroundColor = UIColor.white
        loading.messageLabel.textColor = UIColor(hex: 0x999999)
        
        
        /*refresh.addTarget(self, action: #selector(handlerOf(refreshControl:)), for: .valueChanged)
        consultationsTable.addSubview(refresh)
        refreshLoader.alpha = 0.0
        for view in refresh.subviews {
            view.removeFromSuperview()
        }
        refresh.addSubview(refreshLoader)
        refreshLoader.translatesAutoresizingMaskIntoConstraints = false
        refresh.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "V:[refreshLoader(==40)]-10-|",
            options: .zero,
            metrics: nil,
            views: ["refreshLoader": refreshLoader]))
        refresh.addConstraints(NSLayoutConstraint.constraints(
            withVisualFormat: "H:[refreshLoader(==40)]",
            options: .zero,
            metrics: nil,
            views: ["refreshLoader": refreshLoader]))
        refresh.addConstraints([
            NSLayoutConstraint(item: refreshLoader, attribute: .centerX, relatedBy: .equal, toItem: refresh, attribute: .centerX, multiplier: 1.0, constant: 0.0)/*,
            NSLayoutConstraint(item: refreshLoader, attribute: .centerY, relatedBy: .equal, toItem: refresh, attribute: .centerY, multiplier: 1.0, constant: 0.0)*/
        ])*/
        
        
        df.dateFormat = "HH:mm"
        
        app.nc.addObserver(self, selector: #selector(handleReloadContracts(note:)), name: app.notification(name: "reloadContracts"), object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(newMessageNote:)), name: app.notification(name: app.keys.newMessageNote), object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(channelUpdate:)), name: app.notification(name: app.keys.channelUpdatedNote), object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(chatMemberUpdate:)), name: app.notification(name: app.keys.chatMemberUpdate), object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(reachabilityNote:)), name: app.notification(name: app.keys.reachabilityChangeNote), object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(channelAdded:)), name: app.notification(name: app.keys.channelAddedNote), object: nil)
        app.nc.addObserver(self, selector: #selector(handlerOf(channelAdded:)), name: app.notification(name: app.keys.channelDeletedNote), object: nil)
        
        app.twilio.consultationsDelegate = self
        
        prepareConsultations()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = UIColor(hex: 0x495787)
        navigationController?.navigationBar.tintColor = UIColor(hex: 0xFFFFFF)
        (navigationController as? NavigationController)?.useLightStatusBar = true
        self.reloadConsultations()
        try? app.reachability?.startNotifier()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    func handleReloadContracts(note: NSNotification) {
        prepareConsultations()
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(currentButton: BadgeButton) {
        switchTo(active: true)
    }
    
    
    @IBAction func handlerOf(finishedButton: BadgeButton) {
        switchTo(active: false)
    }
    
    
    func prepareConsultations() {
        loadConsultations {
            self.getConsultations()
            
            if self.consultations.count == 0 {
                self.showEmpty(animated: true)
            } else {
                self.hideEmpty(animated: true)
                
                if app.reachability?.isReachable == .some(true) {
                    app.connectTwilio(callback: { () in
                        // wait for sync
                    }, fallback: { (error) in
                        self.show(error: error)
                    })
                } else {
                    self.consultationsTable.reloadSections(IndexSet(integer: 0), with: .fade)
                }
            }
        }
    }
    
    
    func showPreparing(animated: Bool = false) {
        loading.frame = view.frame
        
        if animated {
            loading.alpha = 0.0
            view.addSubview(loading)
            UIView.animate(withDuration: 0.2, animations: { 
                self.loading.alpha = 1.0
            }, completion: { (finished) in
                self.hideEmpty()
            })
        } else {
            loading.alpha = 1.0
            view.addSubview(loading)
            hideEmpty()
        }
    }
    
    
    func hidePreparing(animated: Bool = false) {
        if animated {
            UIView.animate(withDuration: 0.2, animations: { 
                self.loading.alpha = 0.0
            }, completion: { (finished) in
                if finished {
                    self.loading.removeFromSuperview()
                }
            })
        } else {
            loading.removeFromSuperview()
        }
    }
    
    
    func showEmpty(animated: Bool = false) {
        empty = EmptyView.instance(text: "Ожидаем заключения контракта.".localized, image: #imageLiteral(resourceName: "icon-chat"))
        
        if let empty = empty {
            empty.frame = view.frame
            empty.set(delegate: self, actionTitle: "Обновить")
            
            if animated {
                empty.alpha = 0.0
                view.addSubview(empty)
                UIView.animate(withDuration: 0.2, animations: { 
                    empty.alpha = 1.0
                })
                UIView.animate(withDuration: 0.2, animations: { 
                    empty.alpha = 1.0
                }, completion: nil)
            } else {
                view.addSubview(empty)
            }
        }
    }
    
    func hideEmpty(animated: Bool = false) {
        if animated {
            UIView.animate(withDuration: 0.2, animations: {
                self.empty?.alpha = 0.0
            }, completion: { (finished) in
                if finished {
                    self.empty?.removeFromSuperview()
                }
            })
        } else {
            empty?.removeFromSuperview()
        }
    }
    
    
    func show(error: Error?, animated: Bool = false) {
        show(errorMessage: error?.localizedDescription, animated: animated)
    }
    
    
    func show(errorMessage: String?, animated: Bool = false) {
        empty = EmptyView.instance(text: errorMessage, image: nil)
        
        if let empty = empty {
            empty.frame = view.bounds
            empty.set(delegate: self, actionTitle: "Обновить")
            
            if animated {
                empty.alpha = 0.0
                view.addSubview(empty)
                UIView.animate(withDuration: 0.2, animations: {
                    empty.alpha = 1.0
                })
                UIView.animate(withDuration: 0.2, animations: {
                    empty.alpha = 1.0
                }, completion: nil)
            } else {
                view.addSubview(empty)
            }
        }
    }
    
    
    func getConsultations() {
        let result = app.stack.items(forEntity: Consultation.self, predicateFormat: "isFinished == \(!activeMode)")
        
        consultations = result.sorted(by: { (one, two) -> Bool in
            var result  = false
            
            if let oneDate = one.lastMessage?.timestampAsDate {
                if let twoDate = two.lastMessage?.timestampAsDate {
                    result = oneDate.compare(twoDate) == .orderedDescending
                }
            }
            
            return result
        })
        
        consultations = result.sorted(by: { $0.unconsumedMessagesCount > $1.unconsumedMessagesCount })
    }
    
    
    func loadConsultations(completion: @escaping Block<Void>) {
        loadContracts {
            app.stack.save()
            self.loadSO(completion: { (Void) in
                app.stack.save()
                completion()
            })
        }
    }
    
    
    func loadContracts(completion: @escaping Block<Void>) {
        app.api.contract.contracts(callback: { (meta, contracts) in
            print("contracts count: \(contracts.count)")
            completion()
        }) { (error) in
            log(error: error)
            completion()
        }
    }
    
    
    func loadSO(completion: @escaping Block<Void>) {
        app.api.so.listSO(callback: { (meta, list) in
            print("so count: \(list.count)")
            completion()
        }) { (error) in
            log(error: error)
            completion()
        }
    }
    
    
    func update(withContracts: Bool = false) {
        //navigationTitleView?.startLoading(message: "Обновление...".localized)
        if withContracts {
            app.api.contract.contracts(callback: { (meta, contracts) in
                //self.contracts = app.stack.items(forEntity: Contract.self)
                app.api.so.listSO(callback: { (meta, listSO) in
                    self.getConsultations()
                    self.loadChannelsRecursively()
                }) { (error) in
                    app.alert(error: error)
                    self.getConsultations()
                    self.loadChannelsRecursively()
                }
            }) { (error) in
                app.alert(error: error)
                app.api.so.listSO(callback: { (meta, listSO) in
                    self.getConsultations()
                    self.loadChannelsRecursively()
                }) { (error) in
                    app.alert(error: error)
                    self.getConsultations()
                    self.loadChannelsRecursively()
                }
            }
        } else {
            self.getConsultations()
            loadChannelsRecursively()
        }
    }
    
    
    /* Get channels for contracts recursively. */
    
    private func loadChannelsRecursively(loop: Int = 0) {
        if loop == consultations.count {
            self.reloadConsultations()
        } else {
            if loop < consultations.count {
                let consultation = consultations[loop]
                
                if let sid = consultation.channel?.sid {
                    app.twilio.channel(withSID: sid, callback: { (channel) in
                        consultation.chat = channel
                        channel.join(completion: { (result) in
                            if let _ = result?.isSuccessful() {
                                app.twilio.lastMessage(forChannel: channel, completion: { (sucess, message) in
                                    if let message = message {
                                        consultation.lastMessage = message
                                    }
                                    
                                    channel.getUnconsumedMessagesCount(completion: { (result, count) in
                                        consultation.unconsumedMessagesCount = count
                                        self.loadChannelsRecursively(loop: loop + 1)
                                    })
                                })
                            } else {
                                self.loadChannelsRecursively(loop: loop + 1)
                            }
                        })
                    }, fallback: { (error) in
                        log(error: error)
                        self.loadChannelsRecursively(loop: loop + 1)
                    })
                } else {
                    self.loadChannelsRecursively(loop: loop + 1)
                }
            }/* else {
                let opinion = so[loop - contracts.count]
                
                if let sid = opinion.channel?.sid {
                    app.twilio.channel(withSID: sid, callback: { (channel) in
                        opinion.chat = channel
                        channel.join(completion: { (result) in
                            if let _ = result?.isSuccessful() {
                                app.twilio.lastMessage(forChannel: channel, completion: { (sucess, message) in
                                    if let message = message {
                                        opinion.lastMessage = message
                                    }
                                    
                                    channel.getUnconsumedMessagesCount(completion: { (result, count) in
                                        opinion.unconsumedMessagesCount = count
                                        self.loadChannelsRecursively(loop: loop + 1)
                                    })
                                })
                            } else {
                                self.loadChannelsRecursively(loop: loop + 1)
                            }
                        })
                    }, fallback: { (error) in
                        log(error: error)
                        self.loadChannelsRecursively(loop: loop + 1)
                    })
                } else {
                    self.loadChannelsRecursively(loop: loop + 1)
                }
            }*/
        }
        
    }
    
    
    func update(consultation: Consultation, with channel: TCHChannel, lastMessage: TCHMessage? = nil) {
        if let message = lastMessage {
            if channel.messages.lastConsumedMessageIndex == nil {
                consultation.unconsumedMessagesCount = 0
                consultation.lastMessage = message
                if app.user?.chatIdentity != .some(message.author) {
                    AudioServicesPlayAlertSound(1007)
                }
                self.reloadConsultations()
            } else if channel.messages.lastConsumedMessageIndex == message.index {
                consultation.unconsumedMessagesCount = 0
                consultation.lastMessage = message
                if app.user?.chatIdentity != .some(message.author) {
                    AudioServicesPlayAlertSound(1007)
                }
                self.reloadConsultations()
            } else {
                channel.getUnconsumedMessagesCount(completion: { (result, count) in
                    consultation.unconsumedMessagesCount = count
                    consultation.lastMessage = message
                    if app.user?.chatIdentity != .some(message.author) {
                        AudioServicesPlayAlertSound(1007)
                    }
                    self.reloadConsultations()
                })
            }
        } else {
            app.twilio.lastMessage(forChannel: channel, completion: { (sucess, message) in
                if let message = message {
                    consultation.lastMessage = message
                }
                
                channel.getUnconsumedMessagesCount(completion: { (result, count) in
                    consultation.unconsumedMessagesCount = count
                    self.reloadConsultations()
                })
            })
        }
    }
    
    
    func getLastMessages(for channels: [TCHChannel], completion: @escaping Block<Void>, loop: Int = 0) {
        if loop == channels.count {
            completion()
        } else {
            let channel = channels[loop]
            var consultation: Consultation? = nil
            
            for item in consultations {
                if item.channel?.sid == .some(channel.sid) {
                    consultation = item
                    break
                }
            }
            
            if let consultation = consultation {
                app.twilio.lastMessage(forChannel: channel, completion: { (sucess, message) in
                    if let message = message {
                        consultation.lastMessage = message
                    }
                    
                    channel.getUnconsumedMessagesCount(completion: { (result, count) in
                        consultation.unconsumedMessagesCount = count
                        self.getLastMessages(for: channels, completion: completion, loop: loop + 1)
                    })
                })
            } else {
                self.getLastMessages(for: channels, completion: completion, loop: loop + 1)
            }
        }
    }
    
    
    func reloadConsultations() {
        getConsultations()
        
        if consultations.count == 0 {
            showEmpty()
        } else {
            hideEmpty()
        }
        
        consultationsTable.reloadSections(IndexSet(integer: 0), with: .fade)
    }
    
    
    func showEmpty() {
        if empty == nil {
            empty = EmptyView.instance(text: "Ожидаем заключения контракта.".localized, image: #imageLiteral(resourceName: "icon-chat"))
            empty?.set(delegate: self, actionTitle: "Обновить")
        }
        
        guard let empty = empty else {
            return
        }
        
        if empty.superview == nil {
            empty.alpha = 0.0
            
            view.addSubview(empty)
            
            empty.translatesAutoresizingMaskIntoConstraints = false
            
            view.addConstraints(NSLayoutConstraint.constraints(
                withVisualFormat: "V:|-0-[empty]-0-|",
                options: .zero,
                metrics: nil,
                views: ["empty": empty]))
            
            view.addConstraints(NSLayoutConstraint.constraints(
                withVisualFormat: "H:|-0-[empty]-0-|",
                options: .zero,
                metrics: nil,
                views: ["empty": empty]))
        }
        
        UIView.animate(withDuration: 0.2) { 
            empty.alpha = 1.0
        }
    }
    
    func hideEmpty() {
        UIView.animate(withDuration: 0.2, animations: {
            self.empty?.alpha = 0.0
        }) { (finished) in
            if finished {
                self.empty?.removeFromSuperview()
            }
        }
    }
    
    
    func switchTo(active: Bool) {
        activeMode = active
        getConsultations()
        currentButton.isActive = active == true ? true : false
        finishedButton.isActive = active == true ? false : true
        UIView.animate(withDuration: 0.2) { 
            self.sliderLeading.constant = active == true ? -1.0 : UIScreen.main.bounds.width / 2 + 1.0
            self.view.layoutIfNeeded()
        }
        consultationsTable.reloadSections(IndexSet(integer: 0), with: .fade)
    }
    
    
    func showConsultation(with indexPath: IndexPath) {
        performSegue(withIdentifier: app.segues.showConsultation, sender: consultations[indexPath.row])
        /*if indexPath.row < contracts.count {
            performSegue(withIdentifier: app.segues.showConsultation, sender: contracts[indexPath.row])
        } else {
            performSegue(withIdentifier: app.segues.showConsultation, sender: so[indexPath.row - contracts.count])
        }*/
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == app.segues.showConsultation {
            let destination = segue.destination as? VCConsultation
            destination?.consultation = sender as? Consultation
        }
    }
    
    
    func willDisplay(cell: ConsultationCell?, indexPath: IndexPath, contract: Contract) {
        cell?.headerLabel.text = contract.getTitle()
        
        if contract.lastMessage?.body == .some("") {
            cell?.lastMessageLabel.text = "Файл.".localized
        } else {
            cell?.lastMessageLabel.text = contract.lastMessage == nil ? "В этом чате ещё нет сообщений.".localized : contract.lastMessage?.body
        }
        
        cell?.doctorTypeLabel.text = contract.getSubtitle()
        
        if let date = contract.lastMessage?.timestampAsDate {
            cell?.timeLabel.text = df.string(from: date)
        } else if let date = contract.chat?.dateUpdatedAsDate {
            cell?.timeLabel.text = df.string(from: date)
        }
        
        if contract.unconsumedMessagesCount > 0 {
            cell?.badge = "\(contract.unconsumedMessagesCount)"
        }
    }
    
    
    func willDisplay(cell: ConsultationCell?, indexPath: IndexPath, so: SO) {
        cell?.headerLabel.text = so.getTitle()
        
        if so.lastMessage?.body == .some("") {
            cell?.lastMessageLabel.text = "Файл.".localized
        } else {
            cell?.lastMessageLabel.text = so.lastMessage == nil ? "В этом чате ещё нет сообщений.".localized : so.lastMessage?.body
        }
        
        cell?.doctorTypeLabel.text = so.getSubtitle()
        
        if let date = so.lastMessage?.timestampAsDate {
            cell?.timeLabel.text = df.string(from: date)
        } else if let date = so.chat?.dateUpdatedAsDate {
            cell?.timeLabel.text = df.string(from: date)
        }
        
        cell?.secondOpinionView.isHidden = false
        
        if let status = so.status {
            switch status.code {
            case "request":
                cell?.progress = 0.33
            case "in_progress":
                cell?.progress = 0.66
            case "completed":
                cell?.progress = 1
            default:
                cell?.progress = 0.0
            }
        }
        
        if let checklistItems = so.checkListItems {
            if checklistItems.count > 0 {
                var completeCount = 0
                
                for item in checklistItems {
                    if item.complete {
                        completeCount += 1
                    }
                }
                
                cell?.secondOpinionStepsLabel.text = "\(completeCount)/\(checklistItems.count)"
            } else {
                cell?.secondOpinionStepsLabel.text = nil
            }
        }
        cell?.progress = 0.5
        
        if so.unconsumedMessagesCount > 0 {
            cell?.badge = "\(so.unconsumedMessagesCount)"
        }
    }
    
    
    //MARK: Notes
    
    func handlerOf(reachabilityNote: Notification) {
        if app.reachability?.isReachable == .some(true) {
            navigationTitleView?.startLoading(message: "Готовим консультации...".localized)
            prepareConsultations()
        } else {
            navigationTitleView?.startLoading(message: "Ожидаем подключения к Интернету".localized)
        }
    }
    
}



//MARK: - UITableViewDataSource

extension VCConsultationList: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return consultations.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return tableView.dequeueReusableCell(withIdentifier: reuseId, for: indexPath)
    }
    
}



//MARK: - UITableViewDelegate

extension VCConsultationList: UITableViewDelegate {
    
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height: CGFloat = 125.0
        
        if indexPath.row == 3 {
            height = 150.0
        }
        
        return height
    }*/
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.selectionStyle = .none
        let cell = cell as? ConsultationCell
        cell?.reset()
        
        let consultation = consultations[indexPath.row]
        
        if let contract = consultation as? Contract {
            willDisplay(cell: cell, indexPath: indexPath, contract: contract)
        } else if let so = consultation as? SO {
            willDisplay(cell: cell, indexPath: indexPath, so: so)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        if activeMode {
            if let chat = consultations[indexPath.row].chat {
                if chat.synchronizationStatus == .all {
                    showConsultation(with: indexPath)
                } else  {
                    app.alert(message: "Вы еще не подключены к сервису.".localized)
                }
            } else if let sid = consultations[indexPath.row].channel?.sid {
                if let list = app.twilio.channelsList {
                    app.showLoader()
                    list.channel(withSidOrUniqueName: sid, completion: { (result, channel) in
                        if let channel = channel {
                            app.hideLoader()
                            self.consultations[indexPath.row].chat = channel
                            self.showConsultation(with: indexPath)
                        } else if let error = result?.error {
                            app.hideLoader()
                            log(error: error)
                            app.alert(error: error)
                            app.connectTwilio(callback: {})
                        } else {
                            app.hideLoader()
                            app.alert(message: "Ошибка подключения к сервису.".localized)
                            app.connectTwilio(callback: {})
                        }
                    })
                }
            }
        }
    }

}



//MARK: - EmptyViewDelegate

extension VCConsultationList: EmptyViewDelegate {
    
    func handlerOf(actionButton: Button, emptyView: EmptyView) {
        app.showLoader(inView: view)
        loadConsultations {
            app.hideLoader()
            self.reloadConsultations()
        }
    }
    
}



//MARK: - Twilio updates

extension VCConsultationList {
    
    func handlerOf(channelUpdate message: Notification) {
        /*guard let channel = message.userInfo?["channel"] as? TCHChannel else {
            return
        }
        
        //app.hideLoader()
        
        for consultation in consultations {
            if consultation.channel?.sid == .some(channel.sid) {
                consultation.chat = channel
                update(consultation: consultation, with: channel)
            }
        }*/
    }
    
    
    func handlerOf(newMessageNote note: Notification) {
        guard let channel = note.userInfo?["channel"] as? TCHChannel else {
            return
        }
        
        guard let message = note.userInfo?["message"] as? TCHMessage else {
            return
        }
        
        for consultation in consultations {
            if consultation.channel?.sid == .some(channel.sid) {
                update(consultation: consultation, with: channel, lastMessage: message)
            }
        }
        
        /*for consultation in consultations {
            if consultation.channel?.sid == .some(channel.sid) {
                channel.getUnconsumedMessagesCount(completion: { (result, count) in
                    consultation.unconsumedMessagesCount = count
                    consultation.lastMessage = message
                    if app.user?.chatIdentity != .some(message.author) {
                        AudioServicesPlayAlertSound(1007)
                    }
                    self.reloadConsultations()
                })
            }
        }*/
    }
    
    
    func handlerOf(chatMemberUpdate note: Notification) {
        guard let channel = note.userInfo?["channel"] as? TCHChannel else {
            return
        }
        
        /*guard let member = note.userInfo?["member"] as? TCHMember else {
            return
        }*/
        
        for consultation in consultations {
            if consultation.channel?.sid == .some(channel.sid) {
                update(consultation: consultation, with: channel)
            }
        }
    }
    
    
    func handlerOf(channelAdded note: Notification) {
        self.prepareConsultations()
    }
    
}



//MARK: - ConsultationsDelegate

extension VCConsultationList: ConsultationsDelegate {
    
    func received(error: TCHError, for client: TwilioChatClient) {
        self.show(error: error)
    }
    
    
    func updated(connectionState state: TCHClientConnectionState, of client: TwilioChatClient) {
        switch state {
        case .connecting:
            navigationTitleView?.startLoading(message: "Подключение...".localized)
            print("STS connecting...")
        case .connected:
            print("STS connected")
        case .disconnected:
            print("STS disconnected")
        case .denied:
            print("STS denied")
        case .error:
            print("STS error")
        case .unknown:
            print("STS unknown")
        }
    }
    
    
    func updated(synchronizationStatus status: TCHClientSynchronizationStatus, of client: TwilioChatClient) {
        switch status {
        case .started:
            navigationTitleView?.startLoading(message: "Синхронизация...".localized)
            break
        case .channelsListCompleted:
            break
        case .completed:
            getLastMessages(for: client.channelsList().subscribedChannels(), completion: { () in
                self.navigationTitleView?.stopLoading()
                self.consultationsTable.reloadSections(IndexSet(integer: 0), with: .fade)
                self.hidePreparing(animated: true)
            })
        case .failed:
            self.show(errorMessage: "Не удалось подключиться к сервису.".localized)
        }
    }
    
}



extension VCConsultationList {//: TwilioChatDelegate {
    
    /*
     
    – chatClient:connectionStateUpdated:
    
    – chatClient:synchronizationStatusUpdated:
    
    – chatClient:channelAdded:
    
    – chatClient:channel:updated:
    
    – chatClient:channel:synchronizationStatusUpdated:
    
    – chatClient:channelDeleted:
    
    – chatClient:channel:memberJoined:
    
    – chatClient:channel:member:updated:
    
    – chatClient:channel:memberLeft:
    
    – chatClient:channel:messageAdded:
    
    – chatClient:channel:message:updated:
    
    – chatClient:channel:messageDeleted:
    
    – chatClient:errorReceived:
    
    – chatClient:typingStartedOnChannel:member:
    
    – chatClient:typingEndedOnChannel:member:
    
    – chatClient:notificationNewMessageReceivedForChannelSid:messageIndex:
    
    – chatClient:notificationUpdatedBadgeCount:
    
    – chatClient:user:updated:
    
    – chatClient:userSubscribed:
    
    – chatClient:userUnsubscribed:
    */
}
