//
//  CircleProgress.swift
//  phr.kz
//
//  Created by ivan on 04/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class CircleProgress: UIControl {
    
    //MARK: - VARS
    
    let backgroundLayer = CAShapeLayer()
    let progressLayer = CAShapeLayer()
    private var value: CGFloat = 1.0
    private let start: CGFloat = 3 * .pi / 2
    
    var progress: CGFloat {
        get {
            return value
        }
        
        set {
            if newValue < 0.0 {
                value = 0.0
            } else if newValue > 1.0 {
                value = 1.0
            } else {
                value = newValue
            }
            
            //drawProgress(animated: true)
        }
    }
    
    
    
    //MARK: - LIFE
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    
    
    //MARK: - FUNCS
    
    func setup() {
        backgroundLayer.strokeColor = UIColor(hex: 0xEFEEE5).cgColor
        backgroundLayer.fillColor = UIColor.clear.cgColor
        backgroundLayer.lineWidth = 6.0
        
        progressLayer.strokeColor = UIColor(hex: 0xFDAE55).cgColor
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.lineWidth = 6.0
        progressLayer.strokeStart = 0
        progressLayer.strokeEnd = 0
        progressLayer.shadowColor = UIColor(hex: 0xFDAE55).cgColor
        progressLayer.shadowRadius = 10.0
        progressLayer.shadowOpacity = 0.5
        progressLayer.shadowOffset = .zero
        
        var t = progressLayer.transform
        t = CATransform3DRotate(t, .pi / -2, 0, 0, 1)
        t = CATransform3DTranslate(t, -frame.width, 0, 0)
        progressLayer.transform = t
        
        layer.addSublayer(backgroundLayer)
        layer.addSublayer(progressLayer)
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        redraw()
    }
    
    
    func redraw() {
        backgroundLayer.path = UIBezierPath(ovalIn: bounds).cgPath
        progressLayer.path = UIBezierPath(ovalIn: bounds).cgPath
    }
    
    
    func set(progress: CGFloat, animated: Bool) {
        self.progress = progress
        
        if animated {
            UIView.animate(withDuration: 1.0, animations: {
                self.progressLayer.strokeEnd = self.value
            })
        } else {
            progressLayer.strokeEnd = value
        }
    }

}
