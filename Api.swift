//
//  Api.swift
//  phr.kz
//
//  Created by ivan on 16/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import SystemConfiguration

typealias JsonObject = [String: Any]
typealias JsonArray = [Any]
typealias Block<T> = ((T) -> Void)

func log(error: Error) {
    var message = "\n>>> error:\n\nLocalized description:\n\(error.localizedDescription)\n"
    
    if let reason = (error as NSError).localizedFailureReason {
        message += "Localized failure reason:\n\(reason)\n"
    }
    
    if let suggestion = (error as NSError).localizedRecoverySuggestion {
        message += "Localized recovery suggestion:\n\(suggestion)\n"
    }
    
    message += "\n<<< error\n"
    
    NSLog(message)
}

func log(message: String) {
    NSLog("\n>>> message:\n\n\(message)\n\n<<< message\n")
}


class Api: NSObject {

    //MARK: - VARS
    
    let baseString = "http://phrkz.kr.digital"
    var baseURL: URL?
    var session: URLSession
    var sessionConfiguration: URLSessionConfiguration
    
    var token: String? {
        get {
            return app.ud.string(forKey: app.keys.authorizationToken)
        }
        
        set {
            app.ud.set(newValue, forKey: app.keys.authorizationToken)
        }
    }
    
    lazy var user: ApiUser = {
        return ApiUser(parent: self)
    }()
    
    lazy var contract: ApiContract = {
        return ApiContract(parent: self)
    }()
    
    lazy var reference: ApiReference = {
        return ApiReference(parent: self)
    }()
    
    lazy var cabinet: ApiCabinet = {
        return ApiCabinet(parent: self)
    }()
    
    lazy var files: ApiFiles = {
        return ApiFiles(parent: self)
    }()
    
    lazy var doctors: ApiDoctors = {
        return ApiDoctors(parent: self)
    }()
    
    lazy var so: ApiSO = {
        return ApiSO(parent: self)
    }()
    
    lazy var complaint: ApiComplaint = {
        return ApiComplaint(parent: self)
    }()
    
    lazy var mid: ApiMID = {
        return ApiMID(parent: self)
    }()
    
    
    
    //MARK: - LIFE
    
    override init() {
        sessionConfiguration = URLSessionConfiguration.default
        sessionConfiguration.allowsCellularAccess = true
        sessionConfiguration.httpMaximumConnectionsPerHost = 2
        session = URLSession(configuration: sessionConfiguration)
        super.init()
        baseURL = URL(string: baseString)
    }
    
    
    //MARK: - FUNCS
    
    /* New URL request for current API */
    
    func request(withMethod method: Api.Method, path: String, params: [String: Any]? = nil, token: String? = nil) -> URLRequest? {
        var comps = URLComponents()
        comps.path = "api/\(app.localeCode)/\(path)"
        
        if method == .get {
            if let params = params {
                var items = [URLQueryItem]()
                for (key, value) in params {
                    items.append(URLQueryItem(name: key, value: "\(value)"))
                }
                comps.queryItems = items
            }
        }
        
        guard let url = comps.url(relativeTo: baseURL) else {
            return nil
        }
        
        var result = URLRequest(url: url)
        var serialize = false
        
        switch method {
        case .get:
            result.httpMethod = "GET"
        case .post:
            result.httpMethod = "POST"
            serialize = true
        case .put:
            result.httpMethod = "PUT"
            serialize = true
        case .delete:
            result.httpMethod = "DELETE"
        case .patch:
            result.httpMethod = "PATCH"
            serialize = true
        }
        
        if serialize {
            if let data = self.serialize(params: params)?.data(using: .utf8, allowLossyConversion: true) {
                result.httpBody = data
                result.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                result.addValue("\(data.count)", forHTTPHeaderField: "Content-Length")
            }
        }
        
        if let token = token {
            result.setValue(token, forHTTPHeaderField: "x-token")
        }
        
        return result
    }
    
    
    /* Pass request to the server */
    
    func pass(request: URLRequest?, callback: @escaping Block<JsonObject>, fallback: @escaping Block<Error>) {
        if app.reachability?.isReachable == .some(false) {
            fallback(Api.Errors.noConnection)
            return
        }
        
        guard let request = request else {
            fallback(NSError.with(description: "Невозможно отправить пустой запрос на сервер. Попробуйте позднее или обратитесь в службу поддержки.".localized))
            return
        }
        
        let task = session.dataTask(with: request) { (data, response, error) in
            OperationQueue.main.addOperation {
                if let data = data {
                    if let json = self.processJsonObject(from: data) {
                        if let error = self.processApiError(withJson: json) {
                            fallback(error)
                        } else {
                            callback(json)
                        }
                    } else {
                        fallback(Api.Errors.wrongJsonFormat)
                    }
                } else if let error = error {
                    fallback(error)
                } else {
                    fallback(Api.Errors.unknownResponse)
                }
            }
        }
        
        task.resume()
    }
    
    
    /* Checks internet connection availability */
    
    func netConnected() -> Bool {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        return (isReachable && !needsConnection)
    }
    
    
    /* Get URL formatted query string from params dict */
    
    func serialize(params: [String: Any]?) -> String? {
        guard let params = params else {
            return nil
        }
        
        var comps = [String]()
        
        for (key, value) in params {
            comps.append("\(key)=\(value)")
        }
        
        return comps.joined(separator: "&").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    }
    
    
    /* Process json object from data */
    
    func processJsonObject(from data: Data) -> JsonObject? {
        do {
            let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? JsonObject
            return json
        } catch let error as NSError {
            log(error: error)
            return nil
        }
    }
    
    
    /* Process API error */
    
    func processApiError(withJson json: JsonObject) -> NSError? {
        print(json)
        if let success = json["success"] as? Bool {
            if success {
                return nil
            }
        }
        
        if let data = json["data"] as? JsonObject {
            var code: Int
            var description: String
            
            if let c = data["code"] as? Int {
                code = c
            } else {
                code = 0
            }
            
            if let errors = data["errors"] as? JsonObject {
                var message = ""
                var firstLoop = true
                for (_, value) in errors {
                    if let v = value as? [String] {
                        if firstLoop {
                            message += v.joined(separator: "\n")
                            firstLoop = false
                        } else {
                            message += "\n\n\(v.joined(separator: "\n"))"
                        }
                    } else if let v = value as? String {
                        if firstLoop {
                            message += v
                            firstLoop = false
                        } else {
                            message += "\n\n\(v)"
                        }
                    }
                }
                description = message
            } else if let message = data["message"] as? String {
                description = message
            } else {
                description = Api.Errors.wrongJsonFormat.localizedDescription
            }
            
            return NSError.with(description: description, code: code)
        } else {
            return Api.Errors.wrongJsonFormat
        }
    }
    
    
    //MARK: - Images
    
    func loadImage() {
        
    }
    
    
    
    //MARK: - Api Post
    
    enum Method {
        case get, post, put, delete, patch
    }
    
    
    //MARK: - ApiErrors
    
    struct Errors {
        static let noConnection = NSError.with(description: "Нет подключения к интернету.".localized)
        static let wrongJsonFormat = NSError.with(description: "Неверный формат ответа сервера. Попробуйте позднее или обратитесь в службу поддержки.".localized)
        static let unknownResponse = NSError.with(description: "Неизвестная ошибка сервера. Попробуйте позднее или обратитесь в службу поддержки.".localized)
    }
}




