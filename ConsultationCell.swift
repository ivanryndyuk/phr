//
//  ConsultationCell.swift
//  phr.kz
//
//  Created by ivan on 23/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ConsultationCell: UITableViewCell {

    //MARK: - VARS
    
    @IBOutlet weak var photoView: UIImageView!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLabel: UILabel!
    @IBOutlet weak var border: UIView!
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var statusView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var doctorTypeLabel: UILabel!
    @IBOutlet weak var lastMessageLabel: UILabel!
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var secondOpinionView: UIView!
    @IBOutlet weak var secondOpinionTitle: UILabel!
    @IBOutlet weak var secondOpinionProgress: ProgressBarExtended!
    @IBOutlet weak var secondOpinionStepsLabel: UILabel!
    
    var badge: String? {
        get {
            return badgeLabel.attributedText?.string
        }
        
        set {
            if let value = newValue {
                badgeLabel.attributedText = NSAttributedString(string: value)
                badgeView.isHidden = false
                contentView.backgroundColor = UIColor(hex: 0xEFEEE5)
                badgeView.backgroundColor = UIColor(hex: 0xEFEEE5)
                dotView.isHidden = false
            } else {
                badgeLabel.attributedText = nil
                badgeView.isHidden = true
                contentView.backgroundColor = UIColor.clear
                badgeView.backgroundColor = UIColor.white
                dotView.isHidden = true
            }
        }
    }
    
    
    var isSent = false {
        willSet {
            if !isRead {
                if newValue {
                    statusView.image = #imageLiteral(resourceName: "icon-sent")
                } else {
                    statusView.image = nil
                }
            }
        }
    }
    
    
    var isRead = false {
        willSet {
            if newValue {
                statusView.image = #imageLiteral(resourceName: "icon-read")
            } else {
                if isSent {
                    statusView.image = #imageLiteral(resourceName: "icon-sent")
                } else {
                    statusView.image = nil
                }
            }
        }
    }
    
    
    var progress: CGFloat = 0 {
        willSet {
            secondOpinionProgress.bar.set(progress: newValue, for: secondOpinionProgress.frame)
        }
    }
    
    
    
    //MARK: - FUNCS
    
    func reset() {
        //secondOpinionHeight.constant = 0
        badge = nil
        isSent = false
        isRead = false
        secondOpinionView.isHidden = true
    }

}
