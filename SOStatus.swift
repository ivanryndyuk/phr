//
//  SOStatus.swift
//  phr.kz
//
//  Created by ivan on 26/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class SOStatus: NSObject, NSCoding {
    
    //MARK: - VARS
    
    var name: String
    var code: String
    
    

    //MARK: - LIFE
    
    init?(json: JsonObject) {
        guard let name = json["name"] as? String else {
            return nil
        }
        
        guard let code = json["code"] as? String else {
            return nil
        }
        
        self.name = name
        self.code = code
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        guard let name = aDecoder.decodeObject(forKey: "name") as? String else {
            return nil
        }
        
        guard let code = aDecoder.decodeObject(forKey: "code") as? String else {
            return nil
        }
        
        self.name = name
        self.code = code
    }
    
    
    
    //MARK: - FUNCS
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: "name")
        aCoder.encode(code, forKey: "code")
    }
    
}
