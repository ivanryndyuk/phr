//
//  Button.swift
//  phr.kz
//
//  Created by ivan on 15/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

@IBDesignable class Button: UIButton {
    
    //MARK: - VARS
    
    @IBInspectable var cornerRadius: CGFloat = 0.0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            if let cgcolor = layer.shadowColor {
                return UIColor(cgColor: cgcolor)
            } else {
                return nil
            }
        }
        
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }
    
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        
        set {
            layer.shadowRadius = newValue
        }
    }
    
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    
    /*@IBInspectable var shadowColor: UIColor? {
        didSet {
            if let color = shadowColor {
                layer.shadowColor = color.cgColor
            }
        }
    }
    
    
    @IBInspectable var shadowOffset = CGSize.zero {
        didSet {
            layer.shadowOffset = shadowOffset
        }
    }
    
    
    @IBInspectable var shadowRadius: CGFloat = 0.0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }*/
    
    
    /*@IBInspectable override var shadowOpacity: Float = 0.0 {
        didSet {
            layer.shadowOpacity = shadowOpacity
        }
    }*/
    
    
    
    //MARK: - TOUCH
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let transform = CATransform3DMakeTranslation(0.0, 5.0, 0.0)
        
        UIView.animate(withDuration: 0.2) { 
            self.layer.transform = transform
            self.titleLabel?.alpha = 0.35
        }
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2, animations: { 
            self.layer.transform = CATransform3DIdentity
            self.titleLabel?.alpha = 1.0
        }) { (finished) in
            if finished {
                if let touch = touches.first {
                    let point = touch.location(in: self)
                    if self.bounds.contains(point) {
                        self.sendActions(for: .touchUpInside)
                    }
                }
            }
        }
    }
    
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2) {
            self.layer.transform = CATransform3DIdentity
            self.titleLabel?.alpha = 1.0
        }
    }
    
}
