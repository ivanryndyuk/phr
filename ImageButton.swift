//
//  ImageButton.swift
//  phr.kz
//
//  Created by ivan on 24/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ImageButton: UIControl {

    //MARK: - VARS
    
    @IBOutlet weak var imageView: UIImageView?
    @IBOutlet weak var titleLabel: UILabel?
    
    var image: UIImage? {
        get {
            return imageView?.image
        }
        
        set {
            imageView?.image = newValue
        }
    }
    
    var title: String? {
        get {
            return titleLabel?.text
        }
        
        set {
            titleLabel?.text = newValue
        }
    }
    
    
    
    //MARK: - LIFE
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    
    
    //MARK: - FUNCS
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2) {
            self.alpha = 0.35
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2, animations: {
            self.alpha = 1.0
        }) { (finished) in
            if finished {
                if let touch = touches.first {
                    let point = touch.location(in: self)
                    if self.bounds.contains(point) {
                        self.sendActions(for: .touchUpInside)
                    }
                }
            }
        }
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        UIView.animate(withDuration: 0.2) {
            self.alpha = 1.0
        }
    }
    
}
