//
//  VCAddMID.swift
//  phr.kz
//
//  Created by Ivan Ryndyuk on 29/08/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCAddMID: UITableViewController {
    
    //MARK: - VARS
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    var switcher: UISwitch?
    
    
    
    //MARK: - LIFE
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.barTintColor = UIColor(hex: 0x495787)
        navigationController?.navigationBar.tintColor = UIColor(hex: 0xFFFFFF)
        (navigationController as? NavigationController)?.useLightStatusBar = true
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(cancelItem: UIBarButtonItem) {
        switcher?.isOn = false
        navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func handlerOf(doneItem: UIBarButtonItem) {
        guard let email = emailField.text else {
            app.alert(title: "Ошибка".localized, message: "Укажите эл. почту.".localized)
            return
        }
        
        guard let password = passwordField.text else {
            app.alert(title: "Ошибка".localized, message: "Укажите пароль.".localized)
            return
        }
        
        if email.isEmpty || password.isEmpty {
            app.alert(title: "Ошибка".localized, message: "Укажите эл. почту и пароль.".localized)
            return
        }
        
        app.api.mid.attachMID(email: email, password: password, callback: { (string) in
            app.user?.isMCAttached = true
            app.stack.save()
            self.switcher?.isOn = true
            self.switcher?.isEnabled = false
            
            if let message = string {
                let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
                    self.navigationController?.popViewController(animated: true)
                }))
                
                self.present(alert, animated: true, completion: nil)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
        }) { (error) in
            self.switcher?.isOn = false
            app.alert(error: error)
        }
    }

}
