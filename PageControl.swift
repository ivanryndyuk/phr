//
//  PageControl.swift
//  phr.kz
//
//  Created by ivan on 16/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class PageControl: UIPageControl {
    
    //MARK: - VARS
    
    override var currentPage: Int {
        didSet {
            updateDots()
        }
    }
    
    
    
    //MARK: - LIFE
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateDots()
    }
    
    
    
    //MARK: - FUNCS
    
    func updateDots() {
        for i in 0..<subviews.count {
            let subview = subviews[i]
            subview.backgroundColor = UIColor.clear
            var dot: UIView
            
            if subview.subviews.count == 0 {
                dot = UIView(frame: subview.bounds)
                dot.layer.cornerRadius = subview.frame.width / 2
                subview.addSubview(dot)
            } else {
                dot = subview.subviews[0]
            }
            
            if i == currentPage {
                UIView.animate(withDuration: 0.2, animations: { 
                    dot.backgroundColor = UIColor(hex: 0x329DD2)
                    dot.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
                })
            } else {
                UIView.animate(withDuration: 0.2, animations: { 
                    dot.backgroundColor = UIColor(hex: 0xD8D8D8)
                    dot.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
                })
            }
        }
    }

}
