//
//  DataStack.swift
//  phr.kz
//
//  Created by ivan on 07/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import CoreData

class DataStack: NSObject {

    let modelName = "phrkz"
    var persistentStoreCoordinator: NSPersistentStoreCoordinator?
    var context: NSManagedObjectContext?
    
    
    
    //MARK: - LIFE
    
    override init() {
        super.init()
        addPersistentStore()
    }
    
    
    func addPersistentStore() {
        guard let storeUrl = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last?.appendingPathComponent("\(modelName).sqlite") else {
            log(message: "Can't create store url for data stack.")
            return
        }
        
        guard let modelUrl = Bundle.main.url(forResource: modelName, withExtension: "momd") else {
            log(message: "Can't create model url for data stack.")
            return
        }
        
        guard let managedObjectModel = NSManagedObjectModel(contentsOf: modelUrl) else {
            log(message: "Can't load managed object model for data stack.")
            return
        }
        
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        do {
            try persistentStoreCoordinator?.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeUrl, options: [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true])
        } catch let error as NSError {
            log(error: error)
            return
        }
        
        context = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
        context?.mergePolicy = NSMergePolicy(merge: NSMergePolicyType.mergeByPropertyObjectTrumpMergePolicyType)
        context?.persistentStoreCoordinator = persistentStoreCoordinator
    }
    
    
    
    //MARK: - FUNCS
    
    /* Inserting new empty managed object into context */
    
    func newItem<T: NSManagedObject>() -> T? {
        guard let context = context else {
            log(message: "Can't insert managed object, because context is nil.")
            return nil
        }
        return NSEntityDescription.insertNewObject(forEntityName: String(describing: T.self), into: context) as? T
    }
    
    
    /* Get managed object for Entity class (T) with predicate format string */
    
    func item <T: NSManagedObject>(forEntity entity: T.Type, predicateFormat: String? = nil) -> T? {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: entity))
        
        if let predicateFormat = predicateFormat {
            request.predicate = NSPredicate(format: predicateFormat)
        }
        
        request.fetchLimit = 1
        
        var result: T?
        
        do {
            result = try context?.fetch(request).last as? T
        } catch let error {
            log(error: error)
        }
        
        return result
    }
    
    
    /* Get list of managed objects for Entity class (T) with predicate format string */
    
    func items <T: NSManagedObject>(forEntity entity: T.Type, predicateFormat: String? = nil) -> [T] {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: entity))
        
        if let predicateFormat = predicateFormat {
            request.predicate = NSPredicate(format: predicateFormat)
        }
        
        var result = [T]()
        
        do {
            if let list = try context?.fetch(request) as? [T] {
                result = list
            }
        } catch let error {
            log(error: error)
        }
        
        return result
    }
    
    
    /* Remove all items for entity. */
    
    func removeAll <T: NSManagedObject>(forEntity entity: T.Type) -> Bool {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: entity))
        let delete = NSBatchDeleteRequest(fetchRequest: request)
        
        var result = false
        
        do {
            try context?.execute(delete)
            result = true
        } catch let error {
            log(error: error)
        }
        
        return result
    }
    
    
    /* Saving context */
    
    func save() {
        guard let context = context else {
            log(message: "Can't save context which is nil.")
            return
        }
        if context.hasChanges {
            do {
                try context.save()
            } catch let error {
                log(error: error)
            }
        }
    }
    
    
    /* Resetting context */
    
    func reset() {
        guard let context = context else {
            log(message: "Can't reset context which is nil.")
            return
        }
        context.reset()
    }
    
    
    /* Reset persistent store */
    
    func resetPersistentStore() {
        if let stores = persistentStoreCoordinator?.persistentStores {
            for store in stores {
                do {
                    try persistentStoreCoordinator?.remove(store)
                } catch let error {
                    log(error: error)
                }
                
                if let path = store.url?.path {
                    do {
                        try FileManager.default.removeItem(atPath: path)
                    } catch let error {
                        log(error: error)
                    }
                }
            }
        }
        
        reset()
        addPersistentStore()
    }
    
    
    
    //MARK: User
    
    /* Returns user from data base if exists. */
    
    func user() -> User? {
        if let item = item(forEntity: User.self, predicateFormat: "key == 'user'") {
            return item
        } else {
            let item: User? = newItem()
            item?.key = "user"
            save()
            return item
        }
    }
    
    
    
    //MARK: - Messages
    
    func getHistory(forChannelSid sid: String) -> [Message] {
        var result = [Message]()
        
        if let item = item(forEntity: History.self, predicateFormat: "channelSid == '\(sid)'") {
            result = item.messages 
        }
        
        return result
    }
    
    func setHistory(forChannelSid sid: String, messages: [Message]) {
        if let item = item(forEntity: History.self, predicateFormat: "channelSid == '\(sid)'") {
            item.messages = messages
            save()
        }
    }
    
    
    
    //MARK: - Contracts
    
    func contract(forJson json: JsonObject) -> Contract? {
        guard let id = json["id"] as? String else {
            return nil
        }
        
        guard let number = json["number"] as? String else {
            return nil
        }
        
        guard let price = json["price"] as? NSNumber else {
            return nil
        }
        
        guard let channelJson = json["channel"] as? JsonObject else {
            return nil
        }
        
        let df = DateFormatter()
        df.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'+'HH':'mm"
        
        guard let startDateString = json["startedAt"] as? String else {
            return nil
        }
        
        guard let endDateString = json["endedAt"] as? String else {
            return nil
        }
        
        guard let startDate = df.date(from: startDateString) else {
            return nil
        }
        
        guard let endDate = df.date(from: endDateString) else {
            return nil
        }
        
        guard let channel = Channel(json: channelJson) else {
            return nil
        }
        
        guard let doctorJson = json["doctor"] as? JsonObject else {
            return nil
        }
        
        guard let patientJson = json["patient"] as? JsonObject else {
            return nil
        }
        
        guard let specialitiesJson = json["specialties"] as? JsonArray else {
            return nil
        }
        
        guard let sicknessesJson = json["diseases"] as? JsonArray else {
            return nil
        }
        
        guard let organizationJson = json["organization"] as? JsonObject else {
            return nil
        }
        
        guard let type = json["entity_type"] as? String else {
            return nil
        }
        
        
        var result: Contract?
        
        if let item = item(forEntity: Contract.self, predicateFormat: "id == '\(id)'") {
            result = item
        } else {
            result = newItem()
        }
        
        result?.id = id
        result?.number = number
        result?.price = price
        result?.channel = channel
        result?.startDate = startDate
        result?.endDate = endDate
        result?.doctor = Doctor(json: doctorJson)
        result?.specialities = NSSet(array: specialities(fromJson: specialitiesJson))
        result?.sicknesses = NSSet(array: sicknesses(fromJson: sicknessesJson))
        result?.patient = Patient(json: patientJson)
        result?.type = type
        
        if let organization = organization(forJson: organizationJson) {
            result?.organization = organization
            save()
        }
        
        if Date().compare(endDate) == .orderedDescending {
            result?.isFinished = true
        }
        
        return result
    }
    
    
    func contracts(fromJson json: JsonArray) -> [Contract] {
        var result = [Contract]()
        var oldIds = [String]()
        var newIds = [String]()
        
        for item in app.stack.items(forEntity: Contract.self) {
            if let id = item.id {
                oldIds.append(id)
            }
        }
        
        for case let item as JsonObject in json {
            if let contract = contract(forJson: item) {
                result.append(contract)
                if let id = contract.id {
                    newIds.append(id)
                }
            }
        }
        
        for oldId in oldIds {
            if !newIds.contains(oldId) {
                if let item = item(forEntity: Contract.self, predicateFormat: "id == '\(oldId)'") {
                    context?.delete(item)
                }
            }
        }
        
        return result
    }
    
    
    func so(forJson json: JsonObject) -> SO? {
        // TODO: Process folowing parameters if needed
        // "translationDirections": []
        // "conclusionFiles": []
        
        guard let id = json["id"] as? String else {
            return nil
        }
        
        guard let specialitiesJson = json["specialties"] as? JsonArray else {
            return nil
        }
        
        guard let sicknessesJson = json["diseases"] as? JsonArray else {
            return nil
        }
        
        guard let channelJson = json["channel"] as? JsonObject else {
            return nil
        }
        
        guard let patientJson = json["patient"] as? JsonObject else {
            return nil
        }
        
        guard let channel = Channel(json: channelJson) else {
            return nil
        }
        
        guard let statusJson = json["status"] as? JsonObject else {
            return nil
        }
        
        guard let status = SOStatus(json: statusJson) else {
            return nil
        }
        
        guard let checkListItemsJson = json["checklistItems"] as? JsonArray else {
            return nil
        }
        
        guard let price = json["price"] as? Int else {
            return nil
        }
        
        guard let type = json["entity_type"] as? String else {
            return nil
        }
        
        
        var result: SO?
        
        if let item = item(forEntity: SO.self, predicateFormat: "id == '\(id)'") {
            result = item
        } else {
            result = newItem()
        }
        
        result?.id = id
        result?.channel = channel
        result?.patient = Patient(json: patientJson)
        result?.specialities = NSSet(array: self.specialities(fromJson: specialitiesJson))
        result?.sicknesses = NSSet(array: sicknesses(fromJson: sicknessesJson))
        result?.status = status
        result?.checkListItems = CheckListItem.list(fromJson: checkListItemsJson)
        result?.price = NSNumber(integerLiteral: price)
        result?.conclusionText = json["conclusionText"] as? String
        result?.type = type
        
        if let requestDoctorJson = json["requestDoctor"] as? JsonObject {
            result?.requestDoctor = Doctor(json: requestDoctorJson)
        }
        
        if let doctorJson = json["doctor"] as? JsonObject {
            result?.doctor = Doctor(json: doctorJson)
        }
        
        if let organizationJson = json["organization"] as? JsonObject {
            result?.organization = organization(forJson: organizationJson)
        }
        
        if let number = json["number"] as? String {
            result?.number = number
        }
        
        if status.code == "completed" {
            result?.isFinished = true
        }
        
        if let conclusion = json["conclusionText"] as? String {
            result?.conclusion = conclusion
        }
        
        return result
    }
    
    
    func soList(fromJson json: JsonArray) -> [SO] {
        var result = [SO]()
        
        for case let item as JsonObject in json {
            if let so = so(forJson: item) {
                result.append(so)
            }
        }
        
        return result
    }
    
    
    func organization(forJson json: JsonObject) -> Organization? {
        guard let id = json["id"] as? String else {
            return nil
        }
        
        guard let name = json["name"] as? String else {
            return nil
        }
        
        guard let address = json["address"] as? String else {
            return nil
        }
        
        var result: Organization?
        
        if let item = item(forEntity: Organization.self, predicateFormat: "id == '\(id)'") {
            result = item
        } else {
            result = newItem()
        }
        
        result?.id = id
        result?.name = name
        result?.address = address
        
        return result
    }
    
    
    func organizations(fromJson json: JsonArray) -> [Organization] {
        var result = [Organization]()
        let organizations = items(forEntity: Organization.self)
        
        for case let item as JsonObject in json {
            if let organization = organization(forJson: item) {
                result.append(organization)
            }
        }
        
        for organization in organizations {
            if !result.contains(organization) {
                context?.delete(organization)
            }
        }
        
        app.stack.save()
        
        return result
    }
    
    
    
    //MARK: Reference
    
    func sickness(forJson json: JsonObject) -> Sickness? {
        guard let id = json["id"] as? String else {
            log(message: "Sickness without id cannot be instantiated.")
            return nil
        }
        
        var result: Sickness?
        
        if let item = item(forEntity: Sickness.self, predicateFormat: "id == '\(id)'") {
            result = item
        } else {
            result = newItem()
            result?.id = id
        }
        
        result?.apply(json: json)
        
        return result
    }
    
    
    func sicknesses(fromJson json: JsonArray) -> [Sickness] {
        var result = [Sickness]()
        
        for case let item as JsonObject in json {
            if let sickness = sickness(forJson: item) {
                result.append(sickness)
            }
        }
        
        app.stack.save()
        return result
    }
    
    
    func region(forJson json: JsonObject) -> Region? {
        guard let id = json["id"] as? String else {
            log(message: "Region without id cannot be instantiated.")
            return nil
        }
        
        var result: Region?
        
        if let item = item(forEntity: Region.self, predicateFormat: "id == '\(id)'") {
            result = item
        } else {
            result = newItem()
            result?.id = id
        }
        
        result?.apply(json: json)
        
        return result
    }
    
    
    /* Speciality new instance or update existed. */
    
    func speciality(forJson json: JsonObject) -> Speciality? {
        guard let id = json["id"] as? String else {
            log(message: "Speciality without id cannot be instantiated.")
            return nil
        }
        
        guard let name = json["name"] as? String else {
            log(message: "Speciality without name cannot be instantiated.")
            return nil
        }
        
        var result: Speciality?
        
        if let item = item(forEntity: Speciality.self, predicateFormat: "id == '\(id)'") {
            result = item
        } else {
            result = newItem()
        }
        
        result?.id = id
        result?.name = name
        
        return result
    }
    
    
    /* Update list of specialities. */
    
    func specialities(fromJson json: JsonArray) -> [Speciality] {
        var result = [Speciality]()
        
        for case let item as JsonObject in json {
            if let speciality = speciality(forJson: item) {
                result.append(speciality)
            }
        }
        
        return result
    }
    
    
    
    //MARK: Files
    
    func file(forJson json: JsonObject) -> File? {
        guard let id = json["id"] as? String else {
            log(message: "File without id cannot be instantiated.")
            return nil
        }
        
        var result: File?
        
        if let item = item(forEntity: File.self, predicateFormat: "id == '\(id)'") {
            result = item
        } else {
            result = newItem()
            result?.id = id
        }
        
        result?.apply(json: json)
        
        return result
    }
    
}
