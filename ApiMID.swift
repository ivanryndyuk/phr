//
//  ApiMID.swift
//  phr.kz
//
//  Created by Ivan Ryndyuk on 21/08/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class ApiMID: ApiSection {
    
    //MARK: - FUNCS
    
    func attachMID(email: String, password: String, callback: @escaping Block<String?>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .post, path: "medical-card/link", params: ["email": email, "password": password], token: parent.token)
        parent.pass(request: request, callback: { (json) in
            callback(json["message"] as? String)
        }, fallback: fallback)
    }
    
    
    func grantAccess(consultationID: String, callback: @escaping Block<Void>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .post, path: "medical-card/access", params: ["treaty_id": consultationID], token: parent.token)
        parent.pass(request: request, callback: { (json) in
            callback()
        }, fallback: fallback)
    }
    
    
    func getLink(consultationID: String, callback: @escaping Block<String>, fallback: @escaping Block<Error>) {
        let request = parent.request(withMethod: .get, path: "medical-card/link/\(consultationID)", token: parent.token)
        parent.pass(request: request, callback: { (json) in
            if let data = json["data"] as? JsonObject {
                if let linkString = data["url"] as? String {
                    callback(linkString)
                    return
                }
            }
            
            fallback(Api.Errors.wrongJsonFormat)
        }, fallback: fallback)
    }

}
