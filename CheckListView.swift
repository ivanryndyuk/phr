//
//  CheckListView.swift
//  phr.kz
//
//  Created by ivan on 14/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class CheckListView: UIView {

    //MARK: - VARS
    
    @IBOutlet weak var statusTable: UITableView!
    @IBOutlet weak var checkListTable: UITableView!
    @IBOutlet weak var addButton: UIButton!
    
    
    
    //MARK: - LIFE
    
    class func instance(dataSource: UITableViewDataSource, delegate: UITableViewDelegate) -> CheckListView {
        if let instance = Bundle.main.loadNibNamed("CheckListView", owner: nil, options: nil)?.first as? CheckListView {
            instance.statusTable.register(UINib(nibName: "CheckListStatusCell", bundle: nil), forCellReuseIdentifier: "checkListStatusCell")
            instance.statusTable.dataSource = dataSource
            instance.statusTable.delegate = delegate
            instance.checkListTable.register(UINib(nibName: "CheckListItemCell", bundle: nil), forCellReuseIdentifier: "checkListItemCell")
            instance.checkListTable.dataSource = dataSource
            instance.checkListTable.delegate = delegate
            return instance
        } else {
            return CheckListView()
        }
    }
    
}
