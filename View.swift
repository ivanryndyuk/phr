//
//  View.swift
//  phr.kz
//
//  Created by ivan on 01/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

@IBDesignable class View: UIView {
    
    //MARk: - VARS
    
    @IBInspectable override var radius: CGFloat {
        didSet {
            layer.cornerRadius = radius
        }
    }
    
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            if let cgcolor = layer.shadowColor {
                return UIColor(cgColor: cgcolor)
            } else {
                return nil
            }
        }
        
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }
    
    
    @IBInspectable var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        
        set {
            layer.shadowRadius = newValue
        }
    }
    
    
    @IBInspectable var shadowOffset: CGSize {
        get {
            return layer.shadowOffset
        }
        
        set {
            layer.shadowOffset = newValue
        }
    }
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            if let cgColor = layer.borderColor {
                return UIColor(cgColor: cgColor)
            }
            
            return nil
        }
        
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        
        set {
            layer.borderWidth = newValue
        }
    }

}
