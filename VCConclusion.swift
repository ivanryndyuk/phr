//
//  VCConclusion.swift
//  phr.kz
//
//  Created by ivan on 31/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class VCConclusion: ModalController {
    
    //MARK: - VARS
    
    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var textLabel: UILabel!
    
    private var needsUpdateHeight = true
    
    var header: String? {
        get {
            return headerLabel.text
        }
        
        set {
            headerLabel.text = newValue
            needsUpdateHeight = true
        }
    }
    
    var text: String? {
        get {
            return textLabel.attributedText?.string
        }
        
        set {
            if let newValue = newValue {
                textLabel.attributedText = NSAttributedString(string: newValue, attributes: attrs)
                needsUpdateHeight = true
            }
        }
    }
    
    var attrs = [String: Any]()
    
    
    
    //MARK: - LIFE

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let ps = NSMutableParagraphStyle()
        ps.lineBreakMode = .byWordWrapping
        ps.minimumLineHeight = 22.0
        ps.maximumLineHeight = 22.0
        
        attrs[NSFontAttributeName] = UIFont.with(name: app.theme.fontMuseoSansCyrl, size: 14)
        attrs[NSParagraphStyleAttributeName] = ps
        attrs[NSForegroundColorAttributeName] = UIColor(hex: 0x757475)
        
        set(header: header, text: text)
    }
    
    
    
    //MARK: - FUNCS
    
    @IBAction func handlerOf(backItem: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if needsUpdateHeight {
            updateHeight()
            needsUpdateHeight = false
        }
    }
    
    
    func set(header: String?, text: String?) {
        self.header = header
        self.text = text
        updateHeight()
    }
    
    
    func updateHeight() {
        var height: CGFloat = 188.0
        
        if let header = headerLabel.text {
            height += header.height(forWidth: UIScreen.main.bounds.width - 88.0, attributes: [NSFontAttributeName: headerLabel.font])
        }
        
        if let text = textLabel.attributedText {
            height += text.string.height(forWidth: UIScreen.main.bounds.width - 88.0, attributes: attrs)
        }
        
        print(height)
        contentHeight?.constant = height
    }
    
}
