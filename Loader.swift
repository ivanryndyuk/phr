//
//  Loader.swift
//  phr.kz
//
//  Created by ivan on 21/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class Loader: UIView {

    //MARK: - VARS
    
    private let imageView = UIImageView()
    
    var isAnimating: Bool {
        return imageView.isAnimating
    }
    
    
    
    //MARK: - LIFE
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialSetup()
    }
    
    private func initialSetup() {
        imageView.image = #imageLiteral(resourceName: "loader-1")
        imageView.animationImages = [#imageLiteral(resourceName: "loader-1"), #imageLiteral(resourceName: "loader-2"), #imageLiteral(resourceName: "loader-3"), #imageLiteral(resourceName: "loader-4"), #imageLiteral(resourceName: "loader-5")]
        imageView.animationDuration = 0.25
        imageView.animationRepeatCount = 0
        addSubview(imageView)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat:
            "V:|-0-[imageView]-0-|", options: .zero, metrics: nil, views: ["imageView": imageView]))
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat:
            "H:|-0-[imageView]-0-|", options: .zero, metrics: nil, views: ["imageView": imageView]))
    }
    
    
    
    //MARK: - FUNCS
    
    func startAnimating() {
        imageView.startAnimating()
    }
    
    
    func stopAnimating() {
        imageView.stopAnimating()
    }

}
