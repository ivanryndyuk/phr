//
//  History.swift
//  phr.kz
//
//  Created by ivan on 12/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import CoreData

class History: NSManagedObject {
    
    //MARK: - VARS
    
    @NSManaged var channelSid: String
    @NSManaged var messages: [Message]
    
}
