//
//  CenteredTextView.swift
//  phr.kz
//
//  Created by ivan on 05/06/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit

class CenteredTextView: UITextView {

    //MARK: - LIFE
    
    override var intrinsicContentSize: CGSize {
        get {
            var result = contentSize
            result.width += ((textContainerInset.left + textContainerInset.right) / 2.0)
            result.height += ((textContainerInset.top + textContainerInset.bottom) / 2.0)
            return result
        }
    }
    
    
    
    //MARK: - FUNCS
    
    override func layoutSubviews() {
        super.layoutSubviews()
        if intrinsicContentSize.height <= bounds.size.height {
            var top = (bounds.size.height - contentSize.height * zoomScale) / 2.0
            top = top < 0.0 ? 0.0 : top
            contentOffset = CGPoint(x: 0, y: -top)
        }
    }

}
