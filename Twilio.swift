//
//  Twilio.swift
//  phr.kz
//
//  Created by ivan on 30/05/2017.
//  Copyright © 2017 ivanryndyuk. All rights reserved.
//

import UIKit
import TwilioChatClient
import TwilioAccessManager
import AudioToolbox

protocol TwilioDelegate {
    func clientSyncCompleted()
}

protocol TwilioChatDelegate {
    func new(message: TCHMessage, channel: TCHChannel)
}

protocol ConsultationsDelegate {
    func updated(connectionState state: TCHClientConnectionState, of client: TwilioChatClient)
    func updated(synchronizationStatus status: TCHClientSynchronizationStatus, of client: TwilioChatClient)
    func received(error: TCHError, for client: TwilioChatClient)
}

class Twilio: NSObject {
    
    //MARK: - VARS
    
    var client: TwilioChatClient?
    var channelsList: TCHChannels?
    var channels = [TCHChannel]()
    
    var consultationsDelegate: ConsultationsDelegate?
    var startingDelegate: TwilioDelegate?
    var chatDelegate: TwilioChatDelegate?
    
    var isConnected: Bool {
        get {
            if let client = client {
                switch client.connectionState {
                case .connected:
                    return true
                default:
                    return false
                }
            } else {
                return false
            }
        }
    }
    
    
    
    //MARK: - FUNCS
    
    func connect(completion: @escaping (Bool, NSError?) -> Void) {
        if (client != nil) {
            disconnect()
        }
        
        requestToken { (succeeded, token) in
            if let token = token, succeeded {
                self.client(withToken: token, completion: completion)
            } else {
                let error = NSError.with(description: "Не удаётся получить ключ доступа.".localized, code: 301)
                completion(succeeded, error)
            }
        }
    }
    
    
    /* Disconnect from twilio. */
    
    func disconnect() {
        DispatchQueue.global(qos: .userInitiated).async {
            self.client?.shutdown()
            self.client = nil
        }
    }
    
    
    /* Request user access token. */
    
    func requestToken(withCompletion completion: @escaping (Bool, String?) -> Void) {
        app.api.cabinet.channelToken(callback: { (token) in
            completion(true, token)
        }) { (error) in
            log(error: error)
            completion(false, nil)
        }
    }
    
    
    /* Initialize client. */
    
    func client(withToken token: String, completion: @escaping (Bool, NSError?) -> Void) {
        let accessManager = TwilioAccessManager(token: token, delegate: self)
        TwilioChatClient.chatClient(withToken: token, properties: nil, delegate: self, completion: { (result, client) in
            var success = false
            var error: NSError? = nil
            
            if let result = result {
                if result.isSuccessful() {
                    if let client = client {
                        self.client = client
                        accessManager?.registerClient(client, forUpdates: { (token) in
                            client.updateToken(token, completion: { (result) in
                                if let error = result?.error {
                                    app.alert(error: error)
                                }
                            })
                        })
                        success = true
                    }
                }
            }
            
            if let e = result?.error {
                error = e
            } else {
                error = NSError.with(description: "Ошибка подключения.".localized)
            }
            
            completion(success, error)
        })
    }
    
    
    /* Get channel for specified SID. */
    
    func channel(withSID sid: String, callback: @escaping Block<TCHChannel>, fallback: @escaping Block<Error>) {
        if let channelsList = self.channelsList {
            channelsList.channel(withSidOrUniqueName: sid, completion: { (result, channel) in
                if result?.isSuccessful() == .some(true) {
                    if let channel = channel {
                        if !self.channels.contains(channel) {
                            self.channels.append(channel)
                        }
                        callback(channel)
                    } else {
                        fallback(NSError.with(description: "Невозможно подключиться к несуществующему чату.".localized))
                    }
                } else {
                    if let error = result?.error {
                        fallback(error)//NSError.with(description: error.localizedDescription))
                    } else {
                        fallback(NSError.with(description: "Не удалось подключиться к данному чату.".localized))
                    }
                }
            })
        } else {
            fallback(NSError.with(description: "Список доступных чатов пуст.".localized))
        }
    }
    
    
    /* Get last message for the channel. */
    
    func lastMessage(forChannel channel: TCHChannel, completion: @escaping (Bool, TCHMessage?) -> Void) {
        channel.messages.getLastWithCount(1) { (result, messages) in
            if let messages = messages {
                if !messages.isEmpty {
                    completion(true, messages[0])
                } else {
                    completion(false, nil)
                }
            } else {
                completion(false, nil)
            }
        }
    }
    
    
}



// MARK: - TwilioChatClientDelegate

extension Twilio: TwilioChatClientDelegate {
    
    func chatClient(_ client: TwilioChatClient!, errorReceived error: TCHError!) {
        consultationsDelegate?.received(error: error, for: client)
    }
    
    
    func chatClient(_ client: TwilioChatClient!, connectionStateUpdated state: TCHClientConnectionState) {
        consultationsDelegate?.updated(connectionState: state, of: client)
    }
    
    
    func chatClient(_ client: TwilioChatClient!, synchronizationStatusUpdated status: TCHClientSynchronizationStatus) {
        consultationsDelegate?.updated(synchronizationStatus: status, of: client)
        if status == .completed {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
            self.channelsList = client.channelsList()
            app.nc.post(Notification(name: app.notification(name: app.keys.synchronizationCompleted)))
            //startingDelegate?.clientSyncCompleted()
        }
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channelAdded channel: TCHChannel!) {
        app.nc.post(name: app.notification(name: app.keys.channelAddedNote), object: nil, userInfo: ["channel": channel])
        //app.twilio.channelManager.chatClient(client, channelAdded: channel)
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channelChanged channel: TCHChannel!) {
        print("channelChanged")
        //app.twilio.channelManager.chatClient(client, channelChanged: channel)
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channel: TCHChannel!, updated: TCHChannelUpdate) {
        app.nc.post(name: app.notification(name: app.keys.channelUpdatedNote), object: nil, userInfo: ["channel": channel, "updated": updated])
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channelDeleted channel: TCHChannel!) {
        app.nc.post(name: app.notification(name: app.keys.channelDeletedNote), object: nil, userInfo: ["channel": channel])
        //app.twilio.channelManager.chatClient(client, channelDeleted: channel)
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channel: TCHChannel!, messageAdded message: TCHMessage!) {
        if app.shared.applicationState == .active {
            app.nc.post(app.newMessageNote(channel: channel, message: message))
        }
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channel: TCHChannel!, message: TCHMessage!, updated: TCHMessageUpdate) {
        app.nc.post(app.updateMessageNote(channel: channel, message: message))
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channel: TCHChannel!, messageDeleted message: TCHMessage!) {
        app.nc.post(app.deleteMessageNote(channel: channel, message: message))
    }
    
    
    func chatClient(_ client: TwilioChatClient!, typingEndedOn channel: TCHChannel!, member: TCHMember!) {
        
    }
    
    
    func chatClient(_ client: TwilioChatClient!, channel: TCHChannel!, member: TCHMember!, updated: TCHMemberUpdate) {
        if app.shared.applicationState == .active {
            print(updated)
            app.nc.post(app.chatMemberUpdateNote(channel: channel, member: member))
        }
    }
    
}



extension Twilio: TwilioAccessManagerDelegate {
    
    func accessManagerTokenWillExpire(_ accessManager: TwilioAccessManager) {
        app.api.cabinet.channelToken(callback: { (token) in
            accessManager.updateToken(token)
        }) { (error) in
            app.alert(error: error)
        }
    }
    
}
